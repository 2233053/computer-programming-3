# Book Reservation System
A Computer Programming 3 Preliminary Project

This system allows a user client to reserve a book from the library,
and the librarian can the user's book reservations, 
manage the books, and user accounts.

---
## Description
A Book Reservation System for a Library

## Virtual Local Area Network(VLAN) Setup
ZeroTier setup: 
[Documentation](doc/ZEROTIER.md)

## Application Guide
Application guide: [Documentation](doc/GUIDE.md)

## Features
Librarian
- Manage Books
  - Add Books
  - Remove Books
  - Update Books
  - View Books
  - Update Books Status
- Manage User Accounts
  - Add User
  - Remove User
  - Update User
  - View Users
- Manage Book Reservations
  - View Book Reservations
  - Cancel Book Reservations
  - View Reserved Books

User
- View Books
- Reserve Books
- View Reserved Books
- Cancel Book Reservations
- View Book Reservations
#3
---
## Running the System
Requirements: Build the project using IntelliJ IDEA

## Running the Server
Run the server using IntelliJ IDEA or by running the following commands in the terminal
```bash
cd out/production/Book\ Reservation\ System/
java server.Server
```

## Running the Client Application
Run the client using IntelliJ IDEA or by running the following commands in the terminal
```bash
cd out/production/Book\ Reservation\ System/
java run.Client
```

---
## Authors
* Matt Danielle Bravo
* Neil Angelo Briones
* Milton Junsel Fabe
* Benny Gil Lactaotao
* Dylan Yeoj Magpili
* Jermaine Bryan Pascual
* Hans Elijah Viduya

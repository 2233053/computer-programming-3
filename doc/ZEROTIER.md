# Download and install Zerotier
https://www.zerotier.com/download/
# Joining the network
Open the Zerotier app and join the network with the following network ID:
```
41d49af6c2510005
```
- Click Join New Network
![Image](res/img.png)
- Enter the network ID and click Join
![Image](res/img_1.png)

- IMPORTANT: Finding your Zerotier IP, click on the network id, then manage addresses, and you will see your IP address
![Image](res/img_2.png)

- this will be your computer's IP address on the network

Test the connection by pinging the server from the client
```bash
ping 10.147.17.* # replace '*' with the your Zerotier IP address
```
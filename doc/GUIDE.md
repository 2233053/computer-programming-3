# Login Guide:
Enter your username and password (provided by the librarian, request an account if needed), 
then click on the login button to login to the system.

![img_8.png](res%2Fimg_8.png)

If needed, click the settings button to configure 
server IP address and click on the button to save the changes.

![img_14.png](res%2Fimg_14.png)

---
# Librarian View:
## Reservations:
Librarian can update user book reservations status
![img_3.png](res%2Fimg_3.png)

## Adding New Books:
Librarian can add a new book to the system
![img_4.png](res%2Fimg_4.png)

## Updating or Deleting a Book:
Librarian can update or delete a book from the system
![img_5.png](res%2Fimg_5.png)

## Creating a new account:
Librarian can create a new account for a user
![img_6.png](res%2Fimg_6.png)

## Editing user accounts:
Librarian can update or delete user accounts
![img_7.png](res%2Fimg_7.png)
----
# User View:
## Reserving a book:
Users can reserve a book by clicking on the reserve button.
![img_9.png](res%2Fimg_9.png)

## Viewing reserved books:
Users can view their reserved books
![img_10.png](res%2Fimg_10.png)

## Borrowed book view:
Users can view their borrowed books
![img_12.png](res%2Fimg_12.png)

## Returned books view:
Users can view their returned books
![img_13.png](res%2Fimg_13.png)
/**
 * Server class for managing book reservations and authentication.
 */
package server;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import shared.references.Book;
import shared.references.History;
import shared.references.Reservation;
import shared.references.User;
import shared.AuthenticationStatus;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * The Server class manages client connections and handles requests.
 *
 * @author BRAVO, Matt Danielle
 * @author BRIONES, Neil Angelo
 * @author MAGPILI, Dylan Yeoj
 * @author FABE, Milton Junsel
 * @author LACTAOTAO, Benny Gil
 * @author PASCUAL, Jermaine Bryan
 * @author VIDUYA, Hans Elijah
 */
public class Server {
    static int port = 2000;
    private Queue<ObjectOutputStream> clientOutputStreams = new ConcurrentLinkedDeque<>();

    /**
     * Main method to start the server.
     *
     * @param args Command line arguments (not used).
     */
    public static void main(String[] args) {
        new Server().startServer();
    }

    /**
     * Method to start the server and listen for client connections.
     */
    public void startServer() {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            System.out.println("Server listening on port " + port);

            while (!Thread.currentThread().isInterrupted()) {
                Socket clientSocket = serverSocket.accept();

                ClientHandler clientHandler = new ClientHandler(clientSocket);
                new Thread(clientHandler).start();
            }
        } catch (IOException e) {
            System.out.println("ERR IN SERVER: " + e.getMessage());
        }
    }

    /**
     * Method to update the availability status of a returned book in the XML database.
     *
     * @param bookId The ID of the returned book.
     * @param status The updated availability status.
     */
    public static void writeReturnedBookUpdateToXML(String bookId, String status) {
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document bookDatabase = documentBuilder.parse("res/book.xml");

            cleanXMLDocument(bookDatabase);

            NodeList bookNodes = bookDatabase.getElementsByTagName("book");

            for (int i = 0; i < bookNodes.getLength(); i++) {
                Element book = (Element) bookNodes.item(i);
                String currentBookId = book.getElementsByTagName("book_id").item(0).getTextContent();

                if (bookId.equals(currentBookId)) {
                    // Set the availability to "TRUE" since the book is already returned
                    book.getElementsByTagName("availability").item(0).setTextContent(status);
                    System.out.println(currentBookId);
                    // Write the updated document back to XML
                    writeDocumentToXML(bookDatabase, "res/book.xml");
                    System.out.println("Reservation update written to XML: " + bookId);
                    return; // Exit the method after updating the availability
                }
            }
            // If the loop finishes without finding the book ID, print a message
            System.out.println("Book not found: " + bookId);
        } catch (ParserConfigurationException | SAXException | IOException | TransformerException ex) {
            System.out.println(ex.getMessage());
        } catch (XPathExpressionException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Method to update the availability status of a returned book in the XML database.
     *
     * @param bookId The ID of the returned book.
     */
    public static void writeBookCopiesUpdateToXML(String bookId) {
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document bookDatabase = documentBuilder.parse("res/book.xml");

            cleanXMLDocument(bookDatabase);

            NodeList bookNodes = bookDatabase.getElementsByTagName("book");

            for (int i = 0; i < bookNodes.getLength(); i++) {
                Element book = (Element) bookNodes.item(i);
                String currentBookId = book.getElementsByTagName("book_id").item(0).getTextContent();

                if (bookId.equals(currentBookId)) {
                    // Set the availability to "TRUE" since the book is already returned
                    int copies = Integer.parseInt(book.getElementsByTagName("book_copies").item(0).getTextContent());
                    book.getElementsByTagName("book_copies").item(0).setTextContent(String.valueOf(++copies));
                    book.getElementsByTagName("availability").item(0).setTextContent("TRUE");

                    // Write the updated document back to XML
                    writeDocumentToXML(bookDatabase, "res/book.xml");
                    System.out.println("Reservation update written to XML: " + bookId);
                    return; // Exit the method after updating the availability
                }
            }
            // If the loop finishes without finding the book ID, print a message
            System.out.println("Book not found: " + bookId);
        } catch (ParserConfigurationException | SAXException | IOException | TransformerException ex) {
            System.out.println(ex.getMessage());
        } catch (XPathExpressionException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Method to authenticate a user based on provided ID and password.
     *
     * @param id       The user ID.
     * @param password The user password.
     * @return The authentication status.
     */
    public static AuthenticationStatus authenticateUser(String id, String password) {
        try {
            File file = new File("res/users.xml");
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newDefaultInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(file);
            document.getDocumentElement().normalize();

            NodeList userList = document.getElementsByTagName("user");
            for (int i = 0; i < userList.getLength(); i++) {
                Node userNode = userList.item(i);
                if (userNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element userElement = (Element) userNode;
                    String storedID = userElement.getElementsByTagName("id").item(0).getTextContent();
                    String storedPassword = userElement.getElementsByTagName("password").item(0).getTextContent();
                    if (storedID.equals(id)) {
                        if (storedPassword.equals(password)) {
                            String type = userElement.getAttribute("type");
                            String name = userElement.getElementsByTagName("name").item(0).getTextContent();
                            String email = userElement.getElementsByTagName("email").item(0).getTextContent();
                            AuthenticationStatus successStatus = (type.equals("admin")) ? AuthenticationStatus.SUCCESS_ADMIN : AuthenticationStatus.SUCCESS_CLIENT;
                            return successStatus;
                        } else {
                            return AuthenticationStatus.PASSWORD_INCORRECT;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            System.err.println("Error: " + ex.getMessage());
        }
        // User not found
        return AuthenticationStatus.USER_NOT_FOUND;
    }

    /**
     * Method to update reservation information in the XML database for an admin.
     *
     * @param res       The reservation to be updated.
     * @param operation The operation to perform (APPROVE, REJECT, RETURN, PICKUP).
     * @param status    The updated status.
     */
    public static void adminUpdatedReservationXML(Reservation res, String operation, String status) {
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document reservationDatabase = documentBuilder.parse("res/reservations.xml");
            cleanXMLDocument(reservationDatabase);

            int resId = Integer.parseInt(res.getUId());
            String studentID = res.getStudentId();

            // Find the reservation element within the student node
            NodeList reservations = reservationDatabase.getElementsByTagName("reservation");

            for (int j = 0; j < reservations.getLength(); j++) {
                Element reservation = (Element) reservations.item(resId);
                String reservedBookId = reservation.getElementsByTagName("book_id").item(0).getTextContent();

                if (resId >= 0 && resId < reservations.getLength()) {
                    switch (operation) {
                        case "APPROVE", "REJECT" ->
                                reservation.getElementsByTagName("approved").item(0).setTextContent(status);
                        case "RETURN" ->
                                reservation.getElementsByTagName("returnStatus").item(0).setTextContent(status);
                        case "PICKUP" ->
                                reservation.getElementsByTagName("pickUpStatus").item(0).setTextContent(status);
                    }

                    // Write the updated documents back to XML
                    writeDocumentToXML(reservationDatabase, "res/reservations.xml");
                    System.out.println("Reservation updated for student: " + studentID + ", for book ID: " + res.getBookId());
                    System.out.println(operation + " set to " + status);
                    return;
                } else {
                    System.out.println("Invalid index: " + resId);
                }

            }
            System.out.println("Reservation not found for student: " + studentID + ", book: " + res.getBookId());
            return;

        } catch (ParserConfigurationException e) {
            System.out.println(e.getMessage());
        } catch (IOException | SAXException | XPathExpressionException | TransformerException e) {
            throw new RuntimeException(e);
        }
    }

    public static Boolean adminDueCheckXML(Reservation res) {
        boolean isOverdue = false;
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document reservationDatabase = documentBuilder.parse("res/reservations.xml");
            Document bookDataBase = documentBuilder.parse("res/book.xml");
            cleanXMLDocument(reservationDatabase);
            cleanXMLDocument(bookDataBase);

            int resId = Integer.parseInt(res.getUId());

            // Find the reservation element within the student node
            NodeList reservations = reservationDatabase.getElementsByTagName("reservation");

            for (int j = 0; j < reservations.getLength(); j++) {
                Element reservation = (Element) reservations.item(resId);
                Element dateReturnedElement = (Element) reservation.getElementsByTagName("dateReturned").item(0);

                // Check if the "dateReturned" element exists and is not equal to "0000-01-01"
                if (dateReturnedElement == null || (dateReturnedElement.getTextContent()).equals("0000-01-01")) {
                    LocalDate dateReturned = LocalDate.now();
                    reservation.getElementsByTagName("dateReturned").item(0).setTextContent(dateReturned.toString());
                }

                // Check if the reservation is overdue
                LocalDate dateReturned = LocalDate.parse(reservation.getElementsByTagName("dateReturned").item(0).getTextContent());
                LocalDate dueDate = LocalDate.parse(reservation.getElementsByTagName("dueDate").item(0).getTextContent());

                if (dateReturned.isAfter(dueDate)) {
                    isOverdue = true;
                    // The reservation is overdue, update the "overdueStatus" to true
                    Element overdueStatusElement = (Element) reservation.getElementsByTagName("overdueStatus").item(0);
                    if (overdueStatusElement != null) {
                        overdueStatusElement.setTextContent("TRUE");
                    }

                    System.out.println("Reservation is overdue for reservation ID: " + res.getUId() + ", book ID: " + res.getBookId());
                } else {
                    System.out.println("Reservation is not overdue for reservation ID: " + res.getUId() + ", book ID: " + res.getBookId());
                }
            }

            // Write the updated documents back to XML
            writeDocumentToXML(reservationDatabase, "res/reservations.xml");
            System.out.println("Due checked for reservation ID: " + res.getUId() + ", for book ID: " + res.getBookId());

            return isOverdue;
        } catch (ParserConfigurationException | IOException | SAXException | TransformerException |
                 XPathExpressionException e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * Method to read books from an XML file and return a list of Book objects.
     *
     * @param xmlFilePath The path to the XML file.
     * @return ArrayList of Book objects.
     */
    public static ArrayList<Book> readBooksFromXML(String xmlFilePath) {
        ArrayList<Book> bookList = new ArrayList<>();

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new File(xmlFilePath));
            NodeList bookNodes = document.getElementsByTagName("book");


            for (int i = 0; i < bookNodes.getLength(); i++) {
                Node bookNode = bookNodes.item(i);
                if (bookNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element bookElement = (Element) bookNode;
                    String bookId = bookElement.getElementsByTagName("book_id").item(0).getTextContent();
                    String authors = bookElement.getElementsByTagName("authors").item(0).getTextContent();
                    String publicationYear = bookElement.getElementsByTagName("original_publication_year").item(0).getTextContent();
                    String title = bookElement.getElementsByTagName("original_title").item(0).getTextContent();
                    String languageCode = bookElement.getElementsByTagName("language_code").item(0).getTextContent();
                    boolean availability = Boolean.parseBoolean(bookElement.getElementsByTagName("availability").item(0).getTextContent());
                    int bookCopies = Integer.parseInt(bookElement.getElementsByTagName("book_copies").item(0).getTextContent());

                    Book book = new Book(bookId, title, authors, publicationYear, languageCode, availability, bookCopies);
                    bookList.add(book);
                }
            }
        } catch (IOException | ParserConfigurationException | SAXException e) {
            System.out.println(e.getMessage());
        }

        return bookList;
    }

    /**
     * Method to read history from an XML file and return a list of Returned objects.
     *
     * @param xmlFilePath The path to the XML file.
     * @param bookList    The list of Book objects for reference.
     * @return ArrayList of Returned objects.
     */
    public static ArrayList<History> readHistoryFromXML(String xmlFilePath, ArrayList<Book> bookList) {
        ArrayList<History> historyList = new ArrayList<>();

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(xmlFilePath);
            NodeList studentNodes = document.getElementsByTagName("student");

            for (int i = 0; i < studentNodes.getLength(); i++) {
                Node studentNode = studentNodes.item(i);
                if (studentNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element historyElement = (Element) studentNode;
                    String studentId = historyElement.getAttribute("id");

                    // Get reservation nodes within the context of current student
                    NodeList historyNodes = historyElement.getElementsByTagName("history");

                    for (int j = 0; j < historyNodes.getLength(); j++) {
                        Node reservationNode = historyNodes.item(j);
                        if (reservationNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element reservationElement = (Element) reservationNode;
                            String bookId = reservationElement.getElementsByTagName("book_id").item(0).getTextContent();

                            Book book = null;
                            for (Book b : bookList) {
                                if (b.getBookID().equals(bookId)) {
                                    book = b;
                                    break;
                                }
                            }

                            if (book != null) {
                                History history = new History(studentId, bookId);
                                historyList.add(history);
                            } else {
                                System.out.println("Book not found for reservation with ID: " + bookId);
                            }
                        }
                    }
                }
            }
        } catch (IOException | ParserConfigurationException | SAXException e) {
            System.out.println(e.getMessage());
        }
        return historyList;
    }

    /**
     * Method to read reservations from an XML file and return a list of Reservation objects.
     *
     * @param xmlFilePath The path to the XML file.
     * @param bookList    The list of Book objects for reference.
     * @return ArrayList of Reservation objects.
     */
    public static ArrayList<Reservation> readReservationsFromXML(String xmlFilePath, ArrayList<Book> bookList) {
        ArrayList<Reservation> reservationList = new ArrayList<>();

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(xmlFilePath);
            NodeList studentNodes = document.getElementsByTagName("student");

            for (int i = 0; i < studentNodes.getLength(); i++) {
                Node studentNode = studentNodes.item(i);
                if (studentNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element studentElement = (Element) studentNode;
                    String studentId = studentElement.getAttribute("id");

                    // Get reservation nodes within the context of current student
                    NodeList reservationNodes = studentElement.getElementsByTagName("reservation");

                    for (int j = 0; j < reservationNodes.getLength(); j++) {
                        Node reservationNode = reservationNodes.item(j);
                        if (reservationNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element reservationElement = (Element) reservationNode;
                            String bookId = reservationElement.getElementsByTagName("book_id").item(0).getTextContent();
                            String reservationDateString = reservationElement.getElementsByTagName("reservationDate").item(0).getTextContent();
                            boolean approved = Boolean.parseBoolean(reservationElement.getElementsByTagName("approved").item(0).getTextContent());
                            boolean returnStatus = Boolean.parseBoolean(reservationElement.getElementsByTagName("returnStatus").item(0).getTextContent());
                            boolean pickUpStatus = Boolean.parseBoolean(reservationElement.getElementsByTagName("pickUpStatus").item(0).getTextContent());
                            String dueDateString = reservationElement.getElementsByTagName("dueDate").item(0).getTextContent();
                            String dateReturnedString = reservationElement.getElementsByTagName("dateReturned").item(0).getTextContent();
                            boolean overdueStatus = Boolean.parseBoolean(reservationElement.getElementsByTagName("overdueStatus").item(0).getTextContent());

                            Book book = null;
                            for (Book b : bookList) {
                                if (b.getBookID().equals(bookId)) {
                                    book = b;
                                    break;
                                }
                            }

                            if (book != null) {
                                LocalDate reservationDate = LocalDate.parse(reservationDateString);
                                LocalDate dueDate = LocalDate.parse(dueDateString);
                                LocalDate dateReturned = LocalDate.parse(dateReturnedString);

                                Reservation reservation = new Reservation(studentId, String.valueOf(j), bookId, reservationDate, dueDate, approved, returnStatus, pickUpStatus, dateReturned, overdueStatus);
                                reservationList.add(reservation);
                            } else {
                                System.out.println("Book not found for reservation with ID: " + bookId);
                            }
                        }
                    }
                }
            }
        } catch (IOException | ParserConfigurationException | SAXException e) {
            System.out.println(e.getMessage());
        }
        return reservationList;
    }

    /**
     * Method to cancel a reservation and update the XML databases.
     *
     * @param studentId The ID of the student.
     * @param BookTitle The book to be canceled.
     * @return A status message indicating the success or failure of the cancellation.
     */
    public static String returnCancellation(String studentId, String BookTitle) {
        String BookIdString = "";

        for (Book oldBook : readBooksFromXML("res/book.xml")) {
            if (oldBook.getTitle().equalsIgnoreCase(BookTitle)) {
                BookIdString = oldBook.getBookID();
            }
        }

        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document bookDatabase;
            Document reservationDatabase;

            bookDatabase = documentBuilder.parse("res/book.xml");
            reservationDatabase = documentBuilder.parse("res/reservations.xml");
            cleanXMLDocument(bookDatabase);
            cleanXMLDocument(reservationDatabase);

            // Find the student node by ID
            NodeList studentNodes = reservationDatabase.getElementsByTagName("student");

            for (int i = 0; i < studentNodes.getLength(); i++) {
                Element student = (Element) studentNodes.item(i);
                String studentIdAttr = student.getAttribute("id");

                if (studentId.equals(studentIdAttr)) {
                    // Find all reservation elements within the student node
                    NodeList reservations = student.getElementsByTagName("reservation");

                    for (int j = 0; j < reservations.getLength(); j++) {
                        Element reservation = (Element) reservations.item(j);
                        String reservedBookId = reservation.getElementsByTagName("book_id").item(0).getTextContent();
                        boolean isApproved = Boolean.parseBoolean(reservation.getElementsByTagName("approved").item(0).getTextContent());

                        if (BookIdString.equals(reservedBookId)) {
                            if (isApproved) {
                                return "APPROVED ALREADY";
                            }

                            // Get the book element for the reserved book
                            Element bookElement = findBookById(bookDatabase, BookIdString);

                            // Update book/s information
                            int copies = Integer.parseInt(bookElement.getElementsByTagName("book_copies").item(0).getTextContent());
                            bookElement.getElementsByTagName("book_copies").item(0).setTextContent(String.valueOf(++copies));

                            if (bookElement != null && copies > 0) {
                                bookElement.getElementsByTagName("availability").item(0).setTextContent("TRUE");
                            }

                            // Remove the current reservation element from the document
                            student.removeChild(reservation);

//                            if (student.getChildNodes().getLength() == 0) {
//                                student.getParentNode().removeChild(student);
//
//                            }
                            // Write the updated documents back to XML
                            writeDocumentToXML(reservationDatabase, "res/reservations.xml");
                            writeDocumentToXML(bookDatabase, "res/book.xml");

                            return "SUCCESSFUL";
                        }
                    }

                    // If the loop completes without finding the reservation, print a message
                    return "Reservation not found for student: " + studentId + ", book: " + BookIdString;
                }
            }


            return "Student not found: " + studentId;
        } catch (Exception e) {
            return "UNSUCCESSFUL";
        }
    }

    /**
     * Retrieves a list of reservations by reading data from the XML databases.
     *
     * @return ArrayList of Reservation objects.
     */
    public ArrayList<Reservation> getReservationList() {
        ArrayList<Book> bookList = readBooksFromXML("res/book.xml");
        ArrayList<Reservation> reservationList = readReservationsFromXML("res/reservations.xml", bookList);
        return reservationList;
    }

    /**
     * Handles the reservation of a book for a student. Checks availability, updates databases,
     * and provides a response indicating the success or failure of the reservation.
     *
     * @param studentId
     * @param bookID
     * @return
     * @throws ParserConfigurationException If there is an issue with parser configuration.
     * @throws IOException                  If there is an issue with input or output.
     * @throws SAXException                 If there is an issue with XML parsing.
     * @throws XPathExpressionException     If there is an issue with XPath expression.
     */
    public static String returnReservation(String studentId, String bookID, LocalDate reservationDate) throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document bookDatabase = documentBuilder.parse(new File("res/book.xml"));
        Document reservationDatabase = documentBuilder.parse(new File("res/reservations.xml"));

        String response = writeStringReservationsToXML(studentId, bookID, reservationDate);

        cleanXMLDocument(bookDatabase);
        cleanXMLDocument(reservationDatabase);

        return response;
    }

    /**
     * Writes a reservation entry to the XML database and updates the book's availability.
     *
     * @param studentId
     * @param bookId
     * @param reservationDate
     * @return A response message indicating the success or reason for failure of the reservation.
     */
    public static String writeStringReservationsToXML(String studentId, String bookId, LocalDate reservationDate) {
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document bookDatabase;
            Document reservationDatabase;

            bookDatabase = documentBuilder.parse("res/book.xml");
            reservationDatabase = documentBuilder.parse("res/reservations.xml");
            cleanXMLDocument(bookDatabase);
            cleanXMLDocument(reservationDatabase);

            Element rootElement;

            Element bookElement = findBookById(bookDatabase, bookId);
            if (bookElement != null) {
                String availability = bookElement.getElementsByTagName("availability").item(0).getTextContent();
                String copies = bookElement.getElementsByTagName("book_copies").item(0).getTextContent();
//                if (availability.equalsIgnoreCase("FALSE")) {
//                    return "RESERVED ALREADY";
//                }
                if (copies.equalsIgnoreCase("0")) {
                    return "NO COPIES AVAILABLE";
                }
            } else if (bookElement == null) {
                return "BOOK NOT FOUND!";
            }

            File reservationFile = new File("res/reservations.xml");
            if (reservationFile.exists()) {
                reservationDatabase.getDocumentElement().normalize();
                rootElement = reservationDatabase.getDocumentElement();
            } else {
                reservationDatabase = documentBuilder.newDocument();
                rootElement = reservationDatabase.createElement("data");
                reservationDatabase.appendChild(rootElement);
            }

            NodeList studentNodes = rootElement.getElementsByTagName("student");
            Element studentElement = null;

            // Check if student with given ID already exists
            for (int i = 0; i < studentNodes.getLength(); i++) {
                Node studentNode = studentNodes.item(i);
                if (studentNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element currentStudent = (Element) studentNode;
                    String currentId = currentStudent.getAttribute("id");
                    if (currentId.equals(studentId)) {
                        studentElement = currentStudent;
                        break;
                    }
                }
            }

            // If the student doesn't exist, create a new one
            if (studentElement == null) {
                studentElement = reservationDatabase.createElement("student");
                studentElement.setAttribute("id", studentId);
                rootElement.appendChild(studentElement);
            }

            Element reservationElement = reservationDatabase.createElement("reservation");
            studentElement.appendChild(reservationElement);

            Element bookIdElement = reservationDatabase.createElement("book_id");
            bookIdElement.appendChild(reservationDatabase.createTextNode(bookId));
            reservationElement.appendChild(bookIdElement);

            Element reservationDateElement = reservationDatabase.createElement("reservationDate");
            reservationDateElement.appendChild(reservationDatabase.createTextNode(reservationDate.toString()));
            reservationElement.appendChild(reservationDateElement);

            Element dueDate = reservationDatabase.createElement("dueDate");
            dueDate.appendChild(reservationDatabase.createTextNode(reservationDate.plusWeeks(1).toString()));
            reservationElement.appendChild(dueDate);

            Element approved = reservationDatabase.createElement("approved");
            approved.setTextContent("false");
            reservationElement.appendChild(approved);

            Element returnStatus = reservationDatabase.createElement("returnStatus");
            returnStatus.setTextContent("false");
            reservationElement.appendChild(returnStatus);

            Element pickUpStatus = reservationDatabase.createElement("pickUpStatus");
            pickUpStatus.setTextContent("false");
            reservationElement.appendChild(pickUpStatus);

            Element dateReturnedElement = reservationDatabase.createElement("dateReturned");
            dateReturnedElement.appendChild(reservationDatabase.createTextNode("0000-01-01"));
            reservationElement.appendChild(dateReturnedElement);

            Element overdueStatus = reservationDatabase.createElement("overdueStatus");
            overdueStatus.setTextContent("false");
            reservationElement.appendChild(overdueStatus);

            if (bookElement != null) {
                int oldCopies = Integer.parseInt(bookElement.getElementsByTagName("book_copies").item(0).getTextContent());
                if (oldCopies > 0) {
                    bookElement.getElementsByTagName("book_copies").item(0).setTextContent(String.valueOf(--oldCopies)); // decrement copies
                } else if (oldCopies == 0) {
                    bookElement.getElementsByTagName("availability").item(0).setTextContent("FALSE");
                } else {
                    bookElement.getElementsByTagName("availability").item(0).setTextContent("FALSE");
                }
            }

            writeDocumentToXML(reservationDatabase, "res/reservations.xml");
            cleanXMLDocument(reservationDatabase);
            writeDocumentToXML(bookDatabase, "res/book.xml");
            cleanXMLDocument(bookDatabase);

            return "SUCCESSFUL";

        } catch (ParserConfigurationException | TransformerException | SAXException | IOException |
                 XPathExpressionException e) {
            System.out.println(e.getMessage());
            return "UNSUCCESSFUL";
        }
    }

    /**
     * Writes a history entry to the XML database.
     *
     * @param studentId
     * @param bookId
     * @return A response message indicating the success or reason for failure of writing history entry.
     */
    public static String writeStringHistoryToXML(String studentId, String bookId) {
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document historyDatabase;

            historyDatabase = documentBuilder.parse("res/history.xml");
            cleanXMLDocument(historyDatabase);

            Element rootElement;

            File historyFile = new File("res/history.xml");
            if (historyFile.exists()) {
                historyDatabase.getDocumentElement().normalize();
                rootElement = historyDatabase.getDocumentElement();
            } else {
                historyDatabase = documentBuilder.newDocument();
                rootElement = historyDatabase.createElement("data");
                historyDatabase.appendChild(rootElement);
            }

            NodeList studentNodes = rootElement.getElementsByTagName("student");
            Element studentElement = null;

            // Check if student with given ID already exists
            for (int i = 0; i < studentNodes.getLength(); i++) {
                Node studentNode = studentNodes.item(i);
                if (studentNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element currentStudent = (Element) studentNode;
                    String currentId = currentStudent.getAttribute("id");
                    if (currentId.equals(studentId)) {
                        studentElement = currentStudent;
                        break;
                    }
                }
            }

            // If the student doesn't exist, create a new one
            if (studentElement == null) {
                studentElement = historyDatabase.createElement("student");
                studentElement.setAttribute("id", studentId);
                rootElement.appendChild(studentElement);
            }

            Element historyElement = historyDatabase.createElement("history");
            studentElement.appendChild(historyElement);

            Element bookIdElement = historyDatabase.createElement("book_id");
            bookIdElement.appendChild(historyDatabase.createTextNode(bookId));
            historyElement.appendChild(bookIdElement);

            writeDocumentToXML(historyDatabase, "res/history.xml");
            cleanXMLDocument(historyDatabase);

            return "SUCCESSFUL";

        } catch (ParserConfigurationException | TransformerException | SAXException | IOException |
                 XPathExpressionException e) {
            System.out.println(e.getMessage());
            return "UNSUCCESSFUL";
        }
    }

    /**
     * Updates a reservation by canceling the old book and reserving a new one.
     *
     * @param studentId
     * @param oldBookTitle
     * @param newBookTitle
     * @return A response message indicating the success or reason for failure of the update.
     */
    public static String writeReservationUpdate(String studentId, String oldBookTitle, String newBookTitle, String newReservationDate, String newDueDate) {
        String oldBookIdString = "";
        String newBookIdString = "";

        for (Book oldBook : readBooksFromXML("res/book.xml")) {
            if (oldBook.getTitle().equalsIgnoreCase(oldBookTitle)) {
                oldBookIdString = oldBook.getBookID();
            }
        }

        for (Book newBook : readBooksFromXML("res/book.xml")) {
            if (newBook.getTitle().equalsIgnoreCase(newBookTitle)) {
                newBookIdString = newBook.getBookID();
            }
        }

        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document bookDatabase;
            Document reservationDatabase;

            bookDatabase = documentBuilder.parse("res/book.xml");
            reservationDatabase = documentBuilder.parse("res/reservations.xml");
            cleanXMLDocument(bookDatabase);
            cleanXMLDocument(reservationDatabase);


            // Find the reservation node by student ID and book ID
            NodeList studentNodes = reservationDatabase.getElementsByTagName("student");

            for (int i = 0; i < studentNodes.getLength(); i++) {
                Element student = (Element) studentNodes.item(i);
                String studentIdAttr = student.getAttribute("id");

                if (studentId.equals(studentIdAttr)) {
                    // Find the reservation element within the student node
                    NodeList reservations = student.getElementsByTagName("reservation");

                    for (int j = 0; j < reservations.getLength(); j++) {
                        Element reservation = (Element) reservations.item(j);
                        String reservedBookId = reservation.getElementsByTagName("book_id").item(0).getTextContent();
                        boolean isApproved = Boolean.parseBoolean(reservation.getElementsByTagName("approved").item(0).getTextContent());

                        if (oldBookIdString.equals(reservedBookId)) {
                            if (isApproved) {
                                return "Cancellation not allowed. Reservation is already approved.";
                            }

                            // Get the book element for the reserved book
                            Element oldBookElement = findBookById(bookDatabase, oldBookIdString);
                            Element newBookElement = findBookById(bookDatabase, newBookIdString);

                            if (newBookElement == null) {
                                return "BOOK NOT FOUND!";
                            }

                            // Update book/s information
                            int oldCopies = Integer.parseInt(oldBookElement.getElementsByTagName("book_copies").item(0).getTextContent());
                            int newCopies = Integer.parseInt(newBookElement.getElementsByTagName("book_copies").item(0).getTextContent());
                            oldBookElement.getElementsByTagName("book_copies").item(0).setTextContent(String.valueOf(++oldCopies));
                            newBookElement.getElementsByTagName("book_copies").item(0).setTextContent(String.valueOf(--newCopies));
                            if (newCopies < 0) {
                                return "NO COPIES AVAILABLE";
                            }

                            // Update the book ID element value
                            reservation.getElementsByTagName("book_id").item(0).setTextContent(newBookIdString);
                            reservation.getElementsByTagName("reservationDate").item(0).setTextContent(newReservationDate);
                            reservation.getElementsByTagName("dueDate").item(0).setTextContent(newDueDate);

                            // Write the updated documents back to XML
                            writeDocumentToXML(reservationDatabase, "res/reservations.xml");
                            writeDocumentToXML(bookDatabase, "res/book.xml");

                            return "SUCCESSFUL";

                        }
                    }
                    return "Reservation not found for student: " + studentId + ", book: " + oldBookIdString;
                }
            }
            return "UNSUCCESSFUL";
        } catch (Exception e) {
            return "UNSUCCESSFUL";
        }
    }

    /**
     * Finds a book element in the XML database by book ID.
     *
     * @param bookDatabase The XML document representing the book database.
     * @param chosenId     ID of the book to find.
     * @return The Element representing the book in the XML document, or null if not found.
     */
    public static Element findBookById(Document bookDatabase, String chosenId) {
        NodeList bookNodes = bookDatabase.getElementsByTagName("book");

        for (int i = 0; i < bookNodes.getLength(); i++) {
            Node bookNode = bookNodes.item(i);
            if (bookNode.getNodeType() == Node.ELEMENT_NODE) {
                Element bookElement = (Element) bookNode;
                String bookId = bookElement.getElementsByTagName("book_id").item(0).getTextContent();

                if (bookId.equalsIgnoreCase(chosenId)) {
                    return bookElement;
                }
            }
        }

        return null; // book not found
    }

    /**
     * Writes an XML document to a file and removes empty text nodes.
     *
     * @param document The XML document to write.
     * @param filePath The path of the file to write to.
     * @throws TransformerException     If there is an issue with the transformer.
     * @throws XPathExpressionException If there is an issue with XPath expression.
     */
    private static void writeDocumentToXML(Document document, String filePath) throws TransformerException, XPathExpressionException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(new File(filePath));
        transformer.transform(source, result);
        cleanXMLDocument(document); // remove spaces in xml

    }

    /**
     * Cleans up an XML document by removing empty text nodes.
     *
     * @param doc The XML document to clean.
     * @throws XPathExpressionException If there is an issue with XPath expression.
     */
    private static void cleanXMLDocument(Document doc) throws XPathExpressionException {
        XPathFactory xpathFactory = XPathFactory.newInstance();
        XPathExpression xpathExp = xpathFactory
                .newXPath()
                .compile("//text()[normalize-space(.) = '']");
        NodeList emptyTextNodes = (NodeList) xpathExp
                .evaluate(doc, XPathConstants.NODESET);

        for (int i = 0; i < emptyTextNodes.getLength(); i++) {
            Node emptyTextNode = emptyTextNodes.item(i);
            emptyTextNode.getParentNode().removeChild(emptyTextNode);
        }
    }

    /**
     * Inner class representing a client handler for managing client-server communication.
     */
    private class ClientHandler implements Runnable {
        private Socket clientSocket;
        private ObjectOutputStream outputStream;

        public ClientHandler(Socket clientSocket) {
            this.clientSocket = clientSocket;
        }

        @Override
        public void run() {
            try (ObjectInputStream reader = new ObjectInputStream(clientSocket.getInputStream());
                 ObjectOutputStream writer = new ObjectOutputStream(clientSocket.getOutputStream())
            ) {
                outputStream = writer;
                clientOutputStreams.add(outputStream);

                while (true) {
                    Object recievedObject = reader.readObject();
                    if (recievedObject instanceof User user) {
                        sendObject(writer, authenticateUser(user.getID(), user.getPassword()));

                    } else if (recievedObject instanceof String response) {
                        switch (response) {
                            case "Book List" -> sendObject(writer, new ArrayList<>(readBooksFromXML("res/book.xml")));
                            case "Reserve" -> {
                                String studentID = (String) reader.readObject();
                                String bookID = (String) reader.readObject();
                                LocalDate reservationDate = (LocalDate) reader.readObject();
                                sendObject(writer, returnReservation(studentID, bookID, reservationDate));
                            }
                            case "Reservation" -> sendObject(writer, new ArrayList<>(getReservationList()));
                            case "Cancellation" -> {
                                String studentID = (String) reader.readObject();
                                String bookID = (String) reader.readObject();
                                sendObject(writer, returnCancellation(studentID, bookID));
                            }
                            case "Update" -> {
                                String studentID = (String) reader.readObject();
                                String oldID = (String) reader.readObject();
                                String bookID = (String) reader.readObject();
                                String newReservationDate = (String) reader.readObject();
                                String newDueDate = (String) reader.readObject();
                                sendObject(writer, writeReservationUpdate(studentID, oldID, bookID, newReservationDate, newDueDate));
                            }
                            case "Returned" -> {
                                sendObject(writer, new ArrayList<>(readHistoryFromXML("res/history.xml", readBooksFromXML("res/book.xml"))));
                            }
                        }
                    } else {
                        System.out.println("CLIENT IS NOT SENDING A USER OBJECT!");
                    }
                }
            } catch (IOException | ClassNotFoundException | XPathExpressionException | ParserConfigurationException |
                     SAXException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * Sends an object to the client through the specified ObjectOutputStream.
     *
     * @param writer The ObjectOutputStream used to send the object.
     * @param object The object to be sent.
     */
    private static void sendObject(ObjectOutputStream writer, Object object) {
        try {
            writer.writeObject(object);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}

/**
 * The main class representing the entry point of the Book Reservation System application.
 * It initializes the user interface and sets up the login functionality.
 */

package run;

import com.formdev.flatlaf.fonts.roboto.FlatRobotoFont;
import com.formdev.flatlaf.themes.FlatMacLightLaf;
import raven.toast.Notifications;
import user.login.controller.LoginController;
import user.login.model.LoginModel;
import user.login.view.LoginView;

import javax.swing.*;
import java.awt.*;

/**
 * The main application class that sets up the initial frame and launches the Book Reservation System.
 *
 * @author BRAVO, Matt Danielle
 * @author BRIONES, Neil Angelo
 * @author MAGPILI, Dylan Yeoj
 * @author FABE, Milton Junsel
 * @author LACTAOTAO, Benny Gil
 * @author PASCUAL, Jermaine Bryan
 * @author VIDUYA, Hans Elijah
 */
public class Application extends JFrame {

    /**
     * Constructs an instance of the Application class.
     */
    public Application() {
        init();
    }

    /**
     * Initializes the application by setting up the main frame, configuring settings,
     * and initializing the login view, model, and controller.
     */
    private void init() {
        setTitle("Book Reservation System Login");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(new Dimension(1200, 700));
        setLocationRelativeTo(null);

        ImageIcon img = new ImageIcon("res/icon/user_icon.jpg");
        setIconImage(img.getImage());

        LoginView view = new LoginView();
        LoginModel model = new LoginModel();
        LoginController controller = new LoginController(view, model);

        setContentPane(view);
        Notifications.getInstance().setJFrame(this);
    }

    /**
     * The main method that initializes the FlatLaf theme, sets up the application,
     * and makes the application visible to the user.
     *
     * @param args The command-line arguments.
     */
    public static void main(String[] args) {
        FlatRobotoFont.install();
        FlatMacLightLaf.registerCustomDefaultsSource("themes");
        FlatMacLightLaf.setup();
        EventQueue.invokeLater(() -> new Application().setVisible(true));
    }
}
package util;

/**
 * The PasswordStrength class provides utility methods for various operations.
 */
public class PasswordStrength {

    /**
     * Checks the strength of a password based on various criteria.
     * Returns a strength score indicating the password strength level.
     *
     * @param password The password to be checked.
     * @return An integer representing the strength level of the password:
     *         - 1 if the password strength is weak.
     *         - 2 if the password strength is moderate.
     *         - 3 if the password strength is strong.
     */
    public static int checkPasswordStrength(String password) {
        int score = 0;
        // Check if password length is at least 8 characters
        if (password.length() >= 8) {
            score++;
        }
        // Check if password contains at least one uppercase letter
        boolean hasUppercase = !password.equals(password.toLowerCase());
        if (hasUppercase) {
            score++;
        }
        // Check if password contains at least one lowercase letter
        boolean hasLowercase = !password.equals(password.toUpperCase());
        if (hasLowercase) {
            score++;
        }
        // Check if password contains at least one digit
        boolean hasDigit = password.matches(".*\\d.*");
        if (hasDigit) {
            score++;
        }
        // Check if password contains at least one special character
        boolean hasSpecialChar = !password.matches("[A-Za-z0-9]*");
        if (hasSpecialChar) {
            score++;
        }
        // Determine password strength level based on score
        if (score < 3) {
            return 1;//weak password
        } else if (score < 5) {
            return 2;//Moderate password
        } else {
            return 3;//Strong password
        }
    }
}

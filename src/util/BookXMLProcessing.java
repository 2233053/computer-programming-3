package util;

import org.w3c.dom.*;
import org.xml.sax.SAXException;
import shared.references.Book;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * The BookXMLProcessing class provides methods to write book information to an XML file.
 */
public class BookXMLProcessing {

    @SuppressWarnings("Duplicates")
    public static ArrayList<Book> readBooksFromXML() {
        ArrayList<Book> books = new ArrayList<>();
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new File("res/book.xml"));
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("book");

            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    String id = element.getElementsByTagName("book_id").item(0).getTextContent();
                    String author = element.getElementsByTagName("authors").item(0).getTextContent();
                    String year = element.getElementsByTagName("original_publication_year").item(0).getTextContent();
                    String title = element.getElementsByTagName("original_title").item(0).getTextContent();
                    String language = element.getElementsByTagName("language_code").item(0).getTextContent();
                    String availability = element.getElementsByTagName("availability").item(0).getTextContent();
                    int bookCopies = Integer.parseInt(element.getElementsByTagName("book_copies").item(0).getTextContent());

                    books.add(new Book(id, title, author, year, language, Boolean.parseBoolean(availability), bookCopies));
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            System.err.println("Error: " + e.getMessage());
        }
        return books;
    }

    /**
     * The writeBookToXML method writes a new book to the XML file. <br>
     * This method creates a new book element and appends it to the existing XML file.
     *
     * @param book The Book object to write.
     */
    @SuppressWarnings("Duplicates")
    public static void writeBookToXML(Book book) {
        try {
            if (book == null) { // null check
//                throw new IllegalArgumentException("The book object cannot be null.");
                System.out.println("writeBookToXML: Book is null");
                return;
            }
            // Load existing XML file
            File xmlFile = new File("res/book.xml");
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(xmlFile);

            // Create a new book element and append it to the root
            Element root = doc.getDocumentElement();
            root.appendChild(createBook(doc, book.getBookID(), book.getTitle(), book.getAuthor(), book.getPublicationYear(), book.isAvailable(), book.getBookCopies()));

            // Save the modified XML file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transf = transformerFactory.newTransformer();

            transf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transf.setOutputProperty(OutputKeys.INDENT, "yes");
            transf.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

            DOMSource source = new DOMSource(doc);
            cleanXMLDocument(doc);
            StreamResult result = new StreamResult(xmlFile);
            transf.transform(source, result);
        } catch (IllegalArgumentException | ParserConfigurationException | SAXException | IOException | DOMException |
                 TransformerFactoryConfigurationError | XPathExpressionException | TransformerException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Creates a new book element with the specified information.
     *
     * @param doc             The Document object to create the element.
     * @param bookID          The ID of the book.
     * @param title           The title of the book.
     * @param author          The author of the book.
     * @param publicationYear The publication year of the book.
     * @param availability    The availability status of the book.
     * @return The newly created book element.
     */
    private static Node createBook(Document doc, String bookID, String title, String author, String publicationYear, boolean availability, int bookCopies) {
        Element book = doc.createElement("book");
        book.appendChild(createElement(doc, "book_id", String.valueOf(bookID)));
        book.appendChild(createElement(doc, "authors", author));
        book.appendChild(createElement(doc, "original_publication_year", publicationYear));
        book.appendChild(createElement(doc, "original_title", title));
        book.appendChild(createElement(doc, "language_code", "eng"));
        book.appendChild(createElement(doc, "availability", String.valueOf(availability).toUpperCase())); // Convert to uppercase
        book.appendChild(createElement(doc, "book_copies", String.valueOf(bookCopies)));

        return book;
    }

    /**
     * Creates a new element with the specified name and value.
     *
     * @param doc   The Document object to create the element.
     * @param name  The name of the element.
     * @param value The value of the element.
     * @return The newly created element.
     */
    private static Node createElement(Document doc, String name, String value) {
        Element node = doc.createElement(name);
        node.appendChild(doc.createTextNode(value));

        return node;
    }

    /**
     * Removes empty text nodes from the XML document.
     *
     * @param doc The XML Document to clean.
     * @throws XPathExpressionException If an error occurs while evaluating XPath expressions.
     */
    private static void cleanXMLDocument(Document doc) throws XPathExpressionException {
        XPathFactory xpathFactory = XPathFactory.newInstance();

        XPathExpression xpathExp = xpathFactory
                .newXPath()
                .compile("//text()[normalize-space(.) = '']");
        NodeList emptyTextNodes = (NodeList) xpathExp
                .evaluate(doc, XPathConstants.NODESET);

        for (int i = 0; i < emptyTextNodes.getLength(); i++) {
            Node emptyTextNode = emptyTextNodes.item(i);
            emptyTextNode.getParentNode().removeChild(emptyTextNode);
        }
    }

    /**
     * Updates XML content based on the provided parameters.
     *
     * @param bookID           The ID of the book to update.
     * @param title            The updated title.
     * @param author           The updated author.
     * @param publicationYear  The updated publication year.
     * @param languageCode     The updated language code.
     * @param availability     The updated availability status.
     * @throws ParserConfigurationException If a parser configuration error occurs.
     * @throws IOException                  If an I/O error occurs.
     * @throws SAXException                 If a SAX error occurs.
     * @throws XPathExpressionException     If an XPath expression error occurs.
     * @throws TransformerException         If a transformer error occurs.
     */
    public static void updateXML(String bookID, String title, String author, String publicationYear, String languageCode, boolean availability, int bookCopies) throws ParserConfigurationException, IOException, SAXException, XPathExpressionException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new File("res/book.xml"));
        cleanXMLDocument(doc);

        // Find the book node with the specified bookID
        XPath xPath = XPathFactory.newInstance().newXPath();
        String expression = String.format("//book[book_id/text()='%s']", bookID);
        Node bookNode = (Node) xPath.compile(expression).evaluate(doc, XPathConstants.NODE);

        if (bookNode != null && bookNode.getNodeType() == Node.ELEMENT_NODE) {
            Element bookElement = (Element) bookNode;

            // Update the book fields
            bookElement.getElementsByTagName("language_code").item(0).setTextContent(languageCode);
            bookElement.getElementsByTagName("original_title").item(0).setTextContent(title);
            bookElement.getElementsByTagName("authors").item(0).setTextContent(author);
            bookElement.getElementsByTagName("original_publication_year").item(0).setTextContent(publicationYear);
            bookElement.getElementsByTagName("availability").item(0).setTextContent(String.valueOf(availability).toUpperCase());
            bookElement.getElementsByTagName("book_copies").item(0).setTextContent(String.valueOf(bookCopies));

            // Write the updated document to file
            writeXMLFile(doc);

        } else {
            System.err.println("Error: Book with ID " + bookID + " not found.");
        }
    }

    /**
     * Deletes a book from XML based on the provided book ID.
     *
     * @param bookId The ID of the book to delete.
     * @throws ParserConfigurationException If a parser configuration error occurs.
     * @throws IOException                  If an I/O error occurs.
     * @throws SAXException                 If a SAX error occurs.
     * @throws XPathExpressionException     If an XPath expression error occurs.
     * @throws TransformerException         If a transformer error occurs.
     */
    public static void deleteBook(String bookId) throws ParserConfigurationException, IOException, SAXException, XPathExpressionException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new File("res/book.xml"));

        // Find the book node with the specified ID
        XPath xPath = XPathFactory.newInstance().newXPath();
        String expression = String.format("data/book[book_id='%s']", bookId);
        Node bookNode = (Node) xPath.compile(expression).evaluate(doc, XPathConstants.NODE);

        if (bookNode != null && bookNode.getNodeType() == Node.ELEMENT_NODE) {
            // Remove the book node from the document
            bookNode.getParentNode().removeChild(bookNode);

            // Write the updated document to file
            writeXMLFile(doc);
            System.out.println("Book with ID " + bookId + " deleted successfully");
        } else {
            System.err.println("Book with ID " + bookId + " not found");
        }
    }

    /**
     * Writes the XML document to a file after updating.
     *
     * @param doc The XML document to write.
     * @throws TransformerFactoryConfigurationError If a transformer factory configuration error occurs.
     * @throws TransformerException                 If a transformer error occurs.
     * @throws XPathExpressionException             If an XPath expression error occurs.
     */
    private static void writeXMLFile(Document doc) throws TransformerFactoryConfigurationError, TransformerException, XPathExpressionException {
        Transformer tf = TransformerFactory.newInstance().newTransformer();
        tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        tf.setOutputProperty(OutputKeys.INDENT, "yes");
        tf.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        DOMSource domSource = new DOMSource(doc);
        StreamResult sr = new StreamResult(new File("res/book.xml"));
        cleanXMLDocument(doc);
        tf.transform(domSource, sr);
        System.out.println("XML file updated successfully");

    }

    /**
     * Sets the highest book ID by parsing the "book.xml" file.
     *
     * @return A string representing the new highest book ID.
     */
    public static String setAsHighestBookId() {
        String highestBookId = "0";

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse("res/book.xml");
            NodeList bookNodes = document.getElementsByTagName("book");

            for (int i = 0; i < bookNodes.getLength(); i++) {
                Node bookNode = bookNodes.item(i);
                if (bookNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element bookElement = (Element) bookNode;
                    String bookId = bookElement.getElementsByTagName("book_id").item(0).getTextContent();

                    // Compare the current bookId with the highestBookId found so far
                    if (Integer.parseInt(bookId) > Integer.parseInt(highestBookId)) {
                        highestBookId = bookId;
                    }
                }
            }
        } catch (IOException | ParserConfigurationException | SAXException e) {
            System.out.println(e.getMessage());
        }
        int newHighestBookId = Integer.parseInt(highestBookId) + 1;
        return String.valueOf(newHighestBookId);
    }

    /**
     * Checks if a book ID already exists in the "book.xml" file.
     *
     * @param id The book ID to check for existence.
     * @return True if the book ID exists, false otherwise.
     * @throws ParserConfigurationException If a DocumentBuilder cannot be created.
     * @throws IOException                  If an IO error occurs while parsing the file.
     * @throws SAXException                 If any SAX errors occur during parsing.
     * @throws XPathExpressionException     If an error occurs during XPath expression evaluation.
     */
    public static boolean isBookIdExisting(String id) throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse("res/book.xml");
        doc.getDocumentElement().normalize();

        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();
        XPathExpression expr = xpath.compile("//book[book_id='" + id + "']");

        NodeList nodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
        return nodeList.getLength() > 0;
    }

}
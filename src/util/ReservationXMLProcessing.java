package util;

import org.w3c.dom.*;
import org.xml.sax.SAXException;
import shared.references.Reservation;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * The ReservationXMLProcessing class provides methods to write reservation information to an XML file.
 */
public class ReservationXMLProcessing {

    private static final File reservationXMLFile = new File("res/reservations.xml");

    public static ArrayList<Reservation> readReservationsFromXML() {
        ArrayList<Reservation> reservations = new ArrayList<>();

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(reservationXMLFile);
            doc.getDocumentElement().normalize();
            NodeList studentList = doc.getElementsByTagName("student");

            for (int i = 0; i < studentList.getLength(); i++) {
                Node studentNode = studentList.item(i);
                if (studentNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element studentElement = (Element) studentNode;
                    String studentId = studentElement.getAttribute("id");

                    NodeList reservationList = studentElement.getElementsByTagName("reservation");
                    for (int j = 0; j < reservationList.getLength(); j++) {
                        Node reservationNode = reservationList.item(j);
                        if (reservationNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element reservationElement = (Element) reservationNode;
                            String bookId = reservationElement.getElementsByTagName("book_id").item(0).getTextContent();
                            String reservationDate = reservationElement.getElementsByTagName("reservationDate").item(0).getTextContent();
                            String dueDate = reservationElement.getElementsByTagName("dueDate").item(0).getTextContent();
                            String approved = reservationElement.getElementsByTagName("approved").item(0).getTextContent();
                            String returnStatus = reservationElement.getElementsByTagName("returnStatus").item(0).getTextContent();
                            String pickUpStatus = reservationElement.getElementsByTagName("pickUpStatus").item(0).getTextContent();
                            String dateReturned = reservationElement.getElementsByTagName("dateReturned").item(0).getTextContent();
                            String overdueStatus = reservationElement.getElementsByTagName("overdueStatus").item(0).getTextContent();

                            reservations.add(new Reservation(studentId, String.valueOf(j), bookId, LocalDate.parse(reservationDate), LocalDate.parse(dueDate),
                                    Boolean.parseBoolean(approved), Boolean.parseBoolean(returnStatus), Boolean.parseBoolean(pickUpStatus),
                                    LocalDate.parse(dateReturned), Boolean.parseBoolean(overdueStatus)));
                        }
                    }
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            System.err.println("Error: " + e.getMessage());
        }
        return reservations;
    }

    /**
     * Removes empty text nodes from the XML document.
     *
     * @param doc The XML Document to clean.
     * @throws XPathExpressionException If an error occurs while evaluating XPath expressions.
     */
    public static void cleanXMLDocument(Document doc) throws XPathExpressionException {
        XPathFactory xpathFactory = XPathFactory.newInstance();

        XPathExpression xpathExp = xpathFactory
                .newXPath()
                .compile("//text()[normalize-space(.) = '']");
        NodeList emptyTextNodes = (NodeList) xpathExp
                .evaluate(doc, XPathConstants.NODESET);

        for (int i = 0; i < emptyTextNodes.getLength(); i++) {
            Node emptyTextNode = emptyTextNodes.item(i);
            emptyTextNode.getParentNode().removeChild(emptyTextNode);
        }
    }

    /**
     * Deletes a reservation from the XML file based on the provided student ID and book ID.
     *
     * @param studentID The ID of the student making the reservation.
     * @param bookID    The ID of the book being reserved.
     */
    public static void deleteReservationFromXML(String studentID, String bookID, String index) {
        try {
            int i = Integer.parseInt(index);

            if (studentID == null || studentID.isEmpty() || bookID == null || bookID.isEmpty()) {
                System.out.println("deleteReservationFromXML: Student ID or Book ID is null or empty");
                return;
            }

            File xmlFile = new File("res/reservations.xml");
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(xmlFile);
            XPathFactory xpathFactory = XPathFactory.newInstance();
            XPath xpath = xpathFactory.newXPath();

            // Find the student nodes
            NodeList studentNodes = doc.getElementsByTagName("student");

            for (int studentIndex = 0; studentIndex < studentNodes.getLength(); studentIndex++) {
                Element student = (Element) studentNodes.item(studentIndex);
                String studentIdAttr = student.getAttribute("id");

                if (studentID.equals(studentIdAttr)) {
                    // Find the reservation elements within the student node
                    NodeList reservations = student.getElementsByTagName("reservation");

                    if (i >= 0 && i < reservations.getLength()) {
                        // Remove the reservation node at the specified index
                        Node reservationNode = reservations.item(i);
                        reservationNode.getParentNode().removeChild(reservationNode);

                        TransformerFactory transformerFactory = TransformerFactory.newInstance();
                        Transformer transf = transformerFactory.newTransformer();

                        transf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                        transf.setOutputProperty(OutputKeys.INDENT, "yes");
                        transf.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

                        DOMSource source = new DOMSource(doc);
                        cleanXMLDocument(doc);
                        StreamResult result = new StreamResult(xmlFile);
                        cleanXMLDocument(doc);
                        transf.transform(source, result);

                        System.out.println("Reservation with Student ID " + studentID + " and Book ID " + bookID + " at index " + i + " has been deleted.");
                        return;
                    }
                }
            }

            // If the loop completes without finding the reservation, print a message
            System.out.println("Reservation not found for Student ID: " + studentID + ", Book ID: " + bookID + ", Index: " + index);

        } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException | DOMException |
                 TransformerFactoryConfigurationError | IllegalArgumentException | TransformerException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException(e);
        }
    }

}
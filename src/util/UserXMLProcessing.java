package util;

import org.w3c.dom.*;
import org.xml.sax.SAXException;
import shared.references.User;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * The UserXMLProcessing class provides utility methods to write user data to an XML file.
 */
public class UserXMLProcessing {

    private static final File userXMLFile = new File("res/users.xml");

    public static ArrayList<User> readUsersFromXML() {
        ArrayList<User> users = new ArrayList<>();
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new File("res/users.xml"));
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName("user");

            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    String type = element.getAttribute("type");

                    Node node1 = element.getElementsByTagName("name").item(0);
                    String name = node1.getTextContent();

                    Node node2 = element.getElementsByTagName("email").item(0);
                    String email = node2.getTextContent();

                    Node node3 = element.getElementsByTagName("password").item(0);
                    String password = node3.getTextContent();

                    Node node4 = element.getElementsByTagName("id").item(0);
                    String id = node4.getTextContent();

                    users.add(new User(type, name, email, password, id));
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            System.err.println("Error: " + e.getMessage());
        }
        return users;
    }

    /**
     * Creates a new user node in the XML document.
     *
     * @param doc      The XML document
     * @param type     The type of user
     * @param name     The name of the user
     * @param email    The email of the user
     * @param password The password of the user
     * @param id       The ID of the user
     * @return A new user node
     */
    public static Node createUser(Document doc, String type, String name, String email, String password, String id) {
        Element user = doc.createElement("user");

        user.setAttribute("type", type);
        user.appendChild(createElement(doc, "name", name));
        user.appendChild(createElement(doc, "email", email));
        user.appendChild(createElement(doc, "password", password));
        user.appendChild(createElement(doc, "id", id));

        return user;
    }

    /**
     * Adds a new user to the XML file.
     *
     * @param type     The type of user
     * @param name     The name of the user
     * @param email    The email of the user
     * @param password The password of the user
     * @param id       The ID of the user
     */
    @SuppressWarnings("Duplicates")
    public static void addUser(String type, String name, String email, String password, String id) throws ParserConfigurationException, SAXException, IOException, TransformerException, XPathExpressionException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(userXMLFile);

        Element root = doc.getDocumentElement();
        root.appendChild(createUser(doc, type, name, email, password, id));

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transf = transformerFactory.newTransformer();

        transf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transf.setOutputProperty(OutputKeys.INDENT, "yes");
        transf.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        DOMSource source = new DOMSource(doc);
        cleanXMLDocument(doc);
        StreamResult result = new StreamResult(userXMLFile);
        transf.transform(source, result);
    }

    public static Node createElement(Document doc, String name, String value) {
        Element node = doc.createElement(name);
        node.appendChild(doc.createTextNode(value));

        return node;
    }


    /**
     * Updates the user data in the XML file.
     *
     * @param type     The updated type of user
     * @param name     The updated name of the user
     * @param email    The updated email of the user
     * @param password The updated password of the user
     * @param id       The updated ID of the user
     */
    public static void updateXML(String type, String name, String email, String password, String id) {
        try {
            // Load existing XML file
            File xmlFile = new File("res/users.xml");
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(xmlFile);



            // Find the user node with the specified old ID
            XPath xPath = XPathFactory.newInstance().newXPath();
            String expression = String.format("/account/user[id='%s']", id);
            System.out.println("XPath Expression: " + expression); // Debug statement
            Node userNode = (Node) xPath.compile(expression).evaluate(doc, XPathConstants.NODE);

            if (userNode != null && userNode.getNodeType() == Node.ELEMENT_NODE) {
                Element userElement = (Element) userNode;

                // Update the user attributes
                userElement.setAttribute("type", type);
                NodeList childNodes = userElement.getChildNodes();
                for (int i = 0; i < childNodes.getLength(); i++) {
                    Node node = childNodes.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = (Element) node;
                        String tagName = element.getTagName();
                        switch (tagName) {
                            case "id":
                                element.setTextContent(id);
                                break;
                            case "name":
                                element.setTextContent(name);
                                break;
                            case "email":
                                element.setTextContent(email);
                                break;
                            case "password":
                                element.setTextContent(password);
                                break;
                        }
                    }
                }

                // Write the updated document to file
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.transform(new DOMSource(doc), new StreamResult(System.out));

                DOMSource source = new DOMSource(doc);
                StreamResult result = new StreamResult(xmlFile);
                transformer.transform(source, result);

            } else {
                UserXMLProcessing.addUser(type, name, email, password, id);
            }
        } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException | TransformerException | DOMException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Deletes a user from the XML file.
     *
     * @param id The ID of the user to be deleted
     */
    public static void deleteUser(String id)  {
        try {
            DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
            DocumentBuilder b = f.newDocumentBuilder();
            Document doc = b.parse(new File("res/users.xml"));

            // Find the user node with the specified ID
            XPath xPath = XPathFactory.newInstance().newXPath();
            String expression = String.format("/account/user[id='%s']", id);
            Node userNode = (Node) xPath.compile(expression).evaluate(doc, XPathConstants.NODE);

            if (userNode != null && userNode.getNodeType() == Node.ELEMENT_NODE) {
                // Remove the user node from the document
                userNode.getParentNode().removeChild(userNode);

                // Write the updated document to file
                writeXMLFile(doc);
                System.out.println("User with ID " + id + " deleted successfully");
            } else {
                System.err.println("User with ID " + id + " not found");
            }
        } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException | DOMException |
                 TransformerFactoryConfigurationError e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * Writes the updated XML document to file.
     *
     * @param doc The XML document
     */
    private static void writeXMLFile(Document doc) {
        try {
            cleanXMLDocument(doc);

            Transformer tf = TransformerFactory.newInstance().newTransformer();

            tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            tf.setOutputProperty(OutputKeys.INDENT, "yes");
            tf.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

            DOMSource domSource = new DOMSource(doc);

            StreamResult sr = new StreamResult(new File("res/users.xml"));

            tf.transform(domSource, sr);
            System.out.println("XML file updated successfully");
        } catch (TransformerFactoryConfigurationError | TransformerException e) {
            System.err.println("Error transforming XML: " + e.getMessage());
        } catch (Exception e) {
            System.err.println("Unexpected error: " + e.getMessage());
        }
    }


    /**
     * Removes empty text nodes from the document.
     *
     * @param doc The XML document
     */
    private static void cleanXMLDocument(Document doc) {
        try {
            XPathFactory xpathFactory = XPathFactory.newInstance();

            XPathExpression xpathExp = xpathFactory
                    .newXPath()
                    .compile("//text()[normalize-space(.) = '']");
            NodeList emptyTextNodes = (NodeList) xpathExp
                    .evaluate(doc, XPathConstants.NODESET);

            for (int i = 0; i < emptyTextNodes.getLength(); i++) {
                Node emptyTextNode = emptyTextNodes.item(i);
                emptyTextNode.getParentNode().removeChild(emptyTextNode);
            }
        } catch (XPathExpressionException | DOMException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Reads user data from an XML file and returns a NodeList containing the user data.
     *
     * @param id The user ID to be checked.
     * @return true if the user ID exists in the XML file, false otherwise.
     */
    public static boolean isIdExisting(String id) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(userXMLFile);
            doc.getDocumentElement().normalize();

            XPathFactory xPathfactory = XPathFactory.newInstance();
            XPath xpath = xPathfactory.newXPath();
            XPathExpression expr = xpath.compile("//user[id='" + id + "']");

            NodeList nodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
            return nodeList.getLength() > 0;
        } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Reads user data from an XML file and returns a NodeList containing the user data.
     *
     * @param email The user email to be checked.
     * @return true if the user email exists in the XML file, false otherwise.
     */
    public static boolean isEmailExisting(String email) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(userXMLFile);
            doc.getDocumentElement().normalize();

            XPathFactory xPathfactory = XPathFactory.newInstance();
            XPath xpath = xPathfactory.newXPath();
            XPathExpression expr = xpath.compile("//user[email='" + email + "']");

            NodeList nodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
            return nodeList.getLength() > 0;
        } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException e) {
            throw new RuntimeException(e);
        }
    }
}

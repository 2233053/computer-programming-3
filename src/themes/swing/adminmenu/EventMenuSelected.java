package themes.swing.adminmenu;

public interface EventMenuSelected {

    public void menuSelected(int index, int indexSubMenu);
}

package themes.swing.manager;

import run.Application;


import javax.swing.*;
import java.awt.*;

public class FormsManager {

    private static FormsManager instance;

    public static FormsManager getInstance() {
        if (instance == null) {
            instance = new FormsManager();
        }
        return instance;
    }

    private FormsManager() {

    }

    public void initApplication(Application application) {
    }



    public void showComponent(JPanel body, Component com) {
        body.removeAll();
        body.add(com);
        body.repaint();
        body.revalidate();
    }



}

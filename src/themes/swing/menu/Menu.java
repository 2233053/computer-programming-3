package themes.swing.menu;


import javaswingdev.GoogleMaterialDesignIcon;
import javaswingdev.GoogleMaterialIcon;
import javaswingdev.GradientType;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.animation.timing.Animator;
import org.jdesktop.animation.timing.TimingTargetAdapter;
import themes.system.SystemColor;
import themes.swing.scroll.ScrollBar;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.util.ArrayList;
import java.util.List;


public class Menu extends JPanel {

    private MigLayout menuLayout;
    private JPanel panelMenu;

    private int index = -1;
    private final List<EventMenuSelected> events = new ArrayList<>();

    public Menu(String studentID) {
        init();
        addMenuItem(new ModelMenuItem(GoogleMaterialDesignIcon.PERSON, studentID));
    }

    private void init() {
        setBackground(Color.decode("#04151f"));
        setForeground(Color.decode("#eff6e0"));
        setLayout(new BorderLayout());
        JScrollPane scroll = createScroll();
        panelMenu = createPanelMenu();
        scroll.setViewportView(panelMenu);
        scroll.getViewport().setOpaque(false);
        scroll.setViewportBorder(null);
        add(scroll);
        addTitle("Book Reservation");
        addMenuItem(new ModelMenuItem(GoogleMaterialDesignIcon.INPUT, "Reserve a Book" ));
        addMenuItem(new ModelMenuItem(GoogleMaterialDesignIcon.INBOX, "Reservations"));
        addMenuItem(new ModelMenuItem(GoogleMaterialDesignIcon.BOOK, "Borrowed"));
        addMenuItem(new ModelMenuItem(GoogleMaterialDesignIcon.KEYBOARD_RETURN, "Returned"));
    }

    private JScrollPane createScroll() {
        JScrollPane scroll = new JScrollPane();
        scroll.setBorder(null);
        scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scroll.setVerticalScrollBar(new ScrollBar());
        return scroll;
    }

    private JPanel createPanelMenu() {
        JPanel panel = new JPanel();
        panel.setOpaque(false);
        menuLayout = new MigLayout("wrap,fillx,inset 0,gapy 0", "[fill]");
        panel.setLayout(menuLayout);
        return panel;
    }

    private JPanel createMenuItem(ModelMenuItem item) {
        MenuItem menuItem = new MenuItem(item, ++index, menuLayout);
        menuItem.addEvent(new EventMenuSelected() {
            @Override
            public void menuSelected(int index, int indexSubMenu) {
                if (!menuItem.isHasSubMenu() || indexSubMenu != 0) {
                    clearSelected();
                    setSelectedIndex(index, indexSubMenu);
                }
            }
        });
        return menuItem;
    }

    private void runEvent(int index, int indexSubMenu) {
        for (EventMenuSelected event : events) {
            event.menuSelected(index, indexSubMenu);
        }
    }

    //  Public Method
    public void addMenuItem(ModelMenuItem menu) {
        panelMenu.add(createMenuItem(menu), "h 35!");
    }

    public void addTitle(String title) {
        JLabel label = new JLabel(title);
        label.setBorder(new EmptyBorder(15, 20, 5, 5));
        label.setFont(label.getFont().deriveFont(Font.BOLD));
        label.setForeground(new Color(170, 170, 170));
        panelMenu.add(label);
    }

    public void addSpace(int size) {
        panelMenu.add(new JLabel(), "h " + size + "!");
    }

    public void setSelectedIndex(int index, int indexSubMenu) {
        for (Component com : panelMenu.getComponents()) {
            if (com instanceof MenuItem) {
                MenuItem item = (MenuItem) com;
                if (item.getIndex() == index) {
                    item.setSelectedIndex(indexSubMenu);
                    runEvent(index, indexSubMenu);
                    break;
                }
            }
        }
    }

    public void clearSelected() {
        for (Component com : panelMenu.getComponents()) {
            if (com instanceof MenuItem) {
                MenuItem item = (MenuItem) com;
                item.clearSelected();
            }
        }
    }

    public void addEvent(EventMenuSelected event) {
        events.add(event);
    }

}

class MenuItem extends JPanel {

    private final List<EventMenuSelected> events = new ArrayList<>();
    private final int index;
    private final boolean hasSubMenu;
    private Animator animator;
    private int buttonAngle = -1;
    private boolean open;

    public MenuItem(ModelMenuItem item, int index, MigLayout layout) {
        this.index = index;
        this.hasSubMenu = item.getSubMenu().length > 0;
        init(item);
        if (hasSubMenu) {
            initAnimator(layout);
            buttonAngle = 0;
        }
    }

    private void init(ModelMenuItem item) {
        setOpaque(false);
        setForeground(Color.decode("#eff6e0"));
        setLayout(new MigLayout("wrap,fillx,inset 0", "[fill]", "[fill,35!]" + (hasSubMenu ? "0[fill,30!]" : "")));
        Item menu = getItem(item);
        add(menu);
        int subIndex = 0;
        for (String subMenu : item.getSubMenu()) {
            Item sMenu = new Item(false, ++subIndex);
            sMenu.setText(subMenu);
            sMenu.addActionListener(arg0 -> runEvent(index, sMenu.getIndex()));
            add(sMenu);
        }
    }

    private Item getItem(ModelMenuItem item) {
        Item menu = new Item(true, 0);
        menu.setGoogleIcon(item.getIcon());
        menu.setText("  " + item.getMenuName());
        menu.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                setForeground(menu.getMainColor());
            }

            @Override
            public void mouseExited(MouseEvent e) {
                if (!menu.isSelected()) {
                    setForeground(new Color(170, 170, 170));
                }
            }
        });
        menu.addActionListener(arg0 -> runEvent(index, 0));
        if (hasSubMenu) {
            menu.addActionListener(arg0 -> {
                open = !open;
                startAnimator();
            });
        }
        return menu;
    }

    private void initAnimator(MigLayout layout) {
        animator = new Animator(300, new TimingTargetAdapter() {
            private int height;

            @Override
            public void begin() {
                height = getPreferredSize().height - 35;
            }

            @Override
            public void timingEvent(float fraction) {
                float f = open ? fraction : 1f - fraction;
                int s = (int) (35 + f * height);
                layout.setComponentConstraints(MenuItem.this, "h " + s + "!");
                buttonAngle = (int) (f * 180);
                revalidate();
                repaint();
            }
        });
        animator.setResolution(0);
        animator.setDeceleration(.5f);
        animator.setAcceleration(.5f);
    }

    private void startAnimator() {
        if (animator.isRunning()) {
            float f = animator.getTimingFraction();
            animator.stop();
            animator.setStartFraction(1f - f);
        } else {
            animator.setStartFraction(0f);
        }
        animator.start();
    }

    public void addEvent(EventMenuSelected event) {
        this.events.add(event);
    }

    private void runEvent(int index, int subIndex) {
        for (EventMenuSelected event : events) {
            event.menuSelected(index, subIndex);
        }
    }

    public int getIndex() {
        return index;
    }

    public boolean isHasSubMenu() {
        return hasSubMenu;
    }

    public void clearSelected() {
        setForeground(Color.decode("#eff6e0"));
        for (Component com : getComponents()) {
            Item item = (Item) com;
            item.setSelected(false);
        }
    }

    public void setSelectedIndex(int index) {
        for (Component com : getComponents()) {
            Item item = (Item) com;
            if (item.isMainMenu()) {
                item.setSelected(true);
                setForeground(item.getMainColor());
            }
            if (item.getIndex() == index) {
                item.setSelected(true);
                break;
            }
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (buttonAngle >= 0) {
            Graphics2D g2 = (Graphics2D) g.create();
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setColor(getForeground());
            int x = getWidth() - 25;
            int y = 15;
            Path2D p2 = new Path2D.Double();
            p2.moveTo(x, y);
            p2.lineTo(x + 4, y + 4);
            p2.lineTo(x + 8, y);
            AffineTransform at = AffineTransform.getRotateInstance(Math.toRadians(buttonAngle), x + 4, y + 2);
            g2.setStroke(new BasicStroke(1.8f));
            g2.draw(at.createTransformedShape(p2));
            g2.dispose();
        }
    }
}
class Item extends JButton {

    private final Color mainColor = themes.system.SystemColor.MAIN_COLOR_2;
    private final int index;
    private Animator animator;
    private GoogleMaterialDesignIcon icon;
    private final boolean mainMenu;
    private boolean mouseEnter;
    private float alpha;

    public Item(boolean mainMenu, int index) {
        this.mainMenu = mainMenu;
        this.index = index;
        init();
    }

    private void init() {
        setContentAreaFilled(false);
        setHorizontalAlignment(JButton.LEFT);
        setForeground(Color.decode("#eff6e0"));
        if (mainMenu) {
            setBorder(new EmptyBorder(0, 20, 0, 0));
        } else {
            setBorder(new EmptyBorder(0, 51, 0, 0));
        }
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                setForeground(mainColor);
                setGoogleIcon(icon);
                if (!mainMenu) {
                    mouseEnter = true;
                    startAnimator();
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                if (!isSelected()) {
                    setForeground(Color.decode("#eff6e0"));
                    setGoogleIcon(icon);
                }
                if (!mainMenu) {
                    mouseEnter = false;
                    startAnimator();
                }
            }
        });
        if (!mainMenu) {
            animator = new Animator(350, new TimingTargetAdapter() {
                @Override
                public void timingEvent(float fraction) {
                    alpha = mouseEnter ? fraction : 1f - fraction;
                    repaint();
                }
            });
            animator.setResolution(0);
            animator.setAcceleration(.5f);
            animator.setDeceleration(.5f);
        }
    }

    private void startAnimator() {
        if (animator.isRunning()) {
            float f = animator.getTimingFraction();
            animator.stop();
            animator.setStartFraction(1f - f);
        } else {
            animator.setStartFraction(0f);
        }
        animator.start();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (!mainMenu) {
            Graphics2D g2 = (Graphics2D) g.create();
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setColor(new Color(170, 170, 170));
            int height = getHeight();
            int size = 6;
            int y = (height - size) / 2;
            g2.drawOval(27, y, size, size);
            g2.setColor(mainColor);
            if (isSelected()) {
                alpha = 1;
            }
            g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha));
            g2.fillOval(27, y, size + 1, size + 1);
            g2.dispose();
        } else {
            if (isSelected()) {
                Graphics2D g2 = (Graphics2D) g.create();
                g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                g2.setPaint(new GradientPaint(0, 3, themes.system.SystemColor.MAIN_COLOR_1, 3, getHeight() - 6, themes.system.SystemColor.MAIN_COLOR_2));
                g2.fillRect(0, 3, 3, getHeight() - 6);
                g2.dispose();
            }
        }
    }

    public void setGoogleIcon(GoogleMaterialDesignIcon icon) {
        if (icon != null) {
            this.icon = icon;
            setIcon(new GoogleMaterialIcon(icon, GradientType.HORIZONTAL, themes.system.SystemColor.MAIN_COLOR_1, SystemColor.MAIN_COLOR_2, 19).toIcon());
        }
    }

    @Override
    public void setSelected(boolean b) {
        super.setSelected(b);
        if (b || mouseEnter) {
            setForeground(mainColor);
        } else {
            alpha = 0;
            setForeground(Color.decode("#eff6e0"));
        }
        setGoogleIcon(icon);
    }

    public Color getMainColor() {
        return mainColor;
    }

    public int getIndex() {
        return index;
    }

    public boolean isMainMenu() {
        return mainMenu;
    }
}
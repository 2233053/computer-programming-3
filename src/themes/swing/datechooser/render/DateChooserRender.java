package themes.swing.datechooser.render;


import themes.swing.datechooser.date.DateChooser;

import java.util.Date;

public interface DateChooserRender {
    public String renderLabelCurrentDate(DateChooser dateChooser, Date date);

    public String renderTextFieldDate(DateChooser dateChooser, Date date);


    public String renderDateCell(DateChooser dateChooser, Date date);
}

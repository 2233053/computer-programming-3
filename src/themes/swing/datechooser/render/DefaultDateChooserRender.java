package themes.swing.datechooser.render;


import themes.swing.datechooser.date.DateChooser;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DefaultDateChooserRender implements DateChooserRender {


    @Override
    public String renderLabelCurrentDate(DateChooser dateChooser, Date date) {
        return "Today : " + dateChooser.getDateFormat().format(date);
    }

    @Override
    public String renderTextFieldDate(DateChooser dateChooser, Date date) {
        return dateChooser.getDateFormat().format(date);
    }

    @Override
    public String renderDateCell(DateChooser dateChooser, Date date) {
        DateFormat df = new SimpleDateFormat("d");
        return df.format(date);
    }
}

package themes.swing.datechooser.listener;

import java.util.Date;

public interface DateChooserListener {
    public void dateChanged(Date date, DateChooserAction action);

    void dateChanged(Date date, com.raven.datechooser.listener.DateChooserAction action);
}

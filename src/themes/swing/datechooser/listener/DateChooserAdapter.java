package themes.swing.datechooser.listener;

import com.raven.datechooser.listener.DateChooserAction;

import java.util.Date;

public abstract class DateChooserAdapter implements DateChooserListener{

    @Override
    public void dateChanged(Date date, DateChooserAction action) {
    }

}

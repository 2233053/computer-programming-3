package themes.swing.datechooser.date;

import java.util.Date;

public interface DateSelectable {

    public boolean isDateSelectable(Date date);
}

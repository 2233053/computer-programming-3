package themes.swing.datechooser.date;

public class DateChooserException extends RuntimeException {

    public DateChooserException(String errorMessage) {
        super(errorMessage);
    }
}

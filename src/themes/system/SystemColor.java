package themes.system;

import java.awt.*;

public class SystemColor {

    public final static Color MAIN_COLOR_1 = Color.decode("#8f94fb");
    public final static Color MAIN_COLOR_2 = Color.decode("#b5e2fa");
}

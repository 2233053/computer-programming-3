package shared;

public enum AuthenticationStatus {
    USER_NOT_FOUND,
    PASSWORD_INCORRECT,
    SUCCESS_CLIENT,
    SUCCESS_ADMIN
}

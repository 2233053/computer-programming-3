package shared.references;

import java.io.Serializable;

/**
 * The History class represents a historical record of a student borrowing a book.
 * It contains information about the student and the book.
 */

public class History implements Serializable {
    private String studentId;

    private String bookId;

    /**
     * Constructs a new History object with the given student ID and book ID.
     * @param studentId The ID of the student who borrowed the book.
     * @param bookId The ID of the book borrowed by the student.
     */

    public History(String studentId, String bookId) {
        this.studentId = studentId;
        this.bookId = bookId;
    }

    /**
     * Retrieves the ID of the student who borrowed the book.
     * @return The ID of the student.
     */

    public String getStudentId() {
        return studentId;
    }

    /**
     * Sets the ID of the student who borrowed the book.
     * @param studentId The ID of the student.
     */

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }


    /**
     * Retrieves the ID of the book borrowed by the student.
     * @return The ID of the book.
     */

    public String getBookId() {
        return bookId;
    }

    /**
     * Sets the ID of the book borrowed by the student.
     * @param bookId The ID of the book.
     */

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

}

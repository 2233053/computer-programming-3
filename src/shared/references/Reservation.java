package shared.references;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * The Reservation class represents a reservation in a library system. <br>
 */
public class Reservation implements Serializable {

    private String studentId;

    private  String uId;
    private String bookId;
    private LocalDate reservationDate;
    private LocalDate dueDate;
    private boolean approved;
    private boolean returnStatus;
    private boolean pickUpStatus;

    private LocalDate dateReturned;
    private boolean overdueStatus;

    // Constructors

    /**
     * Constructor for Reservation
     *
     * @param studentId       Student ID
     * @param bookId          Book ID
     * @param reservationDate Reservation Date
     * @param dueDate         Due Date
     * @param approved        Approval Status
     * @param returnStatus    Return Status
     * @param pickUpStatus    Pick Up Status
     */
    public Reservation(String studentId, String bookId, LocalDate reservationDate, LocalDate dueDate, boolean approved, boolean returnStatus, boolean pickUpStatus) {
        this.studentId = studentId;
        this.bookId = bookId;
        this.reservationDate = reservationDate;
        this.dueDate = dueDate;
        this.approved = approved;
        this.returnStatus = returnStatus;
        this.pickUpStatus = pickUpStatus;
    }

    /**
     * Constructor for Reservation
     *
     * @param studentId       Student ID
     * @param bookId          Book ID
     * @param reservationDate Reservation Date
     * @param dueDate         Due Date
     * @param approved        Approval Status
     * @param returnStatus    Return Status
     * @param pickUpStatus    Pick Up Status
     * @param dateReturned    Returned Date
     * @param overdueStatus   Overdue status
     */
    public Reservation(String studentId, String uId, String bookId, LocalDate reservationDate, LocalDate dueDate, boolean approved, boolean returnStatus, boolean pickUpStatus, LocalDate dateReturned, boolean overdueStatus) {
        this.studentId = studentId;
        this.uId = uId;
        this.bookId = bookId;
        this.reservationDate = reservationDate;
        this.dueDate = dueDate;
        this.approved = approved;
        this.returnStatus = returnStatus;
        this.pickUpStatus = pickUpStatus;
        this.dateReturned = dateReturned;
        this.overdueStatus = overdueStatus;
    }

    /**
     * Returns the return status of the book reservation, <br>
     *
     * @return the returnStatus
     */
    public boolean isReturnStatus() {
        return returnStatus;
    }

    /**
     * Sets the return status of the reservation
     *
     * @param returnStatus the returnStatus to set
     */
    public void setReturnStatus(boolean returnStatus) {
        this.returnStatus = returnStatus;
    }

    /**
     * Returns the pickup status of the book reservation, <br>
     *
     * @return the pickUpStatus
     */
    public boolean isPickUpStatus() {
        return pickUpStatus;
    }

    /**
     * Sets the pickup status of the reservation
     *
     * @param pickUpStatus the pickUpStatus to set
     */
    public void setPickUpStatus(boolean pickUpStatus) {
        this.pickUpStatus = pickUpStatus;
    }

    public String getUId() {
        return uId;
    }

    public void setUId(String uId) {
        this.uId = uId;
    }

    /**
     * Returns the studentId of the reservation
     *
     * @return the studentId
     */
    public String getStudentId() {
        return studentId;
    }

    /**
     * Sets the studentId of the reservation
     *
     * @param studentId the studentId to set
     */
    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    /**
     * Returns the bookId of the reservation
     *
     * @return the bookId
     */
    public String getBookId() {
        return bookId;
    }

    /**
     * Sets the bookId of the reservation
     *
     * @param bookId the bookId to set
     */
    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    /**
     * Returns the reservationDate of the reservation
     *
     * @return the reservationDate
     */
    public LocalDate getReservationDate() {
        return reservationDate;
    }

    /**
     * Sets the reservationDate of the reservation
     *
     * @param reservationDate the reservationDate to set
     */
    public void setReservationDate(LocalDate reservationDate) {
        this.reservationDate = reservationDate;
    }

    /**
     * Returns the approval status of the reservation
     *
     * @return the approved
     */
    public boolean isApproved() {
        return approved;
    }

    /**
     * Sets the approval status of the reservation
     *
     * @param approved the approved to set
     */
    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    /**
     * Returns the due date of the reservation
     *
     * @return the dueDate
     */
    public LocalDate getDueDate() {
        return dueDate;
    }

    /**
     * Sets the due date of the reservation
     *
     * @param dueDate the dueDate to set
     */
    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public LocalDate getDateReturned() {
        return dateReturned;
    }

    public void setDateReturned(LocalDate dateReturned) {
        this.dateReturned = dateReturned;
    }

    public boolean isOverdue() {
        return overdueStatus;
    }

    public void setOverdueStatus(boolean overdueStatus) {
        this.overdueStatus = overdueStatus;
    }

    /**
     * Returns the String representation of the Reservation
     *
     * @return String representation of the Reservation
     */
    @Override
    public String toString() {
        return String.format(
                """
                        StudentID: %s
                        ReservationID: %s
                        BookID: %s
                        Reservation Date: %s
                        Due Date: %s
                        Approved: %s
                        Returned: %s
                        Picked Up: %s
                        Date Returned: %s
                        Overdue: %s
                        """,
                studentId,
                uId,
                bookId,
                reservationDate,
                dueDate,
                approved,
                returnStatus,
                pickUpStatus,
                dateReturned,
                overdueStatus
        );
    }
}


package shared.references;

import java.io.Serializable;

/**
 * The User class represents a user in the system.
 * It implements Serializable to support serialization.
 */
@SuppressWarnings("unused")
public class User implements Serializable {
    private String type;
    private String name;
    private String email;
    private String password;
    private String ID;

    /**
     * Constructs a new User with the specified type, name, email, password, and ID.
     *
     * @param type     The type of the user.
     * @param name     The name of the user.
     * @param email    The email of the user.
     * @param password The password of the user.
     * @param ID       The unique ID of the user.
     */
    public User(String type, String name, String email, String password, String ID) {
        this.type = type;
        this.name = name;
        this.email = email;
        this.password = password;
        this.ID = ID;
    }
    /**
     * Gets the name of the user.
     *
     * @return The name of the user.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the user.
     *
     * @param name The new name of the user.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the email of the user.
     *
     * @return The email of the user.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email of the user.
     *
     * @param email The new email of the user.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets the password of the user.
     *
     * @return The password of the user.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password of the user.
     *
     * @param password The new password of the user.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets the ID of the user.
     *
     * @return The ID of the user.
     */
    public String getID() {
        return ID;
    }

    /**
     * Sets the ID of the user.
     *
     * @param ID The new ID of the user.
     */
    public void setID(String ID) {
        this.ID = ID;
    }

    /**
     * Returns a string representation of the User object.
     *
     * @return A string representation of the User object.
     */
    public String toString() {
        return String.format("\t %-6s \t %-10s \t %-20s \t %-10s \t %s",
                type, name, email, password, ID);
    }

    /**
     * Gets the type of the user.
     *
     * @return The type of the user.
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the type of the user.
     *
     * @param type The new type of the user.
     */
    public void setType(String type) {
        this.type = type;
    }
}

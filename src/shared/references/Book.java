package shared.references;

import java.io.Serializable;


/**
 * The Book class represents a book in a library system. <br>
 * Each book has an ID, title, author, publication year, language code, and availability status. <br>
 * The book ID is unique for each book and is used as the primary identifier. <br>
 * The availability status indicates whether the book is currently available for borrowing. <br>
 */
public class Book implements Serializable {
    private String bookID;
    private String title;
    private String author;
    private String publicationYear;
    private String languageCode;
    private boolean availability;

    private int bookCopies;

    /**
     * Constructor for Book
     *
     * @param bookID          Book ID of the book
     * @param title           Title
     * @param author          Author
     * @param publicationYear Publication Year
     * @param languageCode    Language Code
     * @param availability    Availability
     * @param bookCopies      Book Copies
     */
    public Book(String bookID, String title, String author, String publicationYear, String languageCode, boolean availability, int bookCopies) {
        this.bookID = bookID;
        this.title = title;
        this.author = author;
        this.publicationYear = publicationYear;
        this.languageCode = languageCode;
        this.availability = availability;
        this.bookCopies = bookCopies;
    }

    // temp constructor for books no copies param
    public Book(String bookID, String title, String author, String publicationYear, String languageCode, boolean availability) {
        this.bookID = bookID;
        this.title = title;
        this.author = author;
        this.publicationYear = publicationYear;
        this.languageCode = languageCode;
        this.availability = availability;
    }


    /**
     * Constructor for a Book object where bookId is not given <br>
     * Used for adding new books. BookId is shall be incremented from the highest bookId in the system
     *
     * @param title           Title
     * @param author          Author
     * @param publicationYear Publication Year
     * @param languageCode    Language Code
     * @param availability    Availability
     * @param bookCopies      Book Copies
     */
    public Book(String title, String author, String publicationYear, String languageCode, boolean availability, int bookCopies) {
        this.bookID = null;
        this.title = title;
        this.author = author;
        this.publicationYear = publicationYear;
        this.languageCode = languageCode;
        this.availability = availability;
        this.bookCopies = bookCopies;
    }

    public Book() {
        this.bookID = null;
        this.title = null;
        this.author = null;
        this.publicationYear = null;
        this.languageCode = null;
        this.availability = false;
        this.bookCopies = 0;
    }


    /**
     * Returns BookID
     *
     * @return bookId
     */
    public String getBookID() {
        return bookID;
    }

    /**
     * Set BookID
     *
     * @param bookID Book ID
     */
    public void setBookID(String bookID) {
        this.bookID = bookID;
    }

    /**
     * Returns availability of the book
     *
     * @return availability
     */
    public boolean isAvailable() {
        return availability;
    }

    /**
     * Returns title of the book
     *
     * @return Title of the book
     */
    public String getTitle() {
        return title;
    }

    /**
     * Set title of the book
     *
     * @param title Title of the book
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Returns author of the book
     *
     * @return Author of the book
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Set author of the book
     *
     * @param author Author of the book
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * Returns publication year of the book
     *
     * @return Publication Year
     */
    public String getPublicationYear() {
        return publicationYear;
    }

    /**
     * Set publication year of the book
     *
     * @param publicationYear Publication Year
     */
    public void setPublicationYear(String publicationYear) {
        this.publicationYear = publicationYear;
    }

    /**
     * Returns language code of the book
     *
     * @return Language Code
     */
    public String getLanguageCode() {
        return languageCode;
    }

    /**
     * Set language code of the book
     *
     * @param languageCode Language Code
     */
    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    /**
     * Returns availability of the book
     *
     * @return Availability of the book
     */
    public boolean getAvailability() {
        return availability;
    }

    /**
     * Set availability of the book
     *
     * @param availability Availability of the book
     */
    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    public int getBookCopies() {
        return bookCopies;
    }

    public void setBookCopies(int bookCopies) {
        this.bookCopies = bookCopies;
    }

    /**
     * Returns String representation of the book
     *
     * @return String representation of the book
     */
    @Override
    public String toString() {
        return String.format("%-5s\n%-5s\n%-5s\n%-5s\n%-5s\n%-5s\n%-5d\n",
                bookID, title, author, publicationYear, languageCode, availability, bookCopies);
    }

    public static String[] languageCodes = {
            "aar", "abk", "ace", "ach", "ada", "ady", "afa", "afh", "afr", "ain",
            "aka", "akk", "alb", "ale", "alg", "alt", "amh", "ang", "anp", "apa",
            "ara", "arc", "arg", "arm", "arn", "arp", "arw", "asm", "ast", "ath",
            "ava", "ave", "awa", "aym", "aze", "bad", "bai", "bak", "bal", "bam",
            "ban", "baq", "bas", "bat", "bej", "bel", "bem", "ben", "ber", "bho",
            "bih", "bik", "bin", "bis", "bla", "bnt", "bos", "bra", "bre", "btk",
            "bua", "bug", "bul", "bur", "byn", "cad", "cai", "car", "cat", "cau",
            "ceb", "cel", "cha", "chb", "che", "chg", "chi", "chk", "chm", "chn",
            "cho", "chp", "chr", "chu", "chv", "chy", "cmc", "cnr", "cop", "cor",
            "cos", "cpe", "cpf", "cpp", "cre", "crh", "crp", "csb", "cus", "cze",
            "dak", "dan", "dar", "day", "del", "den", "dgr", "din", "div", "doi",
            "dra", "dsb", "dua", "dum", "dut", "dyu", "dzo", "efi", "egy", "eka",
            "elx", "en-US", "en-CA", "eng", "epo", "est", "ewe", "ewo", "fan",
            "fao", "fat", "fij", "fil", "fin", "fiu", "fon", "fre", "frm", "fro", "frr", "frs",
            "fry", "ful", "fur", "gaa", "gay", "gba", "gem", "geo", "ger", "gez",
            "gil", "gla", "gle", "glg", "glv", "gmh", "goh", "gon", "gor", "got",
            "grb", "grc", "gre", "grn", "gsw", "guj", "gwi", "hai", "hat", "hau",
            "haw", "heb", "her", "hil", "him", "hin", "hmn", "hmo", "hrv", "hsb",
            "hun", "hup", "iba", "ibo", "ice", "ido", "iii", "ijo", "iku", "ile",
            "ilo", "ina", "inc", "ind", "ine", "inh", "ipk", "ira", "iro", "ita",
            "jav", "jbo", "jpn", "jpr", "jrb", "kaa", "kab", "kac", "kal", "kam",
            "kan", "kar", "kas", "kau", "kaw", "kaz", "kbd", "kha", "khi", "khm",
            "kho", "kik", "kin", "kir", "kmb", "kok", "kom", "kon", "kor", "kos",
            "kpe", "krc", "krl", "kro", "kru", "kua", "kum", "kur", "kut", "lad",
            "lah", "lam", "lao", "lat", "lav", "lez", "lim", "lin", "lit", "lol",
            "loz", "ltz", "lua", "lub", "lug", "lui", "lun", "luo", "lus", "mac",
            "mad", "mag", "mah", "mai", "mak", "mal", "man", "mao", "map", "mar",
            "mas", "may", "mdf", "mdr", "men", "mga", "mic", "min", "mis", "mkh",
            "mlg", "mlt", "mnc", "mni", "mno", "moh", "mon", "mos", "mul", "mun",
            "mus", "mwl", "mwr", "myn", "myv", "nah", "nai", "nap", "nau", "nav",
            "nbl", "nde", "ndo", "nds", "nep", "new", "nia", "nic", "niu", "nno",
            "nob", "nog", "non", "nor", "nqo", "nso", "nub", "nya", "nym", "nyn",
            "nyo", "nzi", "oci", "oji", "ori", "orm", "osa", "oss", "ota", "oto",
            "paa", "pag", "pal", "pam", "pan", "pap", "pau", "peo", "per", "phi",
            "phn", "pli", "pol", "pon", "por", "pra", "pro", "pus", "que", "raj",
            "rap", "rar", "roa", "roh", "rom", "rum", "run", "rup", "rus", "sad",
            "sag", "sah", "sai", "sal", "sam", "san", "sas", "sat", "scn", "sco",
            "sel", "sem", "sga", "sgn", "shn", "sid", "sin", "sio", "sit", "sla",
            "slo", "slv", "sma", "sme", "smi", "smj", "smn", "smo", "sms", "sna",
            "snd", "snk", "sog", "som", "son", "sot", "spa", "srd", "srn", "srp",
            "srr", "ssa", "ssw", "suk", "sun", "sus", "sux", "swa", "swe", "syc",
            "syr", "tah", "tai", "tam", "tat", "tel", "tem", "ter", "tet", "tgk",
            "tgl", "tha", "tib", "tig", "tir", "tiv", "tkl", "tlh", "tli", "tmh",
            "tog", "ton", "tpi", "tsi", "tsn", "tso", "tuk", "tum", "tup", "tur",
            "tut", "tvl", "twi", "tyv", "udm", "uga", "uig", "ukr", "umb", "und",
            "urd", "uzb", "vai", "ven", "vie", "vol", "vot", "wak", "wal", "war",
            "was", "wel", "wen", "wln", "wol", "xal", "xho", "yao", "yap", "yid",
            "yor", "ypk", "zap", "zbl", "zen", "zha", "znd", "zul", "zun",
    };
}

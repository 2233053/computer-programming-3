package user.settings.view;

import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.themes.FlatMacLightLaf;

import javax.swing.*;
import java.awt.*;

public class SettingsView extends JFrame {
    private JList<String> ipList;
    private JTextField ipField;
    private JButton addButton;
    private JButton removeButton;
    private JButton exitButton;

    public SettingsView() {
        FlatMacLightLaf.setup();
        setTitle("Configure Server IP Address");
        setSize(new Dimension(400, 300));
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());

        ipList = new JList<>();
        ipField = new JTextField();
        ipField.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "Enter a new IP Address or Select address from the list");
        addButton = new JButton("Add");
        removeButton = new JButton("Remove");
        exitButton = new JButton("Save and Exit");

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(addButton);
        buttonPanel.add(removeButton);
        buttonPanel.add(exitButton);
        add(buttonPanel, BorderLayout.SOUTH);
        add(new JScrollPane(ipList), BorderLayout.CENTER);
        add(ipField, BorderLayout.NORTH );
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    public JList<String> getIpList() {
        return ipList;
    }

    public JTextField getIpField() {
        return ipField;
    }

    public JButton getAddButton() {
        return addButton;
    }

    public JButton getRemoveButton() {
        return removeButton;
    }

    public JButton getExitButton() {
        return exitButton;
    }
}

package user.settings.model;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import user.login.controller.ActiveIPListener;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * <h1> SettingModel class</h1>
 * provides methods for managing server IP addresses. <br>
 * It allows adding, removing, and setting the active IP address in the config.xml. <br>
 * The config.xml file is used to store the list of server IP addresses. <br>
 * <br>
 * Note: 10.147.17.0/24 is the private IP address range of the team's VLAN.
 *
 * @see <a href="doc/ZEROTIER.md">VLAN Documentation</a>
 */
public class SettingsModel {
    private final ArrayList<String> serverIPs;
    private String activeIP;

    public SettingsModel() {
        serverIPs = new ArrayList<>();
        loadConfig();
        // sets the first IP address as the active IP
        if (!serverIPs.isEmpty()) {
            activeIP = serverIPs.get(0);
        }
    }

    /**
     * Loads the server IP addresses from the configuration file.
     */
    private void loadConfig() {
        try {
            File inputFile = new File("res/config.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("server");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    // add IP addresses to the list
                    serverIPs.add(eElement.getElementsByTagName("ip").item(0).getTextContent());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Saves the serverIP list of addresses to the configuration file.
     */
    private void saveConfig() {
        try {
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            Element root = document.createElement("configuration");
            document.appendChild(root);

            for (String ip : serverIPs) {
                Element server = document.createElement("server");
                root.appendChild(server);

                Element ipElement = document.createElement("ip");
                ipElement.appendChild(document.createTextNode(ip));
                server.appendChild(ipElement);
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File("res/config.xml"));

            transformer.transform(domSource, streamResult);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Sets the specified IP address as the active IP address.
     *
     * @param ip The IP address to be set as the active IP address.
     */
    public void setActiveAddress(String ip) {
        if (serverIPs.contains(ip)) {
            removeServerIP(ip);
        }
        serverIPs.add(0, ip);
        activeIP = ip;
        saveConfig();
        for (ActiveIPListener listener : listeners) {
            listener.onActiveIPChanged(ip);
        }
    }

    public String getActiveIP() {
        return activeIP;
    }

    /**
     * Adds the specified IP address to the list of server IPs.
     *
     * @param ip The IP address to be added to the list of server IPs.
     */
    public void addServerIP(String ip) {
        if (!serverIPs.contains(ip)) {
            serverIPs.add(ip);
            saveConfig();
        }
    }

    /**
     * Removes the specified IP address from the list of server IPs.
     *
     * @param ip The IP address to be removed from the list of server IPs.
     */
    public void removeServerIP(String ip) {
        if (serverIPs.contains(ip)) {
            serverIPs.remove(ip);
            saveConfig();
        }
    }

    /**
     * Returns a list of server IP addresses.
     *
     * @return A list of server IP addresses.
     */
    public ArrayList<String> getServerIPs() {
        return new ArrayList<>(serverIPs);
    }

    private List<ActiveIPListener> listeners = new ArrayList<>();

    public void addActiveIPListener(ActiveIPListener listener) {
        listeners.add(listener);
    }
}

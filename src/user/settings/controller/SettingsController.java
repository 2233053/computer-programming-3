package user.settings.controller;

import user.settings.view.SettingsView;
import user.settings.model.SettingsModel;

import javax.swing.*;

/**
 * Controller for the settings view
 */
public class SettingsController {
    private SettingsView view;
    private SettingsModel model;

    /**
     * Constructor for the settings controller,<br>
     * Handles the control between the view and the model
     *
     * @param view The view
     * @param model The model
     */
    public SettingsController(SettingsView view, SettingsModel model) {
        this.view = view;
        this.model = model;

        view.getAddButton().addActionListener(e -> {
            String ip = view.getIpField().getText();
            model.addServerIP(ip);
            updateIpList();
        });

        view.getRemoveButton().addActionListener(e -> {
            String selectedIp = view.getIpList().getSelectedValue();
            model.removeServerIP(selectedIp);
            updateIpList();
        });

        view.getExitButton().addActionListener(e -> {
            String selectedIp = view.getIpList().getSelectedValue();
            if (selectedIp != null) {
                model.setActiveAddress(selectedIp);
            }
            view.dispose();
        });
        updateIpList();
    }

    /**
     * Update the IP list in the view
     */
    private void updateIpList() {
        DefaultListModel<String> listModel = new DefaultListModel<>();
        for (String ip : model.getServerIPs()) {
            listModel.addElement(ip);
        }
        view.getIpList().setModel(listModel);
    }
}
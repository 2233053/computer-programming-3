package user.client.view;

import com.formdev.flatlaf.themes.FlatMacLightLaf;
import themes.swing.menu.Menu;
import user.client.model.JFrameModel;
import themes.swing.titlebar.TitleBar;
import javax.swing.*;
import java.awt.*;

@SuppressWarnings("duplicate")
public class JFrameView extends JFrame {

    private Menu menu;

    private final JFrameModel JFrameModel;
    private TitleBar titleBar;
    private JPanel body;
    private final ReserveBookView reserveBook = new ReserveBookView();
    private final ReservationsView reservations = new ReservationsView();
    private final BorrowedView borrowed = new BorrowedView();
    private final ReturnedView returned = new ReturnedView();

    public JFrameView(JFrameModel model, String studentID) {
        ImageIcon img = new ImageIcon("res/icon/user_icon.jpg");
        this.setIconImage(img.getImage());
        JFrameModel = model;
        initComponents(studentID);
        init();
    }

    public Menu getMenu() {
        return menu;
    }

    public ReserveBookView getReserveBook() {
        return reserveBook;
    }

    public ReservationsView getReservations() {
        return reservations;
    }


    public BorrowedView getBorrowing(){
        return borrowed;
    }

    public ReturnedView getHistory(){
        return returned;
    }

    private void init() {
        titleBar.initJFrame(this);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void updateView() {
        body.removeAll();
        body.add(JFrameModel.getCurrentComponent());
        body.repaint();
        body.revalidate();
    }


    public void initComponents(String studentID) {
        FlatMacLightLaf.registerCustomDefaultsSource("themes");
        FlatMacLightLaf.setup();

        JPanel background = new JPanel();
        JPanel panelMenu = new JPanel();
        menu = new Menu(studentID);
        titleBar = new TitleBar();
        body = new JPanel();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        background.setBackground(Color.decode("#04151f"));
        panelMenu.setBackground(Color.decode("#04151f"));
        GroupLayout panelMenuLayout = new GroupLayout(panelMenu);
        panelMenu.setLayout(panelMenuLayout);
        panelMenuLayout.setHorizontalGroup(
                panelMenuLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(panelMenuLayout.createSequentialGroup()
                                .addGroup(panelMenuLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                        .addComponent(menu, GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                                        .addComponent(titleBar, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(0, 0, 0))
        );
        panelMenuLayout.setVerticalGroup(
                panelMenuLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(GroupLayout.Alignment.TRAILING, panelMenuLayout.createSequentialGroup()
                                .addComponent(titleBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(menu, GroupLayout.DEFAULT_SIZE, 676, Short.MAX_VALUE)
                                .addContainerGap())
        );

        body.setOpaque(false);
        body.setLayout(new BorderLayout());

        GroupLayout backgroundLayout = new GroupLayout(background);
        background.setLayout(backgroundLayout);
        backgroundLayout.setHorizontalGroup(
                backgroundLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(backgroundLayout.createSequentialGroup()
                                .addComponent(panelMenu, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(body, GroupLayout.DEFAULT_SIZE, 1098, Short.MAX_VALUE)
                                .addContainerGap())
        );
        backgroundLayout.setVerticalGroup(
                backgroundLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(panelMenu, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(backgroundLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(body, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap())
        );

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(background, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(background, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }

}





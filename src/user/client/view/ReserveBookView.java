package user.client.view;

import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.themes.FlatMacLightLaf;
import net.miginfocom.swing.MigLayout;
import themes.swing.datechooser.listener.DateChooserAction;
import themes.swing.datechooser.listener.DateChooserAdapter;
import themes.system.SystemColor;
import themes.swing.datechooser.date.DateChooser;
import themes.swing.panel.RoundPanel;
import themes.swing.table.Table;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@SuppressWarnings("Duplicate")
public class ReserveBookView extends JPanel {

    private Table table;
    private JTextField bookIDField;
    private JTextField reservationField;

    private JTextField dueDateField;

    private JScrollPane scrollPane;

    private JTextField searchBar;

    private final JLabel description = new JLabel("<html>Reserve a book below.<br></html>");

    private final JButton reserveButton = new JButton("Reserve");

    //private final JButton cancelButton = new JButton("Cancel");

    public JTextField getSearchBar() {
        return searchBar;
    }

    public JLabel getDescription() {
        return description;
    }


    public JTextField getBookIDField() {
        return bookIDField;
    }

    public JTextField getReservationField() {
        return reservationField;
    }

    public Table getTable() {
        return table;
    }

    public void reserveButtonListener(ActionListener listener) {
        reserveButton.addActionListener(listener);
    }

    public void tableAddListener(MouseListener listener) {
        table.addMouseListener(listener);
    }

    public ReserveBookView() {
        initComponents();
        init();
        applyStyle(table);
    }

    private void init() {
        int[] columnWidths = {250, 200, 100, 75};
        table.setColumnWidths(columnWidths);
        table.fixTable(scrollPane);
    }

    //<editor-fold desc="GUI Components">
    private void initComponents() {
        searchBar = new JTextField(20);
        searchBar.setBorder(BorderFactory.createEmptyBorder(8,5,8,5));
        FlatMacLightLaf.setup();
        setBackground(Color.decode("#04151f"));
        JLabel lbTitle = new JLabel("Reserve a Book");
        JLabel titleReservation = new JLabel("Library");
        table = new Table();
        RoundPanel roundPanel = new RoundPanel();

        roundPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        roundPanel.setRound(10);
        JPanel updatePanel = new JPanel(new MigLayout("wrap,fillx,insets 35 45 30 45", "[fill,360]"));
        updatePanel.putClientProperty(FlatClientProperties.STYLE, "arc:10;" + "[light]background:darken(@background,3%);" + "[dark]background:lighten(@background,3%)");

        LocalDate reservationDate = LocalDate.now().plusDays(1);
        LocalDate dueDate = reservationDate.plusDays(7);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        String currentReservationDate = reservationDate.format(formatter);
        String currentDueDate = dueDate.format(formatter);

        DateChooser dateChooser = new DateChooser();
        dateChooser.setThemeColor(SystemColor.MAIN_COLOR_2);
        bookIDField = new JTextField();
        dueDateField = new JTextField(currentDueDate);
        reservationField = new JTextField(currentReservationDate);
        dateChooser.setTextField(reservationField);
        reservationField.setText(currentReservationDate);
        dueDateField.setText(currentDueDate);

        reservationField.setEditable(false);
        dueDateField.setEditable(false);

        dateChooser.setDateSelectable(date -> date.after(new Date()));

        dateChooser.addActionDateChooserListener(new DateChooserAdapter() {
            @Override
            public void dateChanged(Date date, DateChooserAction action) {

                // fixes format of date chosen to yyyy-MM-dd
                LocalDate newReservation = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                String newReservationString = newReservation.toString();
                reservationField.setText(newReservationString);

                // setting due date 7 days from reservation date
                LocalDate newDueDate = newReservation.plusDays(7);
                String dueDateString = newDueDate.format(formatter);
                dueDateField.setText(dueDateString);

            }
        });

        lbTitle.putClientProperty(FlatClientProperties.STYLE, "font:bold +10");
        titleReservation.putClientProperty(FlatClientProperties.STYLE, "font:bold +4");
        description.putClientProperty(FlatClientProperties.STYLE, "[light]foreground:lighten(@foreground,30%);" +
                "[dark]foreground:darken(@foreground,30%)");

        reserveButton.putClientProperty(FlatClientProperties.STYLE, "[light]background:#B3E0FF;" +  // Lighter blue shade
                "[dark]background:#66B2FF;" +
                "borderWidth:0;" +
                "focusWidth:0;" +
                "innerFocusWidth:0");

        updatePanel.add(lbTitle);
        updatePanel.add(description, "gapy 1");
        updatePanel.add(new JLabel("Books"), "gapy 15");
        updatePanel.add(bookIDField);
        updatePanel.add(new JLabel("Reservation Date"), "gapy 15");
        updatePanel.add(reservationField);
        updatePanel.add(new JLabel("Due Date"), "gapy 15");
        updatePanel.add(dueDateField);
        updatePanel.add(reserveButton, "gapy 15");
        updatePanel.setBackground(Color.WHITE);

        table.setModel(new DefaultTableModel(
                new Object[][]{},
                new String[]{"Title", "Author", "Year", "Copies"}

        ) {
            final boolean[] canEdit = new boolean[]{
                    false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        TableRowSorter<TableModel> sorter = new TableRowSorter<>(table.getModel());
        table.setRowSorter(sorter);
        sorter.toggleSortOrder(1);
        scrollPane = new JScrollPane(table);

        GroupLayout roundPanelLayout = new GroupLayout(roundPanel);
        roundPanel.setLayout(roundPanelLayout);
        roundPanelLayout.setHorizontalGroup(
                roundPanelLayout.createParallelGroup(GroupLayout.Alignment.CENTER)  // Align center horizontally
                        .addComponent(titleReservation)  // Center horizontally
                        .addGap(30)
                        .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(searchBar, GroupLayout.Alignment.TRAILING)
                        .addGap(30)
        );

        roundPanelLayout.setVerticalGroup(
                roundPanelLayout.createSequentialGroup()
                        .addGap(10)
                        .addComponent(titleReservation, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)  // Center vertically
                        .addComponent(searchBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGap(10)
                        .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 0, Short.MAX_VALUE)
        );

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createSequentialGroup()
                        .addGap(50)  // Add gap on the left
                        .addComponent(roundPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(20) // Add horizontal space between roundPanel and updatePanel
                        .addComponent(updatePanel, GroupLayout.PREFERRED_SIZE, 400, 400)
                        .addGap(30, 30, 30) // Add gap on the right
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addGap(50)  // Add gap on the top
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(roundPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(50)
                                .addComponent(updatePanel, GroupLayout.DEFAULT_SIZE, 400, 400))
                        .addGap(50)
        );
    }

    private void applyStyle(JTable table) {
        JScrollPane scroll = (JScrollPane) table.getParent().getParent();
        scroll.setBorder(BorderFactory.createEmptyBorder());
        scroll.putClientProperty("ScrollPane.smoothScrolling", true);
        scroll.getVerticalScrollBar().putClientProperty(FlatClientProperties.STYLE, "background:$Table.background;"
                + "track:$Table.background;"
                + "trackArc:999");


        searchBar.putClientProperty("TextComponent.arc", 999);
        searchBar.putClientProperty("JComponent.outline", Color.decode("#edf2f4"));
        searchBar.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "\uD83D\uDD0D  Search here");

        table.getTableHeader().putClientProperty(FlatClientProperties.STYLE_CLASS, "table_style");
        table.getTableHeader().setDefaultRenderer(getAlignmentCellRender(table.getTableHeader().getDefaultRenderer()));
        table.setDefaultRenderer(Object.class, getAlignmentCellRender(table.getDefaultRenderer(Object.class)));

    }

    private TableCellRenderer getAlignmentCellRender(TableCellRenderer oldRender) {
        return new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component com = oldRender.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                if (com instanceof JLabel label) {
                    if (column == 2) {
                        label.setHorizontalAlignment(SwingConstants.CENTER);
                    } else {
                        label.setHorizontalAlignment(SwingConstants.LEADING);
                    }

                }
                return com;
            }
        };
    }
    //</editor-fold>
}
package user.client.view;

import com.formdev.flatlaf.FlatClientProperties;
import themes.swing.panel.RoundPanel;
import themes.swing.table.Table;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

@SuppressWarnings("Duplicate")
public class ReturnedView extends JPanel {

    private JScrollPane scrollPane;
    private Table table;
    private JTextField searchBar;

    public JTextField getSearchBar(){
        return searchBar;
    }

    public Table getTable(){
        return table;
    }

    public ReturnedView() {
        initComponents();
        init();
        applyStyle(table);
    }

    private void init() {
        int[] columnWidths = {350, 200, 75};
        table.setColumnWidths(columnWidths);
        table.fixTable(scrollPane);
    }


    //<editor-fold desc="GUI Components">
    private void initComponents() {
        searchBar = new JTextField(20);
        RoundPanel roundPanel = new RoundPanel();
        scrollPane = new JScrollPane();
        table = new Table();
        setOpaque(false);
        roundPanel.setBackground(new Color(255, 255, 255));
        roundPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        roundPanel.setRound(10);
        searchBar.setBorder(BorderFactory.createEmptyBorder(8,5,8,5));
        table.setModel(new DefaultTableModel(
                new Object[][]{

                },
                new String[]{
                        "Title", "Author", "Year"
                }
        ) {
            final boolean[] canEdit = new boolean[]{
                    false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        scrollPane.setViewportView(table);
        GroupLayout roundPanelLayout = new GroupLayout(roundPanel);
        roundPanel.setLayout(roundPanelLayout);
        roundPanelLayout.setHorizontalGroup(
                roundPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(roundPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(roundPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 0, Short.MAX_VALUE)
                                        .addGap(30)
                                        .addComponent(searchBar, GroupLayout.Alignment.TRAILING)
                                )
                                .addContainerGap()
                        )
        );
        roundPanelLayout.setVerticalGroup(
                roundPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(roundPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(searchBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 0, Short.MAX_VALUE) // Set height to 0 and max height to Short.MAX_VALUE
                                .addContainerGap()
                        )
        );
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addComponent(roundPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(30, 30, 30))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addComponent(roundPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(30, 30, 30))
        );
    }

    private TableCellRenderer getAlignmentCellRender(TableCellRenderer oldRender) {
        return new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component com = oldRender.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                if (com instanceof JLabel label) {
                    if (column == 0 || column == 2 || column == 4 ||column == 5) {
                        label.setHorizontalAlignment(SwingConstants.CENTER);
                    } else {
                        label.setHorizontalAlignment(SwingConstants.LEADING);
                    }
                }
                return com;
            }
        };
    }


    private void applyStyle(JTable table) {
        JScrollPane scroll = (JScrollPane) table.getParent().getParent();
        scroll.setBorder(BorderFactory.createEmptyBorder());
        scroll.putClientProperty("ScrollPane.smoothScrolling", true);
        scroll.getVerticalScrollBar().putClientProperty(FlatClientProperties.STYLE, "background:$Table.background;"
                + "track:$Table.background;"
                + "trackArc:999");

        table.getTableHeader().putClientProperty(FlatClientProperties.STYLE_CLASS, "table_style");
        table.putClientProperty(FlatClientProperties.STYLE_CLASS, "table_style");

        searchBar.putClientProperty("TextComponent.arc", 999);
        searchBar.putClientProperty("JComponent.outline", Color.decode("#edf2f4"));
        searchBar.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "\uD83D\uDD0D  Search here");

        table.getTableHeader().setDefaultRenderer(getAlignmentCellRender(table.getTableHeader().getDefaultRenderer()));
        table.setDefaultRenderer(Object.class, getAlignmentCellRender(table.getDefaultRenderer(Object.class)));
    }
    //</editor-fold>
}
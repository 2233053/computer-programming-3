package user.client.view;

import com.formdev.flatlaf.FlatClientProperties;
import themes.swing.datechooser.date.DateChooser;
import themes.swing.panel.RoundPanel;
import themes.swing.table.Table;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.util.Date;

@SuppressWarnings("Duplicate")
public class ReservationsView extends JPanel {

    private JScrollPane scrollPane;
    private Table table, table2;
    private JTextField searchBar, searchBar2;
    private JTextField searchField;
    private DefaultListModel<Object> suggestionListModel;
    private JList<Object> suggestionList;
    private JScrollPane targetScrollPane;
    private JTextField titleTextField;
    private JTextField dueDateTextField;
    private JTextField reservationTextField;
    private DateChooser reservation;
    private JPanel targetPanel;

    public DateChooser getDateChooser() {
        return dateChooser;
    }

    private DateChooser dateChooser;

    public void setTable(Table table) {
        this.table = table;
    }

    public JTextField getSearchField() {
        return searchField;
    }

    public DefaultListModel<Object> getSuggestionListModel() {
        return suggestionListModel;
    }

    public JList<Object> getSuggestionList() {
        return suggestionList;
    }

    public JScrollPane getTargetScrollPane() {
        return targetScrollPane;
    }

    public JTextField getTitleTextField() {
        return titleTextField;
    }

    public JTextField getDueDateTextField() {
        return dueDateTextField;
    }

    public JTextField getReservationTextField() {
        return reservationTextField;
    }

    public DateChooser getReservation() {
        return reservation;
    }

    public void setReservation(DateChooser reservation) {
        this.reservation = reservation;
    }

    public JPanel getTargetPanel() {
        return targetPanel;
    }

    public JTextField getSearchBar() {
        return searchBar;
    }

    public JTextField getSearchBar2() {
        return searchBar2;
    }

    public Table getTable() {
        return table;
    }

    public Table getTable2() {
        return table2;
    }

    public ReservationsView() {
        initComponents();
        init();
        applyStyle(table);
    }

    private void init() {
        int[] columnWidths = {250, 150, 35};
        table.setColumnWidths(columnWidths);
        table.fixTable(scrollPane);

    }

    //<editor-fold desc="GUI Components">
    private void initComponents() {
        JLabel titleReservation2 = new JLabel("Waiting for approval");
        titleReservation2.putClientProperty(FlatClientProperties.STYLE, "font:bold +4");

        table2 = new Table();
        RoundPanel panel = new RoundPanel();
        panel.setBackground(new Color(255, 255, 255));
        panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        panel.setRound(10);

        searchBar2 = new JTextField(20);
        searchBar2.setBorder(BorderFactory.createEmptyBorder(8, 5, 8, 5));
        JScrollPane scrollPane2 = new JScrollPane();
        scrollPane2.setViewportView(table2);
        table2.setModel(new DefaultTableModel(
                new Object[][]{},
                new String[]{"Title", "Reservation Date", "Due Date"}

        ) {
            final boolean[] canEdit = new boolean[]{
                    false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        int[] columnWidths2 = {250, 150, 75};
        table2.setColumnWidths(columnWidths2);
        table2.fixTable(scrollPane2);
        applyStyle2(table2);
        GroupLayout roundPanelLayout = new GroupLayout(panel);
        panel.setLayout(roundPanelLayout);
        roundPanelLayout.setHorizontalGroup(
                roundPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(GroupLayout.Alignment.CENTER, roundPanelLayout.createSequentialGroup()
                                .addGap(30)
                                .addComponent(titleReservation2)
                                .addGap(30))
                        .addGroup(roundPanelLayout.createSequentialGroup()
                                .addGap(30)
                                .addGap(30)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addComponent(searchBar2, GroupLayout.Alignment.TRAILING)
                        .addGroup(GroupLayout.Alignment.CENTER, roundPanelLayout.createSequentialGroup()
                                .addComponent(scrollPane2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))

        );

        roundPanelLayout.setVerticalGroup(
                roundPanelLayout.createSequentialGroup()
                        .addGroup(roundPanelLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                .addGap(10)
                                .addComponent(titleReservation2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGap(10))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(roundPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(searchBar2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scrollPane2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE) // Set height to 0 and max height to Short.MAX_VALUE
        );




        JLabel titleReservation = new JLabel("Books ready for pick up");
        searchBar = new JTextField(20);
        titleReservation.putClientProperty(FlatClientProperties.STYLE, "font:bold +4");
        RoundPanel roundPanel = new RoundPanel();
        scrollPane = new JScrollPane();
        table = new Table();
        setOpaque(false);
        roundPanel.setBackground(new Color(255, 255, 255));
        roundPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        roundPanel.setRound(10);
        searchBar.setBorder(BorderFactory.createEmptyBorder(8, 5, 8, 5));
        table.setModel(new DefaultTableModel(
                new Object[][]{

                },
                new String[]{
                         "Title", "Author", "Year"
                }
        ) {
            final boolean[] canEdit = new boolean[]{
                    false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
        scrollPane.setViewportView(table);
        
        GroupLayout roundPanelLayout2 = new GroupLayout(roundPanel);
        roundPanel.setLayout(roundPanelLayout2);
        roundPanelLayout2.setHorizontalGroup(
                roundPanelLayout2.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(GroupLayout.Alignment.CENTER, roundPanelLayout2.createSequentialGroup()
                                .addGap(30)
                                .addComponent(titleReservation)
                                .addGap(30))
                        .addGroup(roundPanelLayout2.createSequentialGroup()
                                .addGap(30)
                                .addGap(30)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addComponent(searchBar, GroupLayout.Alignment.TRAILING)
                        .addGroup(GroupLayout.Alignment.CENTER, roundPanelLayout2.createSequentialGroup()
                                .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))

        );
        roundPanelLayout2.setVerticalGroup(
                roundPanelLayout2.createSequentialGroup()
                        .addGroup(roundPanelLayout2.createParallelGroup(GroupLayout.Alignment.CENTER)
                                .addGap(10)
                                .addComponent(titleReservation, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGap(10))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(roundPanelLayout2.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(searchBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE) // Set height to 0 and max height to Short.MAX_VALUE
        );

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createSequentialGroup()
                        .addGap(30, 30, 30)  // Add gap on the left
                                .addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(20) // Add horizontal space between roundPanel and updatePanel
                                .addComponent(roundPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(30, 30, 30)
        );
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30) // Add gap on the top
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(50)
                                        .addComponent(roundPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(30, 30, 30)
        );

        // JOptionPane for updating, cancellation

        searchField = new JTextField(35);
        suggestionListModel = new DefaultListModel<>();
        suggestionList = new JList<>(suggestionListModel);

        targetScrollPane = new JScrollPane(suggestionList);
        targetScrollPane.setPreferredSize(new Dimension(400, 199));

        // date format


        // text field for title
        titleTextField = new JTextField(35);
        titleTextField.setEditable(false);
        titleTextField.setFocusable(false);

        // text field for due date
        dueDateTextField = new JTextField(35);
        dueDateTextField.setEditable(false);
        dueDateTextField.setFocusable(false);

        // text field and date chooser for reservation date
        reservation = new DateChooser();
        reservationTextField = new JTextField(35);
        reservationTextField.setEditable(false);
        reservationTextField.setFocusable(false);
        reservation.setDateSelectable(date -> date.after(new Date()));
        reservation.setDateSelectionMode(DateChooser.DateSelectionMode.SINGLE_DATE_SELECTED);

        // panel to hold the fields
        targetPanel = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(5, 5, 5, 5);

        targetPanel.add(new JLabel("Current Book:"), gbc);
        gbc.gridy++;
        targetPanel.add(titleTextField, gbc);
        gbc.gridy++;
        targetPanel.add(new JLabel("New Book:"), gbc);
        gbc.gridy++;
        targetPanel.add(searchField, gbc);
        gbc.gridy++;
        targetPanel.add(targetScrollPane, gbc);
        gbc.gridy++;
        targetPanel.add(new JLabel("Reservation Date:"), gbc);
        gbc.gridy++;
        targetPanel.add(reservationTextField, gbc);
        gbc.gridy++;
        targetPanel.add(new JLabel("Due Date:"), gbc);
        gbc.gridy++;
        targetPanel.add(dueDateTextField, gbc);
    }

    private TableCellRenderer getAlignmentCellRender(TableCellRenderer oldRender) {
        return new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component com = oldRender.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                if (com instanceof JLabel label) {
                        label.setHorizontalAlignment(SwingConstants.LEADING);
                }
                return com;
            }
        };
    }


    private void applyStyle(JTable table) {
        JScrollPane scroll = (JScrollPane) table.getParent().getParent();
        scroll.setBorder(BorderFactory.createEmptyBorder());
        scroll.putClientProperty("ScrollPane.smoothScrolling", true);
        scroll.getVerticalScrollBar().putClientProperty(FlatClientProperties.STYLE, "background:$Table.background;"
                + "track:$Table.background;"
                + "trackArc:999");

        table.getTableHeader().putClientProperty(FlatClientProperties.STYLE_CLASS, "table_style");
        table.putClientProperty(FlatClientProperties.STYLE_CLASS, "table_style");

        searchBar.putClientProperty("TextComponent.arc", 999);
        searchBar.putClientProperty("JComponent.outline", Color.decode("#edf2f4"));
        searchBar.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "\uD83D\uDD0D  Search here");

        table.getTableHeader().setDefaultRenderer(getAlignmentCellRender(table.getTableHeader().getDefaultRenderer()));
        table.setDefaultRenderer(Object.class, getAlignmentCellRender(table.getDefaultRenderer(Object.class)));
    }

    private void applyStyle2(JTable table) {
        JScrollPane scroll = (JScrollPane) table.getParent().getParent();
        scroll.setBorder(BorderFactory.createEmptyBorder());
        scroll.putClientProperty("ScrollPane.smoothScrolling", true);
        scroll.getVerticalScrollBar().putClientProperty(FlatClientProperties.STYLE, "background:$Table.background;"
                + "track:$Table.background;"
                + "trackArc:999");

        table.getTableHeader().putClientProperty(FlatClientProperties.STYLE_CLASS, "table_style");
        table.putClientProperty(FlatClientProperties.STYLE_CLASS, "table_style");

        searchBar2.putClientProperty("TextComponent.arc", 999);
        searchBar2.putClientProperty("JComponent.outline", Color.decode("#edf2f4"));
        searchBar2.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "\uD83D\uDD0D  Search here");

        table.getTableHeader().setDefaultRenderer(getAlignmentCellRender(table.getTableHeader().getDefaultRenderer()));
        table.setDefaultRenderer(Object.class, getAlignmentCellRender(table.getDefaultRenderer(Object.class)));
    }
    //</editor-fold>
}
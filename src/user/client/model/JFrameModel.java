package user.client.model;

import java.awt.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.time.LocalDate;

public class JFrameModel {

    private Component currentComponent;
    private String host;

    public JFrameModel(String host) {
        this.host = host;
    }
    public void setComponent(Component com) {
        currentComponent = com;
    }

    public Component getCurrentComponent() {
        return currentComponent;
    }

    public Object getBooks() throws IOException, ClassNotFoundException {
        try (Socket socket = new Socket(host, 2000)) {
            ObjectOutputStream writer = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream reader = new ObjectInputStream(socket.getInputStream());
            writer.writeObject("Book List");
            return reader.readObject();
        }
    }

    public String setReserve(String studentID, String bookID, LocalDate reservationDate)throws IOException, ClassNotFoundException{
        try (Socket socket = new Socket(host, 2000)) {
            ObjectOutputStream writer = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream reader = new ObjectInputStream(socket.getInputStream());
            writer.writeObject("Reserve");
            writer.writeObject(studentID);
            writer.writeObject(bookID);
            writer.writeObject(reservationDate);
            return (String) reader.readObject();
        }
    }

    public Object getReservation()throws IOException, ClassNotFoundException{
        try (Socket socket = new Socket(host, 2000)) {
            ObjectOutputStream writer = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream reader = new ObjectInputStream(socket.getInputStream());
            writer.writeObject("Reservation");
            return reader.readObject();
        }
    }

    //vString to void
    public void setCancellation(String studentID, String bookTitle)throws IOException, ClassNotFoundException{
        try (Socket socket = new Socket(host, 2000)) {
            ObjectOutputStream writer = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream reader = new ObjectInputStream(socket.getInputStream());
            writer.writeObject("Cancellation");
            writer.writeObject(studentID);
            writer.writeObject(bookTitle);
            //return (String) reader.readObject();
            reader.readObject();
        }
    }

    // String to void
    public void setUpdate(String studentID, String oldBookTitle, String newBookTitle, String newReservationDate, String newDueDate)throws IOException, ClassNotFoundException{
        try (Socket socket = new Socket(host, 2000)) {
            ObjectOutputStream writer = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream reader = new ObjectInputStream(socket.getInputStream());
            writer.writeObject("Update");
            writer.writeObject(studentID);
            writer.writeObject(oldBookTitle);
            writer.writeObject(newBookTitle);
            writer.writeObject(newReservationDate);
            writer.writeObject(newDueDate);
            //return (String) reader.readObject();
            reader.readObject();
        }
    }

    public Object getHistory()throws IOException, ClassNotFoundException{
        try (Socket socket = new Socket(host, 2000)) {
            ObjectOutputStream writer = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream reader = new ObjectInputStream(socket.getInputStream());
            writer.writeObject("Returned");
            return reader.readObject();
        }
    }
}


package user.client.controller;

import shared.references.Book;
import shared.references.History;
import shared.references.Reservation;
import themes.swing.datechooser.date.DateChooser;
import themes.swing.datechooser.listener.DateChooserAction;
import themes.swing.datechooser.listener.DateChooserAdapter;
import user.client.model.JFrameModel;
import user.client.view.JFrameView;
import user.client.view.ReservationsView;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.time.ZoneId;



@SuppressWarnings({"unchecked", "duplicate"})
public class JFrameController {

    private final String studentID;
    private final JFrameView JFrameView;
    private final JFrameModel JFrameModel;

    public JFrameController(JFrameView view, JFrameModel model, String studentID) {
        JFrameView = view;
        JFrameModel = model;
        this.studentID = studentID;
        init();
    }

    private void init() {
        changeComponent();
        searchListenerBorrowing();
        searchListenerHistory();
        searchListenerReservations();
        reservation();
        tableListener();
        applyTableClickListener();
    }

    private void changeComponent() {
        JFrameView.getMenu().addEvent((index, indexSubMenu) -> {
            if (index == 0 ) {
                setBookList();
                JFrameModel.setComponent(JFrameView.getReserveBook());
            } else if (index == 1) {
                setReserveBook();
                setReservationList();
                JFrameModel.setComponent(JFrameView.getReservations());
            } else if (index == 2) {
                setBorrowing();
                JFrameModel.setComponent(JFrameView.getBorrowing());
            } else if (index == 3) {
                setHistory();
                JFrameModel.setComponent(JFrameView.getHistory());
            }
            JFrameView.updateView();
        });
        JFrameView.getMenu().setSelectedIndex(0, 0);
    }

    private void reservation() {
        JFrameView.getReserveBook().reserveButtonListener((ActionEvent e) -> {
            try {
                JLabel description = JFrameView.getReserveBook().getDescription();
                String books = JFrameView.getReserveBook().getBookIDField().getText();
                if (books.isEmpty()) {
                    description.setForeground(Color.RED);
                    description.setText("All fields must be filled out!");
                    JFrameView.getReserveBook().getBookIDField().putClientProperty("JComponent.outline", Color.RED);
                }
                String[] bookNames = books.split(",");
                ArrayList<Book> booksList = (ArrayList<Book>) JFrameModel.getBooks();
                ArrayList<Reservation> reservationList = (ArrayList<Reservation>) JFrameModel.getReservation();

                StringBuilder errorMessages = new StringBuilder();
                for (String bookName : bookNames) {
                    for (Book book : booksList) {
                        if (book.getTitle().equals(bookName)) {
                            String bookId = book.getBookID();
                            //String reservationDate = JFrameView.getReserveBook().getReservationField().getText();
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                            LocalDate reservationDate = LocalDate.parse(JFrameView.getReserveBook().getReservationField().getText(), formatter);

                                    JFrameView.getReserveBook().getBookIDField().setText("");
                                    JFrameView.getReserveBook().getBookIDField().putClientProperty("JComponent.outline", null);
                                    JFrameView.getReserveBook().getReservationField().putClientProperty("JComponent.outline", null);

                                    String response = JFrameModel.setReserve(studentID, bookId, reservationDate);

                                    if (response.equalsIgnoreCase("SUCCESSFUL")) {
                                        errorMessages.append("<div style='color:green'>Book reserved successfully: ").append(bookName).append("</div>");
                                        setBookList();
                                    } else if (response.equals("RESERVED ALREADY")) {
                                        errorMessages.append("<div style='color:red;'>Error for book '").append(bookName).append("': ").append("Book not available!</div>");
                                    } else if (response.equalsIgnoreCase("NO COPIES AVAILABLE")) {
                                        errorMessages.append("<div style='color:red;'>Error for book '").append(bookName).append("': ").append("No copies available!</div>");
                                    } else {
                                        errorMessages.append("<div style=' color:red;'>Error for book '").append(bookName).append("': ").append(response).append("</div>");
                                    }
                                }
                            }
                        }

                if (errorMessages.toString().contains("Error for book")) {
                    description.setText("<html>" + errorMessages + "</html>");
                } else {
                    description.setText("All books reserved successfully!");
                    description.setForeground(Color.GREEN);
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        });
    }

    private void tableListener() {
        JFrameView.getReserveBook().getTable().getSelectionModel().addListSelectionListener(e -> {
            if (!e.getValueIsAdjusting()) {
                int[] selectedRows = JFrameView.getReserveBook().getTable().getSelectedRows();
                if (selectedRows.length > 0) {
                    StringBuilder bookTitles = new StringBuilder();
                    for (int selectedRow : selectedRows) {
                        String bookTitle = JFrameView.getReserveBook().getTable().getValueAt(selectedRow, 0).toString();
                        bookTitles.append(bookTitle).append(",");
                    }
                    JFrameView.getReserveBook().getBookIDField().setText(bookTitles.toString());
                    //JFrameView.getReserveBook().getCancelButton().setEnabled(true);
                }
            }
        });


        JFrameView.getReserveBook().getSearchBar().getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateTable();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateTable();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updateTable();
            }

            private void updateTable() {
                String searchText = JFrameView.getReserveBook().getSearchBar().getText(); // Get the search text
                JFrameView.getReserveBook().getTable().filterRows(searchText); // Filter rows based on the search text
            }
        });
    }

    private void searchListenerBorrowing() {
        JFrameView.getBorrowing().getSearchBar().getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateTable();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateTable();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updateTable();
            }

            private void updateTable() {
                String searchText = JFrameView.getBorrowing().getSearchBar().getText(); // Get the search text
                JFrameView.getBorrowing().getTable().filterRows(searchText); // Filter rows based on the search text
            }
        });
    }


    private void searchListenerHistory() {
        JFrameView.getHistory().getSearchBar().getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateTable();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateTable();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updateTable();
            }

            private void updateTable() {
                String searchText = JFrameView.getHistory().getSearchBar().getText(); // Get the search text
                JFrameView.getHistory().getTable().filterRows(searchText); // Filter rows based on the search text
            }
        });
    }

    private void searchListenerReservations() {
        JFrameView.getReservations().getSearchBar().getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateTable();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateTable();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updateTable();
            }

            private void updateTable() {
                String searchText = JFrameView.getReservations().getSearchBar().getText(); // Get the search text
                JFrameView.getReservations().getTable().filterRows(searchText); // Filter rows based on the search text
            }
        });

        JFrameView.getReservations().getSearchBar2().getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateTable();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateTable();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updateTable();
            }

            private void updateTable() {
                String searchText = JFrameView.getReservations().getSearchBar2().getText(); // Get the search text
                JFrameView.getReservations().getTable2().filterRows(searchText); // Filter rows based on the search text
            }
        });
    }

    private void setBookList() {
        try {
            DefaultTableModel model = (DefaultTableModel) JFrameView.getReserveBook().getTable().getModel();
            model.setRowCount(0);
            ArrayList<Book> books = (ArrayList<Book>) JFrameModel.getBooks();
            for (Book book : books) {
                String author = book.getAuthor();
                String year = book.getPublicationYear();
                String title = book.getTitle();
                int bookCopies = book.getBookCopies();
                Object[] row = {title, author, year, bookCopies};
                model.addRow(row);
            }
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("SET BOOK LIST: " + e.getMessage());
        }
    }

    private void setReservationList() {
        try {
            DefaultTableModel model = (DefaultTableModel) JFrameView.getReservations().getTable().getModel();
            model.setRowCount(0);
            ArrayList<Reservation> reservations = (ArrayList<Reservation>) JFrameModel.getReservation();
            ArrayList<Book> books = (ArrayList<Book>) JFrameModel.getBooks();
            for (Reservation reservation : reservations) {
                if (reservation.getStudentId().equals(studentID) && reservation.isApproved() && !reservation.isReturnStatus() && !reservation.isPickUpStatus()) {
                    getBooks(model, books, reservation);
                }
            }
        } catch (Exception e) {
            System.out.println("SET RESERVATION LIST: " + e.getMessage());
        }
    }

    private void setBorrowing() {
        try {
            DefaultTableModel model = (DefaultTableModel) JFrameView.getBorrowing().getTable().getModel();
            model.setRowCount(0);
            ArrayList<Reservation> reservations = (ArrayList<Reservation>) JFrameModel.getReservation();
            ArrayList<Book> books = (ArrayList<Book>) JFrameModel.getBooks();
            for (Reservation reservation : reservations) {
                if (reservation.getStudentId().equals(studentID) && reservation.isPickUpStatus() && !reservation.isReturnStatus()) {
                    getBooks3(model, books, reservation);
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void getBooks(DefaultTableModel model, ArrayList<Book> books, Reservation reservation) {
        String bookId = reservation.getBookId();
        for (Book book : books) {
            if (book.getBookID().equals(bookId)) {
                String author = book.getAuthor();
                String year = book.getPublicationYear();
                String title = book.getTitle();
                Object[] row = { title, author, year};
                model.addRow(row);
            }
        }
    }

    private void getBooks3(DefaultTableModel model, ArrayList<Book> books, Reservation reservation) {
        String bookId = reservation.getBookId();
        for (Book book : books) {
            if (book.getBookID().equals(bookId)) {
                String author = book.getAuthor();
                String year = book.getPublicationYear();
                String title = book.getTitle();
                String lang = book.getLanguageCode();
                Object[] row = { title, author, year, lang};
                model.addRow(row);
            }
        }
    }

    private void getBooks2(DefaultTableModel model, ArrayList<Book> books, Reservation reservation) {
        String bookId = reservation.getBookId();
        for (Book book : books) {
            if (book.getBookID().equals(bookId)) {
                String title = book.getTitle();
                Object[] row = { title, reservation.getReservationDate(), reservation.getDueDate()};
                model.addRow(row);
            }
        }
    }


    private void setReserveBook() {
        try {
            DefaultTableModel model = (DefaultTableModel) JFrameView.getReservations().getTable2().getModel();
            model.setRowCount(0);
            ArrayList<Reservation> reservations = (ArrayList<Reservation>) JFrameModel.getReservation();
            ArrayList<Book> books = (ArrayList<Book>) JFrameModel.getBooks();
            for (Reservation reservation : reservations) {
                if (reservation.getStudentId().equals(studentID) && !reservation.isApproved()) {
                    getBooks2(model, books, reservation);
                }
            }
        } catch (Exception e) {
            System.out.println("SET RESERVE BOOK: " + e.getMessage());
        }
    }

    private void setHistory() {
        try {
            DefaultTableModel model = (DefaultTableModel) JFrameView.getHistory().getTable().getModel();
            model.setRowCount(0);
            ArrayList<Reservation> reservations = (ArrayList<Reservation>) JFrameModel.getReservation();
            ArrayList<History> allHistory = (ArrayList<History>) JFrameModel.getHistory();
            ArrayList<Book> books = (ArrayList<Book>) JFrameModel.getBooks();
            for (History history : allHistory) {
                for (Reservation reservation : reservations) {
                    if (history.getStudentId().equals(studentID) && (reservation.getStudentId().equals(studentID))) {
                        String historyBookId = history.getBookId();
                        for (Book book : books) {
                            if (book.getBookID().equals(historyBookId)) {
                                String author = book.getAuthor();
                                String year = book.getPublicationYear();
                                String title = book.getTitle();
                                Object[]row = {title, author, year};
                                model.addRow(row);
                            }
                        }
                    }
                }
            }
        } catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    private void applyTableClickListener() {
        ReservationsView reservations = JFrameView.getReservations();

        reservations.getTable2().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 1) {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                    JTable target = (JTable) e.getSource();
                    int row = target.getSelectedRow();

                    reservations.getSuggestionList().setModel(reservations.getSuggestionListModel());
                    reservations.getTargetScrollPane().setViewportView(reservations.getSuggestionList());

                    String targetTitle = (String) target.getValueAt(row, 0);
                    LocalDate reservationDate = LocalDate.parse(target.getValueAt(row, 1).toString());
                    //LocalDate currentReservationDate = reservationDate.plusDays(1);
                    LocalDate dueDate = (LocalDate) target.getValueAt(row, 2);

                    String reservationDateString = reservationDate.format(formatter);
                    String dueDateString = dueDate.format(formatter);

                    reservations.getTitleTextField().setText(targetTitle);
                    reservations.getDueDateTextField().setText(dueDateString);
                    reservations.getReservation().setTextField(reservations.getReservationTextField());
                    reservations.getReservationTextField().setText(reservationDateString);


                    reservations.getReservation().setDateSelectable(date -> date.after(new Date()));

                    reservations.getReservation().addActionDateChooserListener(new DateChooserAdapter() {
                        @Override
                        public void dateChanged(Date date, DateChooserAction action) {
                            // fixes format of date chosen to yyyy-MM-dd
                            LocalDate newReservation = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                            String newReservationString = newReservation.toString();
                            reservations.getReservationTextField().setText(newReservationString);

                            // setting due date 7 days from reservation date
                            LocalDate newDueDate = newReservation.plusDays(7);
                            String dueDateString = newDueDate.format(formatter);
                            reservations.getDueDateTextField().setText(dueDateString);

                        }
                    });

                    reservations.getReservation().setDateSelectionMode(DateChooser.DateSelectionMode.SINGLE_DATE_SELECTED);

                    // document listener to update suggestions while typing
                    reservations.getSearchField().getDocument().addDocumentListener(new DocumentListener() {
                        @Override
                        public void insertUpdate(DocumentEvent e) {
                            try {
                                updateSuggestions();
                            } catch (IOException | ClassNotFoundException ex) {
                                throw new RuntimeException(ex);
                            }
                        }

                        @Override
                        public void removeUpdate(DocumentEvent e) {
                            try {
                                updateSuggestions();
                            } catch (IOException | ClassNotFoundException ex) {
                                throw new RuntimeException(ex);
                            }
                        }

                        @Override
                        public void changedUpdate(DocumentEvent e) {
                            try {
                                updateSuggestions();
                            } catch (IOException | ClassNotFoundException ex) {
                                throw new RuntimeException(ex);
                            }
                        }
                    });

                    // mouse listener to handle selection from suggestion list
                    reservations.getSuggestionList().addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseClicked(MouseEvent e) {
                            if (e.getClickCount() == 2) {
                                JList<String> list = (JList<String>) e.getSource();
                                int index = list.locationToIndex(e.getPoint());
                                if (index >= 0) {
                                    String selectedValue = list.getModel().getElementAt(index);
                                    reservations.getSearchField().setText(selectedValue);
                                }
                            }
                        }
                    });

                    // focus listener for suggestion list to handle focus loss
                    reservations.getSuggestionList().addFocusListener(new FocusListener() {
                        @Override
                        public void focusGained(FocusEvent e) {}

                        @Override
                        public void focusLost(FocusEvent e) {
                            reservations.getSearchField().requestFocusInWindow();
                        }
                    });

                    try {
                        updateSuggestions();
                    } catch (IOException | ClassNotFoundException ex) {
                        throw new RuntimeException(ex);
                    }

                    // Show the option pane with panel
                    String[] options = {"Update", "Delete"}; // JOption buttons
                    int selection = JOptionPane.showOptionDialog(
                            JFrameView,
                            new Object[]{ reservations.getTargetPanel() },
                            "",
                            JOptionPane.DEFAULT_OPTION,
                            JOptionPane.PLAIN_MESSAGE,
                            null,
                            options,
                            options[0]);

                    // option checker
                    if (selection == 0) {
                        try {

                        String oldBookTitle = reservations.getTitleTextField().getText();
                        String newBookTitle = reservations.getSearchField().getText();
                        String newReservationDate = reservations.getReservationTextField().getText();
                        String newDueDate = reservations.getDueDateTextField().getText();

                        if (newBookTitle.isEmpty()){
                            newBookTitle = oldBookTitle;
                        }
                                JFrameModel.setUpdate(studentID, oldBookTitle, newBookTitle, newReservationDate, newDueDate);
                            } catch (IOException | ClassNotFoundException ex) {
                                throw new RuntimeException(ex);
                            }

                        // update option
                            JOptionPane.showMessageDialog(JFrameView,
                                    "New Book: " + reservations.getSearchField().getText() +
                                            "\nNew Reservation Date: " + reservations.getReservationTextField().getText() +
                                            "\nNew Due Date: " + reservations.getDueDateTextField().getText());

                    } else if (selection == 1) {
                        try {
                            String bookTitle = reservations.getTitleTextField().getText();
                            JFrameModel.setCancellation(studentID, bookTitle);
                        } catch (IOException | ClassNotFoundException ex) {
                            throw new RuntimeException(ex);
                        }
                        // delete option
                        JOptionPane.showMessageDialog(JFrameView,
                                "You Deleted: " + reservations.getTitleTextField().getText());
                    }
                }
            }
        });
    }

    private void updateSuggestions() throws IOException, ClassNotFoundException {
        ArrayList<Book> bookTitles = (ArrayList<Book>) JFrameModel.getBooks();
        JFrameView.getReservations().getSuggestionListModel().clear();
        String search = JFrameView.getReservations().getSearchField().getText().toLowerCase();
        for (Book books : bookTitles) {
            if ((books.getTitle()).toLowerCase().startsWith(search)) {
                String title = books.getTitle();
                JFrameView.getReservations().getSuggestionListModel().addElement(title);
            }
        }
    }
}

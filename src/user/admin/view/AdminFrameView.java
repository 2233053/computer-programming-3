package user.admin.view;

import com.formdev.flatlaf.themes.FlatMacLightLaf;
import themes.swing.adminmenu.EventMenuSelected;
import themes.swing.titlebar.TitleBar;
import themes.swing.adminmenu.Menu;
import javax.swing.*;
import java.awt.Component;
import java.awt.Color;
import java.awt.BorderLayout;

public class AdminFrameView extends JFrame {

    private final AddBookView addBookView = new AddBookView();
    private final AddUserView addUserView = new AddUserView();
    private final ReservationView reservationView = new ReservationView();
    private final UpdateBookView updateBookView = new UpdateBookView();
    private final UpdateUserView updateUserView = new UpdateUserView();

    public ReservationView getReservationView() {
        return reservationView;
    }
    public JPanel getBody() {
        return body;
    }

    public void setBody(JPanel body) {
        this.body = body;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public TitleBar getTitleBar() {
        return titleBar;
    }

    public void setTitleBar(TitleBar titleBar) {
        this.titleBar = titleBar;
    }

    public UpdateBookView getUpdateBook() {
        return updateBookView;
    }

    private JPanel body;
    private Menu menu;
    private TitleBar titleBar;

    public UpdateUserView getUpdateUser() {
        return updateUserView;
    }
    public AddBookView getAddBookView() {
        return addBookView;
    }

    public AddUserView getAddUser(){
        return addUserView;
    }


    public void setMenuEvent(EventMenuSelected event){
        menu.addEvent(event);
    }

    public Menu getMenu() {
        return menu;
    }

    public AdminFrameView() {
        ImageIcon img = new ImageIcon("res/icon/admin_icon.png");
        this.setIconImage(img.getImage());
        initComponents();
        init();
    }

    public void init() {
        titleBar.initJFrame(this);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void updateView(Component com) {
        body.removeAll();
        body.add(com);
        body.repaint();
        body.revalidate();
    }

    public void initComponents() {
        FlatMacLightLaf.registerCustomDefaultsSource("themes");
        FlatMacLightLaf.setup();

        JPanel background = new JPanel();
        JPanel panelMenu = new JPanel();
        menu = new Menu();
        titleBar = new TitleBar();
        body = new JPanel();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        background.setBackground(Color.decode("#04151f"));

        panelMenu.setBackground(Color.decode("#04151f"));

        GroupLayout panelMenuLayout = new GroupLayout(panelMenu);
        panelMenu.setLayout(panelMenuLayout);
        panelMenuLayout.setHorizontalGroup(
                panelMenuLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(panelMenuLayout.createSequentialGroup()
                                .addGroup(panelMenuLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                        .addComponent(menu, GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                                        .addComponent(titleBar, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(0, 0, 0))
        );
        panelMenuLayout.setVerticalGroup(
                panelMenuLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(GroupLayout.Alignment.TRAILING, panelMenuLayout.createSequentialGroup()
                                .addComponent(titleBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(menu, GroupLayout.DEFAULT_SIZE, 676, Short.MAX_VALUE)
                                .addContainerGap())
        );

        body.setOpaque(false);
        body.setLayout(new BorderLayout());

        GroupLayout backgroundLayout = new GroupLayout(background);
        background.setLayout(backgroundLayout);
        backgroundLayout.setHorizontalGroup(
                backgroundLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(backgroundLayout.createSequentialGroup()
                                .addComponent(panelMenu, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(body, GroupLayout.DEFAULT_SIZE, 1098, Short.MAX_VALUE)
                                .addContainerGap())
        );
        backgroundLayout.setVerticalGroup(
                backgroundLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(panelMenu, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(backgroundLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(body, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap())
        );

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(background, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(background, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }
}

package user.admin.view;

import javax.swing.*;
import java.awt.Font;
import java.awt.Color;

public class Empty extends JPanel {

    public Empty(String name) {
        initComponents();
        lb.setText("Form " + name);
    }


    private void initComponents() {

        lb = new javax.swing.JLabel();

        setOpaque(false);

        lb.setFont(new Font("sansserif", 1, 48)); // NOI18N
        lb.setForeground(new Color(125, 125, 125));
        lb.setHorizontalAlignment(SwingConstants.CENTER);
        lb.setText("Form");

        GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lb, javax.swing.GroupLayout.DEFAULT_SIZE, 702, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lb, GroupLayout.DEFAULT_SIZE, 435, Short.MAX_VALUE)
                .addContainerGap())
        );
    }

    private JLabel lb;
}

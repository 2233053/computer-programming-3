package user.admin.view;

import themes.swing.panel.RoundPanel;
import themes.swing.table.Table;
import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.themes.FlatMacLightLaf;
import net.miginfocom.swing.MigLayout;
import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.Component;
import java.awt.Color;


public class UpdateUserView extends JPanel {

    private JLabel description;

    private JButton updateButton, deleteButton;

    public UpdateUserView() {
        initComponents();
        init();
        applyStyle(table);
    }

    @SuppressWarnings("Duplicates")
    private void applyStyle(JTable table) {

        JScrollPane scroll = (JScrollPane) table.getParent().getParent();
        scroll.setBorder(BorderFactory.createEmptyBorder());
        scroll.putClientProperty("ScrollPane.smoothScrolling", true);
        scroll.getVerticalScrollBar().putClientProperty(FlatClientProperties.STYLE, "background:$Table.background;"
                + "track:$Table.background;"
                + "trackArc:999");

        table.getTableHeader().putClientProperty(FlatClientProperties.STYLE_CLASS, "table_style");

        table.getTableHeader().setDefaultRenderer(getAlignmentCellRender(table.getTableHeader().getDefaultRenderer()));
        table.setDefaultRenderer(Object.class, getAlignmentCellRender(table.getDefaultRenderer(Object.class)));

    }

    private TableCellRenderer getAlignmentCellRender(TableCellRenderer oldRender) {
        return new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component com = oldRender.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                if (com instanceof JLabel label) {
                    if (column == 0 || column == 4) {
                        label.setHorizontalAlignment(SwingConstants.CENTER);
                    } else {
                        label.setHorizontalAlignment(SwingConstants.LEADING);
                    }

                    // Check if it's the password column and it's in the table body
                    if (column == 3 && row >= 0) {
                        // Set the text as asterisks
                        label.setText("****");
                    }
                }
                return com;
            }
        };
    }


    private void init() {
        int[] columnWidths = {60, 100, 100, 60, 60};
        table.setColumnWidths(columnWidths);
        table.fixTable(scrollPane);
    }

    public JLabel getDescription() {
        return description;
    }

    public JButton getUpdateButton() {
        return updateButton;
    }

    public JButton getDeleteButton() {
        return deleteButton;
    }

    @SuppressWarnings("Duplicates")
    private void initComponents() {
        FlatMacLightLaf.setup();
        setBackground(Color.decode("#04151f"));


        JLabel lbTitle = new JLabel("Update User");
        description = new JLabel("Edit user below");

        JTextField searchBar = new JTextField();
        searchBar.putClientProperty("TextComponent.arc", 999);
        searchBar.putClientProperty("JComponent.outline", Color.decode("#edf2f4"));
        searchBar.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "\uD83D\uDD0D  Search here");
        table = new Table();

        RoundPanel roundPanel = new RoundPanel();
        roundPanel.setBackground(Color.WHITE);
        roundPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        roundPanel.setRound(10);

        JPanel updatePanel = new JPanel(new MigLayout("wrap,fillx,insets 35 45 30 45", "[fill,360]"));
        updatePanel.putClientProperty(FlatClientProperties.STYLE, "arc:10;" + "[light]background:darken(@background,3%);" + "[dark]background:lighten(@background,3%)");

        String[] userTypes = {"admin", "client"};
        typeField = new JComboBox<>(userTypes);
        nameField = new JTextField();
        emailField = new JTextField();
        passwordField = new JPasswordField();
        passwordField.putClientProperty(FlatClientProperties.STYLE, "showRevealButton:true");
        idField = new JTextField();
        updateButton = new JButton("Update");
        deleteButton = new JButton("Delete");

        typeField.putClientProperty(FlatClientProperties.STYLE, "arc:0;" + "background:#edf2f4");

        lbTitle.putClientProperty(FlatClientProperties.STYLE, "font:bold +10");
        description.putClientProperty(FlatClientProperties.STYLE, "[light]foreground:lighten(@foreground,30%);" +
                "[dark]foreground:darken(@foreground,30%)");

        updateButton.putClientProperty(FlatClientProperties.STYLE, "[light]background:#B3E0FF;" +  // Lighter blue shade
                "[dark]background:#66B2FF;" +
                "borderWidth:0;" +
                "focusWidth:0;" +
                "innerFocusWidth:0");

        deleteButton.putClientProperty(FlatClientProperties.STYLE, "[light]background:#FFCCCC;" +
                "[dark]background:#CC9999;" +
                "borderWidth:0;" +
                "focusWidth:0;" +
                "innerFocusWidth:0");

        // Add components to updatePanel
        updatePanel.add(lbTitle);
        updatePanel.add(description);
        updatePanel.add(new JLabel("Type"), "gapy 30");
        updatePanel.add(typeField);
        updatePanel.add(new JLabel("Name"), "gapy 10");
        updatePanel.add(nameField);
        updatePanel.add(new JLabel("Email"), "gapy 10");
        updatePanel.add(emailField);
        updatePanel.add(new JLabel("Password"), "gapy 10");
        updatePanel.add(passwordField);
        updatePanel.add(new JLabel("ID"), "gapy 10");
        updatePanel.add(idField);
        updatePanel.add(updateButton, "gapy 30");
        updatePanel.add(deleteButton, "gapy 10");


        table.setModel(new DefaultTableModel(
                new Object[][]{},
                new String[]{"Type", "Name", "Email", "Password", "ID"}
        ) {
            final boolean[] canEdit = new boolean[]{
                    false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        scrollPane = new JScrollPane(table);
        GroupLayout roundPanelLayout = new GroupLayout(roundPanel);
        roundPanel.setLayout(roundPanelLayout);
        roundPanelLayout.setHorizontalGroup(
                roundPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(GroupLayout.Alignment.TRAILING, roundPanelLayout.createSequentialGroup()
                                .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(GroupLayout.Alignment.TRAILING, roundPanelLayout.createSequentialGroup()
                                .addGap(30)
                                .addComponent(searchBar, 0, 50, 300)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        roundPanelLayout.setVerticalGroup(
                roundPanelLayout.createSequentialGroup()
                        .addGap(10)
                        .addGroup(roundPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(searchBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createSequentialGroup()
                        .addGap(30)  // Add gap on the left
                        .addComponent(roundPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(10) // Add horizontal space between roundPanel and updatePanel
                        .addComponent(updatePanel, GroupLayout.PREFERRED_SIZE, 380, GroupLayout.PREFERRED_SIZE)
                        .addGap(30) // Add gap on the right
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addGap(20)  // Add gap on the top
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(roundPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(updatePanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(20)
        );
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    public JTextField getNameField() {
        return nameField;
    }

    public JTextField getEmailField() {
        return emailField;
    }

    public JTextField getPasswordField() {
        return passwordField;
    }

    public JTextField getIdField() {
        return idField;
    }

    public JComboBox<String> getTypeField() {
        return typeField;
    }

    private JTextField nameField, emailField, passwordField, idField;
    private JComboBox<String> typeField;
    private JScrollPane scrollPane;
    private Table table;

}

package user.admin.view;

import com.formdev.flatlaf.FlatClientProperties;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import javax.swing.*;
import java.awt.Color;

import static shared.references.Book.languageCodes;

public class AddBookView extends JPanel {

    public AddBookView() {
        init();
    }

    private void init() {
        JLabel lbTitle = new JLabel("Add Book");
        description = new JLabel("Please fill up the fields below.");
        setBackground(Color.decode("#04151f"));
        setLayout(new MigLayout("fill,insets 20", "[center]", "[center]"));
        txtBookId = new JTextField();
        txtAuthor = new JTextField();
        txtYear = new JTextField();
        txtTitle = new JTextField();
        txtLangCode = new JComboBox<>(languageCodes);
        txtCopies = new JTextField();
        createBookButton = new JButton("Add Book");

        txtLangCode.putClientProperty(FlatClientProperties.STYLE, "arc:0;" + "background:#edf2f4");
        txtBookId.putClientProperty(FlatClientProperties.STYLE, "background:#edf2f4");
        txtBookId.setEditable(false);

        JPanel panel = new JPanel(new MigLayout("wrap,fillx,insets 35 45 30 45", "[fill,360]"));
        panel.putClientProperty(FlatClientProperties.STYLE, "arc:10;" + "[light]background:darken(@background,3%);" + "[dark]background:lighten(@background,3%)");

        txtBookId.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "Enter book id");
        txtAuthor.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "Author name");
        txtYear.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "Enter year");
        txtTitle.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "Enter title");
        txtCopies.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "Enter book copies");
        AutoCompleteDecorator.decorate(txtLangCode);

        createBookButton.putClientProperty(FlatClientProperties.STYLE, "[light]background:#B3E0FF;" +  // Lighter blue shade
                "[dark]background:#66B2FF;" +
                "borderWidth:0;" +
                "focusWidth:0;" +
                "innerFocusWidth:0");


        lbTitle.putClientProperty(FlatClientProperties.STYLE, "font:bold +10");
        description.putClientProperty(FlatClientProperties.STYLE, "[light]foreground:lighten(@foreground,30%);" +
                "[dark]foreground:darken(@foreground,30%)");

        panel.add(lbTitle);
        panel.add(description);
        panel.add(new JLabel("Book ID"), "gapy 10");
        panel.add(txtBookId);
        panel.add(new JLabel("Author"), "gapy 10");
        panel.add(txtAuthor);
        panel.add(new JLabel("Year"), "gapy 10");
        panel.add(txtYear);
        panel.add(new JLabel("Title"), "gapy 10");
        panel.add(txtTitle);
        panel.add(new JLabel("Lang Code"), "gapy 10");
        panel.add(txtLangCode);
        panel.add(new JLabel("Book Copies"), "gapy 10");
        panel.add(txtCopies);
        panel.add(createBookButton, "gapy 20");
        add(panel);
    }

    public JTextField getTxtBookId() {
        return txtBookId;
    }

    public JTextField getTxtTitle() {
        return txtTitle;
    }

    public JTextField getTxtAuthor() {
        return txtAuthor;
    }

    public JTextField getTxtYear() {
        return txtYear;
    }

    public JLabel getDescription() {
        return description;
    }

    public JTextField getTxtCopies() { return txtCopies; }

    public JComboBox<String> getTxtLangCode() {
        return txtLangCode;
    }

    public JButton getCreateBookButton() {
        return createBookButton;
    }
    private JTextField txtBookId;
    private JTextField txtTitle;
    private JTextField txtAuthor;
    private JTextField txtYear;
    private JTextField txtCopies;
    private JButton createBookButton;
    private JLabel description;
    private JComboBox<String> txtLangCode;
}

package user.admin.view;

import com.formdev.flatlaf.FlatClientProperties;
import themes.swing.adduserutil.PasswordStrengthStatus;
import net.miginfocom.swing.MigLayout;
import javax.swing.*;
import java.awt.Color;

public class AddUserView extends JPanel {

    private JTextField txtFullName;
    private JTextField txtID;
    private JTextField txtEmail;
    private JComboBox<String> txtUserType;
    private JPasswordField txtPassword;
    private JPasswordField txtConfirmPassword;
    private JLabel description;
    private JButton cmdRegister;

    public JTextField getTxtFullName() {
        return txtFullName;
    }

    public JTextField getTxtID() {
        return txtID;
    }

    public JTextField getTxtEmail() {
        return txtEmail;
    }

    public JComboBox<String> getTxtUserType() {
        return txtUserType;
    }

    public JPasswordField getTxtPassword() {
        return txtPassword;
    }

    public JPasswordField getTxtConfirmPassword() {
        return txtConfirmPassword;
    }

    public JButton getCmdRegister() {
        return cmdRegister;
    }

    public AddUserView() {
        init();
    }

    public JLabel getDescription() {
        return description;
    }

    private void init() {
        JLabel lbTitle = new JLabel("Register User");
        description = new JLabel("Please fill up the fields below.");
        setBackground(Color.decode("#04151f"));
        String[] userTypes = {"admin", "client"};
        setLayout(new MigLayout("fill,insets 20", "[center]", "[center]"));
        txtFullName = new JTextField();
        txtUserType = new JComboBox<>(userTypes);
        txtPassword = new JPasswordField();
        txtEmail = new JTextField();
        txtID = new JTextField();
        txtConfirmPassword = new JPasswordField();
        cmdRegister = new JButton("Sign Up");

        txtUserType.setEditable(false);
        txtUserType.putClientProperty(FlatClientProperties.STYLE, "arc:0;" + "background:#edf2f4");
        txtUserType.setSelectedItem("client");
        PasswordStrengthStatus passwordStrengthStatus = new PasswordStrengthStatus();
        JPanel panel = new JPanel(new MigLayout("wrap,fillx,insets 35 45 30 45", "[fill,360]"));
        panel.putClientProperty(FlatClientProperties.STYLE, "arc:10;" + "[light]background:darken(@background,3%);" + "[dark]background:lighten(@background,3%)");
        txtFullName.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "Full name");
        txtUserType.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "admin or client");
        txtPassword.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "Enter password");
        txtID.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "Enter ID number");
        txtEmail.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "Enter email address");
        txtConfirmPassword.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "Re-enter password");
        txtPassword.putClientProperty(FlatClientProperties.STYLE, "showRevealButton:true");
        txtConfirmPassword.putClientProperty(FlatClientProperties.STYLE, "showRevealButton:true");
        cmdRegister.putClientProperty(FlatClientProperties.STYLE, "[light]background:#B3E0FF;" +  // Lighter blue shade
                "[dark]background:#66B2FF;" +
                "borderWidth:0;" +
                "focusWidth:0;" +
                "innerFocusWidth:0");


        lbTitle.putClientProperty(FlatClientProperties.STYLE, "font:bold +10");
        description.putClientProperty(FlatClientProperties.STYLE, "[light]foreground:lighten(@foreground,30%);" +
                "[dark]foreground:darken(@foreground,30%)");

        passwordStrengthStatus.initPasswordField(txtPassword);

        panel.add(lbTitle);
        panel.add(description);
        panel.add(new JLabel("User Type"), "gapy 10");
        panel.add(txtUserType);
        panel.add(new JLabel("Full Name"), "gapy 10");
        panel.add(txtFullName);
        panel.add(new JLabel("ID Number"), "gapy 10");
        panel.add(txtID);
        panel.add(new JLabel("Email"), "gapy 10");
        panel.add(txtEmail);
        panel.add(new JLabel("Password"), "gapy 10");
        panel.add(txtPassword);
        panel.add(passwordStrengthStatus, "gapy 0");
        panel.add(new JLabel("Confirm Password"), "gapy 0");
        panel.add(txtConfirmPassword);
        panel.add(cmdRegister, "gapy 20");
        add(panel);
    }

    public boolean isMatchPassword() {
        String password = String.valueOf(txtPassword.getPassword());
        String confirmPassword = String.valueOf(txtConfirmPassword.getPassword());
        return password.equals(confirmPassword);
    }

    public boolean areFieldsEmpty() {
        return txtFullName.getText().isEmpty() || txtID.getText().isEmpty() ||
                txtID.getText().isEmpty() || txtPassword.getPassword().length == 0 ||
                txtConfirmPassword.getPassword().length == 0 || txtEmail.getText().isEmpty();
    }
}

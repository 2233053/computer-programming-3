package user.admin.view;

import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.themes.FlatMacLightLaf;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTextField;
import themes.swing.table.Table;
import themes.swing.panel.RoundPanel;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.Component;
import java.awt.Color;

public class UpdateBookView extends JPanel {

    public UpdateBookView() {
        initComponents();
        init();
        applyStyle(table);
    }

    @SuppressWarnings("Duplicates")
    private void applyStyle(JTable table) {

        JScrollPane scroll = (JScrollPane) table.getParent().getParent();
        scroll.setBorder(BorderFactory.createEmptyBorder());
        scroll.putClientProperty("ScrollPane.smoothScrolling", true);
        scroll.getVerticalScrollBar().putClientProperty(FlatClientProperties.STYLE, "background:$Table.background;"
                + "track:$Table.background;"
                + "trackArc:999");

        table.getTableHeader().putClientProperty(FlatClientProperties.STYLE_CLASS, "table_style");
        table.getTableHeader().setDefaultRenderer(getAlignmentCellRender(table.getTableHeader().getDefaultRenderer()));
        table.setDefaultRenderer(Object.class, getAlignmentCellRender(table.getDefaultRenderer(Object.class)));
        table.setShowHorizontalLines(true);

    }

    private TableCellRenderer getAlignmentCellRender(TableCellRenderer oldRender) {
        return new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component com = oldRender.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                if (com instanceof JLabel label) {
                    if (column == 0 || column == 4 || column == 5) {
                        label.setHorizontalAlignment(SwingConstants.CENTER);
                    } else {
                        label.setHorizontalAlignment(SwingConstants.LEADING);
                    }
                }
                return com;
            }
        };
    }


    private void init() {
        int[] columnWidths = {60, 150, 60, 150, 60, 60};
        table.setColumnWidths(columnWidths);
        table.fixTable(scrollPane);
    }

    @SuppressWarnings("Duplicates")
    private void initComponents() {
        FlatMacLightLaf.setup();
        setBackground(Color.decode("#04151f"));

        searchBar = new JTextField();
        JLabel lbTitle = new JLabel("Update Book");
        description = new JLabel("Edit book below");
        String[] availabilityItems = {"TRUE", "FALSE"};
        languageCodeField = new JComboBox<>();
        authorField = new JTextField();
        titleField = new JTextField();
        bookIDField = new JTextField();
        yearField = new JXTextField();
        titleField = new JTextField();
        availabilityField = new JComboBox<>(availabilityItems);
        copiesField = new JTextField();
        updateButton = new JButton("Update");
        deleteButton = new JButton("Delete");
        table = new Table();

        RoundPanel roundPanel = new RoundPanel();
        roundPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        roundPanel.setRound(10);

        JPanel updatePanel = new JPanel(new MigLayout("wrap,fillx,insets 35 45 30 45", "[fill,360]"));
        updatePanel.putClientProperty(FlatClientProperties.STYLE, "arc:10;" + "[light]background:darken(@background,3%);" + "[dark]background:lighten(@background,3%)");

        availabilityField.setEditable(false);
        availabilityField.putClientProperty(FlatClientProperties.STYLE, "arc:0;" + "background:#edf2f4");
        languageCodeField.putClientProperty(FlatClientProperties.STYLE, "arc:0;" + "background:#edf2f4");

        lbTitle.putClientProperty(FlatClientProperties.STYLE, "font:bold +10");
        description.putClientProperty(FlatClientProperties.STYLE, "[light]foreground:lighten(@foreground,30%);" +
                "[dark]foreground:darken(@foreground,30%)");

        updateButton.putClientProperty(FlatClientProperties.STYLE, "[light]background:#B3E0FF;" +  // Lighter blue shade
                "[dark]background:#66B2FF;" +
                "borderWidth:0;" +
                "focusWidth:0;" +
                "innerFocusWidth:0");

        deleteButton.putClientProperty(FlatClientProperties.STYLE, "[light]background:#FFCCCC;" +
                "[dark]background:#CC9999;" +
                "borderWidth:0;" +
                "focusWidth:0;" +
                "innerFocusWidth:0");


        // Add components to updatePanel
        updatePanel.add(lbTitle);
        updatePanel.add(description);
        updatePanel.add(new JLabel("Book ID"), "gapy 30");
        updatePanel.add(bookIDField);
        updatePanel.add(new JLabel("Author"), "gapy 10");
        updatePanel.add(authorField);
        updatePanel.add(new JLabel("Year"), "gapy 10");
        updatePanel.add(yearField);
        updatePanel.add(new JLabel("Title"), "gapy 10");
        updatePanel.add(titleField);
        updatePanel.add(new JLabel("Language Code"), "gapy 10");
        updatePanel.add(languageCodeField);
        updatePanel.add(new JLabel("Availability"), "gapy 10");
        updatePanel.add(availabilityField);
        updatePanel.add(new JLabel("Book Copies"), "gapy 10");
        updatePanel.add(copiesField);
        updatePanel.add(updateButton, "gapy 30");
        updatePanel.add(deleteButton, "gapy 10");


        table.setModel(new DefaultTableModel(
                new Object[][]{},
                new String[]{"ID", "Author", "Year", "Title", "Lang", "Availability", "Copies"}
        ) {
            final boolean[] canEdit = new boolean[]{
                    false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        scrollPane = new JScrollPane();
        scrollPane.setViewportView(table);
        searchBar.setBackground(Color.decode("#edf2f4"));
        searchBar.putClientProperty("TextComponent.arc", 999);
        searchBar.putClientProperty("JComponent.outline", Color.decode("#edf2f4"));
        searchBar.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "\uD83D\uDD0D  Search here");

        roundPanel.setBackground(Color.WHITE);
        GroupLayout roundPanelLayout = new GroupLayout(roundPanel);
        roundPanel.setLayout(roundPanelLayout);
        roundPanelLayout.setHorizontalGroup(
                roundPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(GroupLayout.Alignment.TRAILING, roundPanelLayout.createSequentialGroup()
                                .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(GroupLayout.Alignment.TRAILING, roundPanelLayout.createSequentialGroup()
                                .addGap(30)
                                .addComponent(searchBar, 0, 50, 300)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        roundPanelLayout.setVerticalGroup(
                roundPanelLayout.createSequentialGroup()
                        .addGap(10)
                        .addGroup(roundPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(searchBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createSequentialGroup()
                        .addGap(30)  // Add gap on the left
                        .addComponent(roundPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(10) // Add horizontal space between roundPanel and updatePanel
                        .addComponent(updatePanel, GroupLayout.PREFERRED_SIZE, 380, GroupLayout.PREFERRED_SIZE)
                        .addGap(30) // Add gap on the right
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addGap(20)  // Add gap on the top
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(roundPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)// Add gap at the bottom
                                .addComponent(updatePanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(20) // Add gap at the bottom
        );
    }

    public JTextField getSearchBar() {
        return searchBar;
    }

    public JButton getUpdateButton() {
        return updateButton;
    }

    public JButton getDeleteButton() {
        return deleteButton;
    }

    public Table getTable() {
        return table;
    }

    public JTextField getAuthorField() {
        return authorField;
    }

    public JTextField getBookIDField() {
        return bookIDField;
    }

    public JTextField getYearField() {
        return yearField;
    }

    public JTextField getTitleField() {
        return titleField;
    }

    public JTextField getCopiesField() {
        return copiesField;
    }

    public JComboBox<String> getAvailabilityField() {
        return availabilityField;
    }

    public JComboBox<String> getLanguageCodeField() {
        return languageCodeField;
    }

    public JLabel getDescription() {
        return description;
    }

    private Table table;
    private JButton updateButton, deleteButton;
    private JTextField authorField, bookIDField, yearField, titleField, copiesField;
    private JLabel description;
    private JComboBox<String> availabilityField;
    private JComboBox<String> languageCodeField;
    private JScrollPane scrollPane;
    private JTextField searchBar;

}

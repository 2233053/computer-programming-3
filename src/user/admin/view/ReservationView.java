package user.admin.view;

import com.formdev.flatlaf.FlatClientProperties;
import com.formdev.flatlaf.themes.FlatMacLightLaf;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.swingx.JXTextField;
import themes.swing.table.Table;
import themes.swing.panel.RoundPanel;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.Color;
import java.awt.Component;

public class ReservationView extends JPanel {

    private JLabel description;

    public ReservationView() {
        initComponents();
        init();
        applyStyle(table);
    }

    @SuppressWarnings("Duplicates")
    private void applyStyle(JTable table) {

        JScrollPane scroll = (JScrollPane) table.getParent().getParent();
        scroll.setBorder(BorderFactory.createEmptyBorder());
        scroll.putClientProperty("ScrollPane.smoothScrolling", true);
        scroll.getVerticalScrollBar().putClientProperty(FlatClientProperties.STYLE, "background:$Table.background;"
                + "track:$Table.background;"
                + "trackArc:999");

        table.getTableHeader().putClientProperty(FlatClientProperties.STYLE_CLASS, "table_style");
        table.getTableHeader().setDefaultRenderer(getAlignmentCellRender(table.getTableHeader().getDefaultRenderer()));
        table.setDefaultRenderer(Object.class, getAlignmentCellRender(table.getDefaultRenderer(Object.class)));
        table.setShowHorizontalLines(true);

    }

    private TableCellRenderer getAlignmentCellRender(TableCellRenderer oldRender) {
        return new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component com = oldRender.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                if (com instanceof JLabel label) {
                    if (column == 0 || column == 1 || column == 4 || column == 5 || column == 6) {
                        label.setHorizontalAlignment(SwingConstants.CENTER);
                    } else {
                        label.setHorizontalAlignment(SwingConstants.LEADING);
                    }
                }
                return com;
            }
        };
    }


    private void init() {
        int[] columnWidths = {55, 55, 55, 75, 75, 60, 60, 60, 50, 50};

        table.setColumnWidths(columnWidths);

        table.fixTable(scrollPane);
    }

    @SuppressWarnings("Duplicates")
    private void initComponents() {
        FlatMacLightLaf.setup();
        setBackground(Color.decode("#04151f"));

        searchBar = new JTextField();
        JLabel lbTitle = new JLabel("Select Reservation");
        description = new JLabel("Update reservation status below.");
        String[] status = {"TRUE", "FALSE"};
        returnField = new JComboBox<>(status);
        bookIdField = new JTextField();
        dueDateField = new JTextField();
        uIdField = new JTextField();
        studentIdField = new JTextField();
        dateField = new JXTextField();
        dateReturnedField = new JXTextField();
        approveField = new JComboBox<>(status);
        pickUpField = new JComboBox<>(status);
        overdueField = new JComboBox<>(status);
        approveButton = new JButton("Approve");
        deleteButton = new JButton("Delete");
        returnButton = new JButton("Returned");
        pickUpButton = new JButton("Picked Up");
        String[] statusOptions = {"Pending", "Approved", "Picked-up", "Returned", "Overdue"};
        statusDropdown = new JComboBox<>(statusOptions);

        table = new Table();

        RoundPanel roundPanel = new RoundPanel();
        roundPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        roundPanel.setRound(10);

        JPanel updatePanel = new JPanel(new MigLayout("wrap,fillx,insets 35 45 30 45", "[fill,360]"));
        updatePanel.putClientProperty(FlatClientProperties.STYLE, "arc:10;" + "[light]background:darken(@background,3%);" + "[dark]background:lighten(@background,3%)");

        approveField.setEditable(false);
        dateField.setEditable(false);
        dueDateField.setEditable(false);
        returnField.setEditable(false);
        pickUpField.setEditable(false);
        overdueField.setEditable(false);
        bookIdField.setEditable(false);
        uIdField.setEditable(false);
        studentIdField.setEditable(false);
        dateReturnedField.setEditable(false);
        approveField.putClientProperty(FlatClientProperties.STYLE, "arc:0;" + "background:#edf2f4");
        returnField.putClientProperty(FlatClientProperties.STYLE, "arc:0;" + "background:#edf2f4");
        pickUpField.putClientProperty(FlatClientProperties.STYLE, "arc:0;" + "background:#edf2f4");
        overdueField.putClientProperty(FlatClientProperties.STYLE, "arc:0;" + "background:#edf2f4");
        dueDateField.putClientProperty(FlatClientProperties.STYLE, "background:#edf2f4");
        dateField.putClientProperty(FlatClientProperties.STYLE, "background:#edf2f4");
        studentIdField.putClientProperty(FlatClientProperties.STYLE, "background:#edf2f4");
        bookIdField.putClientProperty(FlatClientProperties.STYLE, "background:#edf2f4");

        lbTitle.putClientProperty(FlatClientProperties.STYLE, "font:bold +10");
        description.putClientProperty(FlatClientProperties.STYLE, "[light]foreground:lighten(@foreground,30%);" +
                "[dark]foreground:darken(@foreground,30%)");

        approveButton.putClientProperty(FlatClientProperties.STYLE, "[light]background:#B3E0FF;" +  // Lighter blue shade
                "[dark]background:#66B2FF;" +
                "borderWidth:0;" +
                "focusWidth:0;" +
                "innerFocusWidth:0");

        deleteButton.putClientProperty(FlatClientProperties.STYLE, "[light]background:#FFCCCC;" +
                "[dark]background:#CC9999;" +
                "borderWidth:0;" +
                "focusWidth:0;" +
                "innerFocusWidth:0");

        statusDropdown.setFocusable(false);
        statusDropdown.setLightWeightPopupEnabled(false);

        approveButton.setEnabled(false);
        returnButton.setEnabled(false);
        pickUpButton.setEnabled(false);
        deleteButton.setEnabled(false);
        // Add components to updatePanel
        updatePanel.add(lbTitle);
        updatePanel.add(description);
        updatePanel.add(new JLabel("Student ID"), "gapy 10");
        updatePanel.add(studentIdField);
        updatePanel.add(new JLabel("Reservation ID"), "gapy 10");
        updatePanel.add(uIdField);
        updatePanel.add(new JLabel("Book ID"), "gapy 10");
        updatePanel.add(bookIdField);
        updatePanel.add(new JLabel("Reservation Date"), "gapy 10");
        updatePanel.add(dateField);
        updatePanel.add(new JLabel("Due Date"), "gapy 10");
        updatePanel.add(dueDateField);
        updatePanel.add(new JLabel("Date Returned"), "gapy 10");
        updatePanel.add(dateReturnedField);
        updatePanel.add(approveButton, "gapy 10");
        updatePanel.add(pickUpButton, "gapy 10");
        updatePanel.add(returnButton, "gapy 10");
        updatePanel.add(deleteButton, "gapy 10");



        table.setModel(new DefaultTableModel(
                new Object[][]{},
                new String[]{"Student ID", "Res. ID", "Book ID", "Reservation Date", "Due Date", "Approved", "Returned", "Picked-up", "Overdue", "Date Returned"}
        ) {
            final boolean[] canEdit = new boolean[]{
                    false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        scrollPane = new JScrollPane();
        scrollPane.setViewportView(table);
        searchBar.setBackground(Color.decode("#edf2f4"));
        searchBar.putClientProperty("TextComponent.arc", 999);
        searchBar.putClientProperty("JComponent.outline", Color.decode("#edf2f4"));
        searchBar.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "\uD83D\uDD0D  Search here");

        roundPanel.setBackground(Color.WHITE);
        GroupLayout roundPanelLayout = new GroupLayout(roundPanel);
        roundPanel.setLayout(roundPanelLayout);
        roundPanelLayout.setHorizontalGroup(
                roundPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(GroupLayout.Alignment.TRAILING, roundPanelLayout.createSequentialGroup()
                                .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(GroupLayout.Alignment.TRAILING, roundPanelLayout.createSequentialGroup()
                                .addGap(30)
                                .addComponent(searchBar, 0, 100, 300)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(statusDropdown, 0, 50, 300))
        );

        roundPanelLayout.setVerticalGroup(
                roundPanelLayout.createSequentialGroup()
                        .addGap(10)
                        .addGroup(roundPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(searchBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(statusDropdown, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );


        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createSequentialGroup()
                        .addGap(30)  // Add gap on the left
                        .addComponent(roundPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(10) // Add horizontal space between roundPanel and updatePanel
                        .addComponent(updatePanel, GroupLayout.PREFERRED_SIZE, 380, GroupLayout.PREFERRED_SIZE)
                        .addGap(30) // Add gap on the right
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addGap(20)  // Add gap on the top
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(roundPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)// Add gap at the bottom
                                .addComponent(updatePanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(20) // Add gap at the bottom
        );
    }

    public Table getTable() {
        return table;
    }

    public JLabel getDescription() {
        return description;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    public JTextField getBookIdField() {
        return bookIdField;
    }

    public JTextField getUIdField() {
        return uIdField;
    }

    public JTextField getStudentIdField() {
        return studentIdField;
    }

    public JTextField getDateField() {
        return dateField;
    }

    public JTextField getDueDateField() {
        return dueDateField;
    }

    public JTextField getDateReturnedField() {
        return dateReturnedField;
    }

    public JComboBox<String> getApproveField() {
        return approveField;
    }

    public JComboBox<String> getPickUpField() {
        return pickUpField;
    }

    public JComboBox<String> getReturnField() {
        return returnField;
    }

    public JComboBox<String> getOverdueField() {
        return overdueField;
    }

    public JButton getApproveButton() {
        return approveButton;
    }

    public JButton getDeleteButton() {
        return deleteButton;
    }

    public JButton getPickUpButton() {
        return pickUpButton;
    }

    public JButton getReturnButton() {
        return returnButton;
    }

    public JTextField getSearchBar() {
        return searchBar;
    }

    private Table table;
    private JTextField bookIdField, uIdField, studentIdField, dateField, dueDateField, dateReturnedField;
    private JComboBox<String> approveField, pickUpField, returnField, overdueField;
    private JButton approveButton, deleteButton, pickUpButton, returnButton;
    private JScrollPane scrollPane;
    private JTextField searchBar;
    public JComboBox<String> getStatusDropdown() {
        return statusDropdown;
    }
    private JComboBox<String> statusDropdown;

}

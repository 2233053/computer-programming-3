package user.admin.model;

import org.xml.sax.SAXException;
import shared.references.Book;
import shared.references.Reservation;
import shared.references.User;
import util.BookXMLProcessing;
import util.ReservationXMLProcessing;
import util.UserXMLProcessing;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.xpath.*;
import java.awt.Component;
import java.io.IOException;
import java.util.ArrayList;

public class AdminFrameModel {
    private Component currentComponent;

    public void setComponent(Component com) {
        currentComponent = com;
    }

    public Component getCurrentComponent() {
        return currentComponent;
    }

    public ArrayList<Reservation> getReservations() {
        return ReservationXMLProcessing.readReservationsFromXML();
    }

    public ArrayList<Book> getBooks() {
        return BookXMLProcessing.readBooksFromXML();
    }

    public void addUser(String userType, String fullName, String email, String password, String id) throws XPathExpressionException, ParserConfigurationException, IOException, TransformerException, SAXException {
        UserXMLProcessing.addUser(userType, fullName, email, password, id);
    }

    public void deleteUser(String id) {
        UserXMLProcessing.deleteUser(id);
    }

    public void updateUserXML(String type, String name, String email, String password, String id) {
        UserXMLProcessing.updateXML(type, name, email, password, id);
    }

    public ArrayList<User> getUsers() {
        return UserXMLProcessing.readUsersFromXML();
    }

    public boolean isIdExisting(String id) {
        return UserXMLProcessing.isIdExisting(id);
    }
    public boolean isEmailExisting(String email) {
        return UserXMLProcessing.isEmailExisting(email);
    }

    public boolean isBookIdExisting(String bookId) throws XPathExpressionException, ParserConfigurationException, IOException, SAXException {
        return BookXMLProcessing.isBookIdExisting(bookId);
    }

    public void writeBookToXML(Book book) {
        BookXMLProcessing.writeBookToXML(book);
    }

    public void updateBookXML(String bookId, String title, String author, String year, String langCode, Boolean availabilityValue, int bookCopies) throws XPathExpressionException, ParserConfigurationException, IOException, TransformerException, SAXException {
        BookXMLProcessing.updateXML(bookId, title, author, year, langCode, availabilityValue, bookCopies);
    }

    public void deleteBook(String bookId) throws XPathExpressionException, ParserConfigurationException, IOException, TransformerException, SAXException {
        BookXMLProcessing.deleteBook(bookId);
    }

    public String setAsHighestBookId() {
        return BookXMLProcessing.setAsHighestBookId();
    }

    public void deleteReservationFromXML(String studentId, String bookId, String index) {
        ReservationXMLProcessing.deleteReservationFromXML(studentId, bookId, index);
    }
}


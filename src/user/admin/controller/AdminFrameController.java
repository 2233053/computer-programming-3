package user.admin.controller;

import org.xml.sax.SAXException;
import server.Server;
import shared.references.Book;
import shared.references.Reservation;
import shared.references.User;
import user.admin.model.AdminFrameModel;
import user.admin.view.*;

import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import java.awt.*;
import java.awt.event.HierarchyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;


public class AdminFrameController {
    private final AdminFrameView adminFrameView;
    private final AdminFrameModel adminFrameModel;

    public AdminFrameController(AdminFrameView adminFrameView, AdminFrameModel adminFrameModel) {
        this.adminFrameView = adminFrameView;
        this.adminFrameModel = adminFrameModel;
        init();
    }

    public void init() {
        changeComponent();
        searchListenerReservation();
        tableListenerReservation();
        reservation();
        addUser();
        updateUser();
        updateBook();
        addBook();
        deleteUser();
    }

    public void changeComponent() {
        adminFrameView.getMenu().addEvent((index, indexSubMenu) -> {
            if (index == 0 && indexSubMenu == 0) {
                reservationDropdownStatus();
                adminFrameView.updateView(adminFrameView.getReservationView());
            } else if (index == 1 && indexSubMenu == 1) {
                adminFrameView.updateView(adminFrameView.getAddBookView());
            } else if (index == 1 && indexSubMenu == 2) {
                setBookList();
                adminFrameView.updateView(adminFrameView.getUpdateBook());
            } else if (index == 2 && indexSubMenu == 1) {
                adminFrameView.updateView(adminFrameView.getAddUser());
            } else if (index == 2 && indexSubMenu == 2) {
                setUserList();
                adminFrameView.updateView(adminFrameView.getUpdateUser());
            } else {
                adminFrameView.updateView(new Empty(index + " " + indexSubMenu));
            }
        });
        adminFrameView.getMenu().setSelectedIndex(0, 0);
    }

    public void updateUser() {
        UpdateUserView updateUserView = adminFrameView.getUpdateUser();

        updateUserView.getTable().addMouseListener(new MouseListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
                int selected = updateUserView.getTable().getSelectedRow();
                String type = updateUserView.getTable().getValueAt(selected, 0).toString();
                String name = updateUserView.getTable().getValueAt(selected, 1).toString();
                String email = updateUserView.getTable().getValueAt(selected, 2).toString();
                String password = updateUserView.getTable().getValueAt(selected, 3).toString();
                String id = updateUserView.getTable().getValueAt(selected, 4).toString();

                updateUserView.getTypeField().setSelectedItem(type);
                updateUserView.getNameField().setText(name);
                updateUserView.getEmailField().setText(email);
                updateUserView.getPasswordField().setText(password);
                updateUserView.getIdField().setText(id);
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {
            }
        });

        updateUserView.getUpdateButton().addActionListener(e -> {
            try {
                int selected = updateUserView.getTable().getSelectedRow();
                if (selected != -1) {
                    String type = (String) updateUserView.getTypeField().getSelectedItem();
                    String name = updateUserView.getNameField().getText();
                    String email = updateUserView.getEmailField().getText();
                    String password = updateUserView.getPasswordField().getText();
                    String id = updateUserView.getIdField().getText();

                    if (name.isEmpty() || email.isEmpty() || password.isEmpty() || id.isEmpty()) {
                        updateUserView.getDescription().setForeground(Color.RED);
                        updateUserView.getDescription().setText("All fields must be filled out!");
                        updateUserView.getTypeField().putClientProperty("JComponent.outline", Color.RED);
                        updateUserView.getNameField().putClientProperty("JComponent.outline", Color.RED);
                        updateUserView.getEmailField().putClientProperty("JComponent.outline", Color.RED);
                        updateUserView.getPasswordField().putClientProperty("JComponent.outline", Color.RED);
                        updateUserView.getIdField().putClientProperty("JComponent.outline", Color.RED);
                    } else {
                        updateUserView.getDescription().setText("Edit user below");
                        updateUserView.getTypeField().putClientProperty("JComponent.outline", null);
                        updateUserView.getNameField().putClientProperty("JComponent.outline", null);
                        updateUserView.getEmailField().putClientProperty("JComponent.outline", null);
                        updateUserView.getPasswordField().putClientProperty("JComponent.outline", null);
                        updateUserView.getIdField().putClientProperty("JComponent.outline", null);

                        String currentID = (String) updateUserView.getTable().getValueAt(selected, 4);
                        String currentEmail = (String) updateUserView.getTable().getValueAt(selected, 2);

                        if ((adminFrameModel.isIdExisting(id) && !id.equals(currentID))) {
                            updateUserView.getDescription().setForeground(Color.RED);
                            updateUserView.getIdField().putClientProperty("JComponent.outline", Color.RED);
                            updateUserView.getDescription().setText("User already exists!");
                        } else if (adminFrameModel.isEmailExisting(email) && !email.equals(currentEmail)) {
                            updateUserView.getDescription().setForeground(Color.RED);
                            updateUserView.getEmailField().putClientProperty("JComponent.outline", Color.RED);
                            updateUserView.getDescription().setText("Email already exists!");
                        } else {
                            adminFrameModel.deleteUser(currentID);
                            DefaultTableModel defaultTableModel = (DefaultTableModel) updateUserView.getTable().getModel();
                            defaultTableModel.setValueAt(type, selected, 0);
                            defaultTableModel.setValueAt(name, selected, 1);
                            defaultTableModel.setValueAt(email, selected, 2);
                            defaultTableModel.setValueAt(password, selected, 3);
                            defaultTableModel.setValueAt(id, selected, 4);

                            adminFrameModel.updateUserXML(type, name, email, password, id);

                            updateUserView.getDescription().setText("Successfully edited user.");
                            updateUserView.getDescription().setForeground(Color.GREEN);

                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Please select a user to update.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        });

    }

    public void addBook() {
        AddBookView addBookView = adminFrameView.getAddBookView();

        addBookView.getTxtBookId().setText(adminFrameModel.setAsHighestBookId());
        addBookView.addHierarchyListener(e -> {
            if ((e.getChangeFlags() & HierarchyEvent.SHOWING_CHANGED) != 0) {
                addBookView.getTxtBookId().setText(adminFrameModel.setAsHighestBookId());
            }
        });

        addBookView.getCreateBookButton().addActionListener(e -> {
            if (areFieldsEmpty()) {
                addBookView.getDescription().setForeground(Color.RED);
                addBookView.getDescription().setText("Please fill in all fields!");
                addBookView.getTxtTitle().putClientProperty("JComponent.outline", Color.RED);
                addBookView.getTxtBookId().putClientProperty("JComponent.outline", Color.RED);
                addBookView.getTxtAuthor().putClientProperty("JComponent.outline", Color.RED);
                addBookView.getTxtYear().putClientProperty("JComponent.outline", Color.RED);
                addBookView.getTxtLangCode().putClientProperty("JComponent.outline", Color.RED);
                addBookView.getTxtCopies().putClientProperty("JComponent.outline", Color.RED);
                return;
            } else {
                addBookView.getDescription().setText("Book has been added successfully!");
                addBookView.getDescription().setForeground(Color.GREEN);
                addBookView.getTxtTitle().putClientProperty("JComponent.outline", null);
                addBookView.getTxtBookId().putClientProperty("JComponent.outline", null);
                addBookView.getTxtAuthor().putClientProperty("JComponent.outline", null);
                addBookView.getTxtYear().putClientProperty("JComponent.outline", null);
                addBookView.getTxtLangCode().putClientProperty("JComponent.outline", null);
                addBookView.getTxtCopies().putClientProperty("JComponent.outline", null);
            }

            String bookId = adminFrameModel.setAsHighestBookId();
            String author = addBookView.getTxtAuthor().getText();
            String year = addBookView.getTxtYear().getText();
            String title = addBookView.getTxtTitle().getText();
            String langCode = (String) addBookView.getTxtLangCode().getSelectedItem();
            System.out.println(langCode);
            int bookCopies = Integer.parseInt(addBookView.getTxtCopies().getText());

            try {
                if (adminFrameModel.isBookIdExisting(bookId)) {
                    addBookView.getDescription().setForeground(Color.RED);
                    addBookView.getTxtTitle().putClientProperty("JComponent.outline", Color.RED);
                    addBookView.getDescription().setText("Book already exists!");
                    return;
                }

                adminFrameModel.writeBookToXML(new Book(bookId, title, author, year, langCode, true, bookCopies));
                clearFields();
                addBookView.getTxtBookId().setText(adminFrameModel.setAsHighestBookId());
            } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException ex) {
                System.err.println("Error: " + ex.getMessage());
            }
        });
    }

    @SuppressWarnings("Duplicates")
    public void reservation() {
        ReservationView reservationView = adminFrameView.getReservationView();

        // approve button listener
        reservationView.getApproveButton().addActionListener(e -> {
            reservationView.getDescription().setText("Book approved successfully!");
            reservationView.getDescription().setForeground(Color.GREEN);
            reservationView.getApproveButton().setEnabled(false);
            reservationView.getPickUpButton().setEnabled(true);
            try {
                int selected = reservationView.getTable().getSelectedRow();
                if (selected != -1) {
                    String studentId = reservationView.getStudentIdField().getText();
                    String uId = reservationView.getUIdField().getText();
                    String bookId = reservationView.getBookIdField().getText();
                    String dueDateStr = reservationView.getDueDateField().getText();
                    String dateStr = reservationView.getDateField().getText();
                    String returnStatus = (String) reservationView.getReturnField().getSelectedItem();
                    String approveStatus = (String) reservationView.getApproveField().getSelectedItem();
                    String pickUpStatus = (String)  reservationView.getPickUpField().getSelectedItem();
                    String dateReturnedStr = reservationView.getDateReturnedField().getText();
                    String overdueStatus = (String) reservationView.getOverdueField().getSelectedItem();

                    if (studentId.isEmpty() || Objects.requireNonNull(returnStatus).isEmpty()  || Objects.requireNonNull(pickUpStatus).isEmpty() || bookId.isEmpty() || dueDateStr.isEmpty() || dateStr.isEmpty() || Objects.requireNonNull(approveStatus).isEmpty()) {
                        reservationView.getDescription().setForeground(Color.RED);
                        reservationView.getDescription().setText("All fields must be filled out except ID!");
                        reservationView.getReturnField().putClientProperty("JComponent.outline", Color.RED);
                        reservationView.getBookIdField().putClientProperty("JComponent.outline", Color.RED);
                        reservationView.getUIdField().putClientProperty("JComponent.outline", Color.RED);
                        reservationView.getStudentIdField().putClientProperty("JComponent.outline", Color.RED);
                        reservationView.getDateField().putClientProperty("JComponent.outline", Color.RED);
                    } else {
                        // Clear error messages and update the table only when all fields are filled out
                        reservationView.getReturnField().putClientProperty("JComponent.outline", null);
                        reservationView.getBookIdField().putClientProperty("JComponent.outline", null);
                        reservationView.getUIdField().putClientProperty("JComponent.outline", null);
                        reservationView.getStudentIdField().putClientProperty("JComponent.outline", null);
                        reservationView.getDateField().putClientProperty("JComponent.outline", null);

                        LocalDate dueDate = LocalDate.parse(dueDateStr);
                        LocalDate date = LocalDate.parse(dateStr);
                        LocalDate dateReturned = LocalDate.parse(dateReturnedStr);

                        // Update the table only when all fields are filled out
                        DefaultTableModel defaultTableModel = (DefaultTableModel) reservationView.getTable().getModel();
                        defaultTableModel.setValueAt(studentId, selected, 0);
                        defaultTableModel.setValueAt(uId, selected, 1);
                        defaultTableModel.setValueAt(bookId, selected, 2);
                        defaultTableModel.setValueAt(date, selected, 3);
                        defaultTableModel.setValueAt(dueDate, selected, 4);
                        defaultTableModel.setValueAt(approveStatus, selected, 5);

                        boolean approveStatusValue = true;
                        boolean returnStatusValue = true;
                        boolean pickUpStatusValue = true;
                        boolean overdueStatusValue = false;

                        if (reservationView.getApproveField().getSelectedItem() != null) {
                            approveStatusValue = Boolean.parseBoolean(reservationView.getApproveField().getSelectedItem().toString().toUpperCase());
                        }

                        if (reservationView.getReturnField().getSelectedItem() != null) {
                            returnStatusValue = Boolean.parseBoolean(reservationView.getReturnField().getSelectedItem().toString().toUpperCase());
                        }

                        if (reservationView.getReturnField().getSelectedItem() != null) {
                            pickUpStatusValue = Boolean.parseBoolean(reservationView.getPickUpField().getSelectedItem().toString().toUpperCase());
                        }

                        if (reservationView.getOverdueField().getSelectedItem() != null) {
                            overdueStatusValue = Boolean.parseBoolean(reservationView.getOverdueField().getSelectedItem().toString().toUpperCase());
                        }

                        Server.adminUpdatedReservationXML(new Reservation(studentId, uId, bookId, date, dueDate, approveStatusValue, returnStatusValue,
                                pickUpStatusValue, dateReturned, overdueStatusValue ), "APPROVE", "TRUE");
                        Server.writeReturnedBookUpdateToXML(bookId, "FALSE");

                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Please select a user to update.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } catch (NullPointerException exception) {
                System.err.println("BOOLEAN VALUES: " + exception.getMessage());
            } catch (Exception ex) {
                System.err.println("approveButton.addActionListener: " + ex.getMessage());
                throw new RuntimeException(ex);
            }
        });

        // pickup button listener
        reservationView.getPickUpButton().addActionListener(e -> {
            reservationView.getDescription().setText("");
            reservationView.getDescription().setText("Book has been picked up.");
            reservationView.getDescription().setForeground(Color.ORANGE);
            reservationView.getPickUpButton().setEnabled(false);
            reservationView.getPickUpButton().setEnabled(true);
            try {
                int selected = reservationView.getTable().getSelectedRow();
                if (selected != -1) {
                    String studentId = reservationView.getStudentIdField().getText();
                    String uId = reservationView.getUIdField().getText();
                    String bookId = reservationView.getBookIdField().getText();
                    String dueDateStr = reservationView.getDueDateField().getText();
                    String dateStr = reservationView.getDateField().getText();
                    String returnStatus = (String) reservationView.getReturnField().getSelectedItem();
                    String approveStatus = (String) reservationView.getApproveField().getSelectedItem();
                    String pickUpStatus = (String)  reservationView.getPickUpField().getSelectedItem();
                    String dateReturnedStr = reservationView.getDateReturnedField().getText();
                    String overdueStatus = (String) reservationView.getOverdueField().getSelectedItem();

                    if (studentId.isEmpty() || Objects.requireNonNull(returnStatus).isEmpty()  || Objects.requireNonNull(pickUpStatus).isEmpty() || bookId.isEmpty() || dueDateStr.isEmpty() || dateStr.isEmpty() || Objects.requireNonNull(approveStatus).isEmpty()) {
                        reservationView.getDescription().setForeground(Color.RED);
                        reservationView.getDescription().setText("All fields must be filled out except ID!");
                        reservationView.getReturnField().putClientProperty("JComponent.outline", Color.RED);
                        reservationView.getBookIdField().putClientProperty("JComponent.outline", Color.RED);
                        reservationView.getUIdField().putClientProperty("JComponent.outline", Color.RED);
                        reservationView.getStudentIdField().putClientProperty("JComponent.outline", Color.RED);
                        reservationView.getDateField().putClientProperty("JComponent.outline", Color.RED);
                    } else {
                        // Clear error messages and update the table only when all fields are filled out
                        reservationView.getReturnField().putClientProperty("JComponent.outline", null);
                        reservationView.getBookIdField().putClientProperty("JComponent.outline", null);
                        reservationView.getUIdField().putClientProperty("JComponent.outline", null);
                        reservationView.getStudentIdField().putClientProperty("JComponent.outline", null);
                        reservationView.getDateField().putClientProperty("JComponent.outline", null);

                        LocalDate dueDate = LocalDate.parse(dueDateStr);
                        LocalDate date = LocalDate.parse(dateStr);
                        LocalDate dateReturned = LocalDate.parse(dateReturnedStr);

                        // Update the table only when all fields are filled out
                        DefaultTableModel defaultTableModel = (DefaultTableModel) reservationView.getTable().getModel();
                        defaultTableModel.setValueAt(studentId, selected, 0);
                        defaultTableModel.setValueAt(bookId, selected, 2);
                        defaultTableModel.setValueAt(date, selected, 3);
                        defaultTableModel.setValueAt(dueDate, selected, 4);
                        defaultTableModel.setValueAt(pickUpStatus, selected, 7);

                        boolean approveStatusValue = true;
                        boolean returnStatusValue = true;
                        boolean pickUpStatusValue = true;
                        boolean overdueStatusValue = false;

                        if (reservationView.getApproveField().getSelectedItem() != null) {
                            approveStatusValue = Boolean.parseBoolean(reservationView.getApproveField().getSelectedItem().toString().toUpperCase());
                        }

                        if (reservationView.getReturnField().getSelectedItem() != null) {
                            returnStatusValue = Boolean.parseBoolean(reservationView.getReturnField().getSelectedItem().toString().toUpperCase());
                        }

                        if (reservationView.getPickUpField().getSelectedItem() != null) {
                            pickUpStatusValue = Boolean.parseBoolean(reservationView.getPickUpField().getSelectedItem().toString().toUpperCase());
                        }

                        Server.adminUpdatedReservationXML(new Reservation(studentId, uId, bookId, date, dueDate, approveStatusValue, returnStatusValue, pickUpStatusValue, dateReturned, overdueStatusValue ), "PICKUP" , "TRUE");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Please select a user to update.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        });

        // return button listener
        reservationView.getReturnButton().addActionListener(e -> {
            reservationView.getReturnButton().setEnabled(false);

            Server.writeStringHistoryToXML(reservationView.getStudentIdField().getText(), reservationView.getBookIdField().getText());
            try {
                int selected = reservationView.getTable().getSelectedRow();
                if (selected != -1) {
                    String studentId = reservationView.getStudentIdField().getText();
                    String uId = reservationView.getUIdField().getText();
                    String bookId = reservationView.getBookIdField().getText();
                    String dueDateStr = reservationView.getDueDateField().getText();
                    String dateStr = reservationView.getDateField().getText();
                    String returnStatus = (String) reservationView.getReturnField().getSelectedItem();
                    String approveStatus = (String) reservationView.getApproveField().getSelectedItem();
                    String pickUpStatus = (String)  reservationView.getPickUpField().getSelectedItem();
                    String dateReturnedStr = reservationView.getDateReturnedField().getText();
                    String overdueStatus = (String) reservationView.getOverdueField().getSelectedItem();

                    if (studentId.isEmpty() || Objects.requireNonNull(returnStatus).isEmpty()  || Objects.requireNonNull(pickUpStatus).isEmpty() || bookId.isEmpty() || dueDateStr.isEmpty() || dateStr.isEmpty() || Objects.requireNonNull(approveStatus).isEmpty()) {
                        reservationView.getDescription().setForeground(Color.RED);
                        reservationView.getDescription().setText("All fields must be filled out except ID!");
                        reservationView.getReturnField().putClientProperty("JComponent.outline", Color.RED);
                        reservationView.getBookIdField().putClientProperty("JComponent.outline", Color.RED);
                        reservationView.getUIdField().putClientProperty("JComponent.outline", Color.RED);
                        reservationView.getStudentIdField().putClientProperty("JComponent.outline", Color.RED);
                        reservationView.getDateField().putClientProperty("JComponent.outline", Color.RED);
                    } else {
                        // Clear error messages and update the table only when all fields are filled out
                        reservationView.getReturnField().putClientProperty("JComponent.outline", null);
                        reservationView.getBookIdField().putClientProperty("JComponent.outline", null);
                        reservationView.getStudentIdField().putClientProperty("JComponent.outline", null);
                        reservationView.getUIdField().putClientProperty("JComponent.outline", null);
                        reservationView.getDateField().putClientProperty("JComponent.outline", null);

                        LocalDate dueDate = LocalDate.parse(dueDateStr);
                        LocalDate date = LocalDate.parse(dateStr);
                        LocalDate dateReturned = LocalDate.parse(dateReturnedStr);


                        // Update the table only when all fields are filled out
                        DefaultTableModel defaultTableModel = (DefaultTableModel) reservationView.getTable().getModel();
                        defaultTableModel.setValueAt(studentId, selected, 0);
                        defaultTableModel.setValueAt(bookId, selected, 2);
                        defaultTableModel.setValueAt(date, selected, 3);
                        defaultTableModel.setValueAt(dueDate, selected, 4);
                        defaultTableModel.setValueAt(returnStatus, selected, 6);

                        boolean approveStatusValue = true;
                        boolean returnStatusValue = true;
                        boolean pickUpStatusValue = true;
                        boolean overdueStatusValue = false;

                        if (reservationView.getApproveField().getSelectedItem() != null) {
                            approveStatusValue = Boolean.parseBoolean(reservationView.getApproveField().getSelectedItem().toString().toUpperCase());
                        }

                        if (reservationView.getReturnField().getSelectedItem() != null) {
                            returnStatusValue = Boolean.parseBoolean(reservationView.getReturnField().getSelectedItem().toString().toUpperCase());
                        }

                        if (reservationView.getPickUpField().getSelectedItem() != null) {
                            pickUpStatusValue = Boolean.parseBoolean(reservationView.getPickUpField().getSelectedItem().toString().toUpperCase());
                        }

                        Server.adminUpdatedReservationXML(new Reservation(studentId, uId, bookId, date, dueDate, approveStatusValue, returnStatusValue, pickUpStatusValue, dateReturned, overdueStatusValue), "RETURN" , "TRUE");
                        Server.writeReturnedBookUpdateToXML(bookId, "TRUE");
                        Server.writeBookCopiesUpdateToXML(bookId);
                        Boolean isOverdue = Server.adminDueCheckXML(new Reservation(studentId, uId, bookId, date, dueDate, approveStatusValue, returnStatusValue, pickUpStatusValue, dateReturned, overdueStatusValue));
                        if (Boolean.TRUE.equals(isOverdue)) {
                            reservationView.getDescription().setText("Book has been returned.");
                            reservationView.getDescription().setForeground(Color.ORANGE);
                            reservationView.getDescription().setText("Book is overdue.");
                            reservationView.getDescription().setForeground(Color.RED);
                        } else {
                            reservationView.getDescription().setText("Book has been returned.");
                            reservationView.getDescription().setForeground(Color.ORANGE);
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Please select a user to update.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        });

        // delete button listener
        reservationView.getDeleteButton().addActionListener(e -> {
            reservationView.getDescription().setText("Reservation deleted.");
            reservationView.getDescription().setForeground(Color.RED);
            reservationView.getApproveButton().setEnabled(false);
            reservationView.getPickUpButton().setEnabled(false);
            reservationView.getReturnButton().setEnabled(false);
            reservationView.getDeleteButton().setEnabled(false);
            try {
                int selected = reservationView.getTable().getSelectedRow();
                String studentId = reservationView.getStudentIdField().getText();
                String uId = reservationView.getUIdField().getText();
                String bookId = reservationView.getBookIdField().getText(); // Get the selected user ID from the ID field
                String isReturned = (String) reservationView.getReturnField().getSelectedItem();

                DefaultTableModel defaultTableModel = (DefaultTableModel) reservationView.getTable().getModel();
                if (selected >= 0) {
                    reservationView.getSearchBar().setText("");
                    defaultTableModel.removeRow(selected);
                } else {
                    System.out.println("Select a row");
                }
                Server.writeReturnedBookUpdateToXML(bookId, "TRUE");

                adminFrameModel.deleteReservationFromXML(studentId, bookId, uId);
                System.out.println("RESERVATION DELETED.");
                if (!Objects.requireNonNull(isReturned).equalsIgnoreCase("TRUE")) {
                    Server.writeBookCopiesUpdateToXML(bookId);
                }

                reservationView.getStudentIdField().setText("");
                reservationView.getBookIdField().setText("");
                reservationView.getDueDateField().setText("");
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }

        });
        adminFrameView.updateView(adminFrameView.getReservationView());
    }

    public void updateBook() {
     UpdateBookView updateBookView = adminFrameView.getUpdateBook();

     for (String langCode : Book.languageCodes) {
         updateBookView.getLanguageCodeField().addItem(langCode);
     }

        adminFrameView.getUpdateBook().getUpdateButton().addActionListener(e -> {
            try {
                int selected = updateBookView.getTable().getSelectedRow();
                if (selected != -1) {
                    String bookId = updateBookView.getBookIDField().getText();
                    String author = updateBookView.getAuthorField().getText();
                    String title = updateBookView.getTitleField().getText();
                    String year = updateBookView.getYearField().getText();
                    String langCode = (String) updateBookView.getLanguageCodeField().getSelectedItem();
                    String availability = (String) updateBookView.getAvailabilityField().getSelectedItem();
                    int bookCopies = Integer.parseInt(updateBookView.getCopiesField().getText());

                    if (bookId.isEmpty() || Objects.requireNonNull(langCode).isEmpty() || author.isEmpty() || title.isEmpty() || year.isEmpty() || Objects.requireNonNull(availability).isEmpty()) {
                        updateBookView.getDescription().setForeground(Color.RED);
                        updateBookView.getDescription().setText("All fields must be filled out except ID!");
                        updateBookView.getLanguageCodeField().putClientProperty("JComponent.outline", Color.RED);
                        updateBookView.getAuthorField().putClientProperty("JComponent.outline", Color.RED);
                        updateBookView.getBookIDField().putClientProperty("JComponent.outline", Color.RED);
                        updateBookView.getYearField().putClientProperty("JComponent.outline", Color.RED);
                        updateBookView.getCopiesField().putClientProperty("JComponent.outline", Color.RED);
                    } else {
                        // Clear error messages and update the table only when all fields are filled out
                        updateBookView.getDescription().setText("Edit book below.");
                        updateBookView.getLanguageCodeField().putClientProperty("JComponent.outline", null);
                        updateBookView.getAuthorField().putClientProperty("JComponent.outline", null);
                        updateBookView.getBookIDField().putClientProperty("JComponent.outline", null);
                        updateBookView.getYearField().putClientProperty("JComponent.outline", null);
                        updateBookView.getCopiesField().putClientProperty("JComponent.outline", null);

                        try {
                            String currentBookId = updateBookView.getTable().getValueAt(selected, 0).toString();

                            if (adminFrameModel.isBookIdExisting(bookId) && !bookId.equalsIgnoreCase(currentBookId)) {
                                updateBookView.getDescription().setForeground(Color.RED);
                                updateBookView.getDescription().setText("Book already exists!");

                            } else {
                                DefaultTableModel defaultTableModel = (DefaultTableModel) updateBookView.getTable().getModel();
                                defaultTableModel.setValueAt(bookId, selected, 0);
                                defaultTableModel.setValueAt(author, selected, 1);
                                defaultTableModel.setValueAt(year, selected, 2);
                                defaultTableModel.setValueAt(title, selected, 3);
                                defaultTableModel.setValueAt(langCode, selected, 4);
                                defaultTableModel.setValueAt(bookCopies, selected, 6);
                                boolean availabilityValue = true;

                                if (updateBookView.getAvailabilityField().getSelectedItem() != null) {
                                    availabilityValue = Boolean.parseBoolean(updateBookView.getAvailabilityField().getSelectedItem().toString().toUpperCase());
                                }

                                updateBookView.getDescription().setText("Book updated successfully!");
                                updateBookView.getDescription().setForeground(Color.GREEN);

                                adminFrameModel.updateBookXML(bookId, title, author, year, langCode, availabilityValue, bookCopies);
                                clearFields();
                            }
                        } catch (ParserConfigurationException | SAXException | IOException |
                                 XPathExpressionException ex) {
                            System.err.println("Error: " + ex.getMessage());
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Please select a user to update.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        });

        updateBookView.getDeleteButton().addActionListener(e -> {
            updateBookView.getDescription().setText("Book has been deleted.");
            updateBookView.getDescription().setForeground(Color.GREEN);

            try {
                int selected = updateBookView.getTable().getSelectedRow();
                String bookId = updateBookView.getBookIDField().getText(); // Get the selected user ID from the ID field

                DefaultTableModel defaultTableModel = (DefaultTableModel) updateBookView.getTable().getModel();
                if (selected >= 0) {
                    updateBookView.getSearchBar().setText(""); // Clear the search bar to reset the filter
                    defaultTableModel.removeRow(selected); // Remove the row from the table model
                } else {
                    System.out.println("Select a row");
                }

                adminFrameModel.deleteBook(bookId);

            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }

            updateBookView.getBookIDField().setText("");
            updateBookView.getAuthorField().setText("");
            updateBookView.getYearField().setText("");
            updateBookView.getTitleField().setText("");
            updateBookView.getCopiesField().setText("");
        });

        updateBookView.getTable().addMouseListener(new MouseListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
                int selected = updateBookView.getTable().getSelectedRow();
                String id = updateBookView.getTable().getValueAt(selected, 0).toString();
                String author = updateBookView.getTable().getValueAt(selected, 1).toString();
                String year = updateBookView.getTable().getValueAt(selected, 2).toString();
                String title = updateBookView.getTable().getValueAt(selected, 3).toString();
                String langCode = updateBookView.getTable().getValueAt(selected, 4).toString();
                String availability = updateBookView.getTable().getValueAt(selected, 5).toString();
                String bookCopies = updateBookView.getTable().getValueAt(selected, 6).toString();

                updateBookView.getBookIDField().setText(id);
                updateBookView.getAuthorField().setText(author);
                updateBookView.getYearField().setText(year);
                updateBookView.getTitleField().setText(title);
                updateBookView.getLanguageCodeField().setSelectedItem(langCode);
                updateBookView.getAvailabilityField().setSelectedItem(availability);
                updateBookView.getCopiesField().setText(String.valueOf(bookCopies));
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {
            }
        });

        updateBookView.getSearchBar().getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateTable();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateTable();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updateTable();
            }

            private void updateTable() {
                String searchText = updateBookView.getSearchBar().getText(); // Get the search text
                updateBookView.getTable().filterRows(searchText); // Filter rows based on the search text
            }
        });
    }

    public boolean areFieldsEmpty() {
        AddBookView addBookView = adminFrameView.getAddBookView();
        return addBookView.getTxtBookId().getText().isEmpty() || addBookView.getTxtTitle().getText().isEmpty() ||
                addBookView.getTxtYear().getText().isEmpty() || addBookView.getTxtAuthor().getText().isEmpty() ||
                addBookView.getTxtCopies().getText().isEmpty();
    }

    public void clearFields() {
        AddBookView addBookView = adminFrameView.getAddBookView();
        addBookView.getTxtAuthor().setText("");
        addBookView.getTxtYear().setText("");
        addBookView.getTxtTitle().setText("");
        addBookView.getTxtCopies().setText("");
    }

    public void addUser() {
        adminFrameView.getAddUser().getCmdRegister().addActionListener(e -> {
            if (adminFrameView.getAddUser().areFieldsEmpty()) {
                adminFrameView.getAddUser().getDescription().setForeground(Color.RED);
                adminFrameView.getAddUser().getDescription().setText("Please fill in all fields!");
                adminFrameView.getAddUser().getTxtID().putClientProperty("JComponent.outline", Color.RED);
                adminFrameView.getAddUser().getTxtEmail().putClientProperty("JComponent.outline", Color.RED);
                adminFrameView.getAddUser().getTxtFullName().putClientProperty("JComponent.outline", Color.RED);
                adminFrameView.getAddUser().getTxtPassword().putClientProperty("JComponent.outline", Color.RED);
                adminFrameView.getAddUser().getTxtConfirmPassword().putClientProperty("JComponent.outline", Color.RED);
                return;
            } else {
                adminFrameView.getAddUser().getDescription().setForeground(Color.GREEN);
                adminFrameView.getAddUser().getDescription().setText("Successfully added user!");
                adminFrameView.getAddUser().getTxtID().putClientProperty("JComponent.outline", null);
                adminFrameView.getAddUser().getTxtEmail().putClientProperty("JComponent.outline", null);
                adminFrameView.getAddUser().getTxtFullName().putClientProperty("JComponent.outline", null);
                adminFrameView.getAddUser().getTxtPassword().putClientProperty("JComponent.outline", null);
                adminFrameView.getAddUser().getTxtConfirmPassword().putClientProperty("JComponent.outline", null);
            }

            if (!adminFrameView.getAddUser().isMatchPassword()) {
                adminFrameView.getAddUser().getDescription().setForeground(Color.RED);
                adminFrameView.getAddUser().getDescription().setText("Passwords don't match. Try again!");
                adminFrameView.getAddUser().getTxtPassword().putClientProperty("JComponent.outline", Color.RED);
                adminFrameView.getAddUser().getTxtConfirmPassword().putClientProperty("JComponent.outline", Color.RED);
                return;
            }

            String userType = (String) adminFrameView.getAddUser().getTxtUserType().getSelectedItem();
            String fullName = adminFrameView.getAddUser().getTxtFullName().getText();
            String id = adminFrameView.getAddUser().getTxtID().getText();
            String email = adminFrameView.getAddUser().getTxtEmail().getText();
            String password = String.valueOf(adminFrameView.getAddUser().getTxtPassword().getPassword());


            try {
                if (adminFrameModel.isIdExisting(id)) {
                    adminFrameView.getAddUser().getDescription().setForeground(Color.RED);
                    adminFrameView.getAddUser().getTxtID().putClientProperty("JComponent.outline", Color.RED);
                    adminFrameView.getAddUser().getDescription().setText("User already exists!");
                    return;
                } else if (adminFrameModel.isEmailExisting(email)) {
                    adminFrameView.getAddUser().getDescription().setForeground(Color.RED);
                    adminFrameView.getAddUser().getTxtEmail().putClientProperty("JComponent.outline", Color.RED);
                    adminFrameView.getAddUser().getDescription().setText("Email already exists!");
                    return;
                }

                adminFrameModel.addUser(userType, fullName, email, password, id);

                adminFrameView.getAddUser().getTxtID().setText("");
                adminFrameView.getAddUser().getTxtEmail().setText("");
                adminFrameView.getAddUser().getTxtFullName().setText("");
                adminFrameView.getAddUser().getTxtPassword().setText("");
                adminFrameView.getAddUser().getTxtConfirmPassword().setText("");

            } catch (ParserConfigurationException | SAXException | TransformerException | IOException |
                     XPathExpressionException ex) {
                JOptionPane.showMessageDialog(adminFrameView.getAddUser(), "An error occurred while creating the user.", "Error", JOptionPane.ERROR_MESSAGE);
                System.err.println("Error: " + ex.getMessage());
            }
        });
    }

    public void deleteUser() {
        adminFrameView.getUpdateUser().getDeleteButton().addActionListener(e -> {
            adminFrameView.getUpdateUser().getDescription().setText("User deleted successfully.");
            adminFrameView.getUpdateUser().getDescription().setForeground(Color.RED);

            try {
                int selected = adminFrameView.getUpdateUser().getTable().getSelectedRow();
                String id = adminFrameView.getUpdateUser().getIdField().getText(); // Get the selected user ID from the ID field


                DefaultTableModel defaultTableModel = (DefaultTableModel) adminFrameView.getUpdateUser().getTable().getModel();
                if (selected >= 0) {
                    defaultTableModel.removeRow(selected);
                } else {
                    System.out.println("Select a row");
                }

                adminFrameModel.deleteUser(id);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }

            adminFrameView.getUpdateUser().getNameField().setText("");
            adminFrameView.getUpdateUser().getIdField().setText("");
            adminFrameView.getUpdateUser().getPasswordField().setText("");
            adminFrameView.getUpdateUser().getEmailField().setText("");
        });
    }

    public void update() {
        adminFrameView.getUpdateUser().getUpdateButton().addActionListener(e -> {
            try {
                int selected = adminFrameView.getUpdateUser().getTable().getSelectedRow();
                if (selected != -1) {
                    String type = (String) adminFrameView.getUpdateUser().getTypeField().getSelectedItem();
                    String name = adminFrameView.getUpdateUser().getNameField().getText();
                    String email = adminFrameView.getUpdateUser().getEmailField().getText();
                    String password = adminFrameView.getUpdateUser().getPasswordField().getText();
                    String id = adminFrameView.getUpdateUser().getIdField().getText();

                    if (name.isEmpty() || email.isEmpty() || password.isEmpty() || id.isEmpty()) {
                        adminFrameView.getUpdateUser().getDescription().setForeground(Color.RED);
                        adminFrameView.getUpdateUser().getDescription().setText("All fields must be filled out!");
                        adminFrameView.getUpdateUser().getNameField().putClientProperty("JComponent.outline", Color.RED);
                        adminFrameView.getUpdateUser().getNameField().putClientProperty("JComponent.outline", Color.RED);
                        adminFrameView.getUpdateUser().getEmailField().putClientProperty("JComponent.outline", Color.RED);
                        adminFrameView.getUpdateUser().getPasswordField().putClientProperty("JComponent.outline", Color.RED);
                        adminFrameView.getUpdateUser().getIdField().putClientProperty("JComponent.outline", Color.RED);
                    } else {
                        adminFrameView.getUpdateUser().getDescription().setText("Edit user below");
                        adminFrameView.getUpdateUser().getTypeField().putClientProperty("JComponent.outline", null);
                        adminFrameView.getUpdateUser().getNameField().putClientProperty("JComponent.outline", null);
                        adminFrameView.getUpdateUser().getEmailField().putClientProperty("JComponent.outline", null);
                        adminFrameView.getUpdateUser().getPasswordField().putClientProperty("JComponent.outline", null);
                        adminFrameView.getUpdateUser().getIdField().putClientProperty("JComponent.outline", null);

                        String currentID = (String) adminFrameView.getUpdateUser().getTable().getValueAt(selected, 4);
                        String currentEmail = (String) adminFrameView.getUpdateUser().getTable().getValueAt(selected, 2);

                        if ((adminFrameModel.isIdExisting(id) && !id.equals(currentID))) {
                            adminFrameView.getUpdateUser().getDescription().setForeground(Color.RED);
                            adminFrameView.getUpdateUser().getIdField().putClientProperty("JComponent.outline", Color.RED);
                            adminFrameView.getUpdateUser().getDescription().setText("User already exists!");
                        } else if (adminFrameModel.isEmailExisting(email) && !email.equals(currentEmail)) {
                            adminFrameView.getUpdateUser().getDescription().setForeground(Color.RED);
                            adminFrameView.getUpdateUser().getEmailField().putClientProperty("JComponent.outline", Color.RED);
                            adminFrameView.getUpdateUser().getDescription().setText("Email already exists!");
                        } else {
                            adminFrameModel.deleteUser(currentID);
                            DefaultTableModel defaultTableModel = (DefaultTableModel) adminFrameView.getUpdateUser().getTable().getModel();
                            defaultTableModel.setValueAt(type, selected, 0);
                            defaultTableModel.setValueAt(name, selected, 1);
                            defaultTableModel.setValueAt(email, selected, 2);
                            defaultTableModel.setValueAt(password, selected, 3);
                            defaultTableModel.setValueAt(id, selected, 4);

                            adminFrameModel.updateUserXML(type, name, email, password, id);

                            adminFrameView.getUpdateUser().getDescription().setForeground(Color.GREEN);
                            adminFrameView.getUpdateUser().getDescription().setText("Successfully edited user.");
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Please select a user to update.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        });
    }

    public void setBookList() {
        try {
            DefaultTableModel model = (DefaultTableModel) adminFrameView.getUpdateBook().getTable().getModel();
            model.setRowCount(0);
            ArrayList<Book> books = adminFrameModel.getBooks();
            for (Book book : books) {
                String bookId = book.getBookID();
                String title = book.getTitle();
                String author = book.getAuthor();
                String year = book.getPublicationYear();
                String langCode = book.getLanguageCode();
                String availability = String.valueOf(book.isAvailable());
                String bookCopies = String.valueOf(book.getBookCopies());
                Object[] row = {bookId, author, year, title, langCode, availability, bookCopies};
                model.addRow(row);
            }
        } catch (Exception e) {
            System.out.println("SET BOOK LIST: " + e.getMessage());
        }
    }

    public void setUserList() {
        try {
            DefaultTableModel model = (DefaultTableModel) adminFrameView.getUpdateUser().getTable().getModel();
            model.setRowCount(0);
            ArrayList<User> users = adminFrameModel.getUsers();
            for (User user : users) {
                String type = user.getType();
                String name = user.getName();
                String email = user.getEmail();
                String password = user.getPassword();
                String ID = user.getID();
                Object[] row = {type, name, email, password, ID};
                model.addRow(row);
            }
        } catch (Exception e) {
            System.out.println("SET USER LIST: " + e.getMessage());
        }
    }

    public void reservationList(String status) {
        try {
            DefaultTableModel model = (DefaultTableModel) adminFrameView.getReservationView().getTable().getModel();
            model.setRowCount(0);

            ArrayList<Reservation> reservations = adminFrameModel.getReservations();

            switch (status) {
                case "Pending" -> {
                    for (Reservation res : reservations) {
                        if (!res.isApproved() && !res.isOverdue() && !res.isReturnStatus() && !res.isPickUpStatus()) {
                            setReservationList(res, model);
                        }
                    }
                }
                case "Approved" -> {
                    for (Reservation res : reservations) {
                        if (res.isApproved() && !res.isOverdue() && !res.isReturnStatus() && !res.isPickUpStatus()) {
                            setReservationList(res, model);
                        }
                    }
                }
                case "Picked-up" -> {
                    for (Reservation res : reservations) {
                        if (res.isApproved() && !res.isOverdue() && !res.isReturnStatus() && res.isPickUpStatus()) {
                            setReservationList(res, model);
                        }
                    }
                }
                case "Returned" -> {
                    for (Reservation res : reservations) {
                        if (res.isApproved() && !res.isOverdue() && res.isReturnStatus() && res.isPickUpStatus()) {
                            setReservationList(res, model);
                        }
                    }
                }
                case "Overdue" -> {
                    for (Reservation res : reservations) {
                        if (res.isApproved() && res.isOverdue() && res.isReturnStatus() && res.isPickUpStatus()) {
                            setReservationList(res, model);
                        }
                    }
                }

                default -> throw new IllegalStateException("Unexpected value: " + status);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public void setReservationList(Reservation res, DefaultTableModel model) {
        String studentId = res.getStudentId();
        String uId = res.getUId();
        String bookId = res.getBookId();
        String reservationDate = res.getReservationDate().toString();
        String dueDate = res.getDueDate().toString();
        String approved = String.valueOf(res.isApproved()).toUpperCase();
        String returned = String.valueOf(res.isReturnStatus()).toUpperCase();
        String pickedUp = String.valueOf(res.isPickUpStatus()).toUpperCase();
        String overdue = String.valueOf(res.isOverdue());
        String dateReturned = res.getDateReturned().toString();
        Object[] row = {studentId, uId, bookId, reservationDate, dueDate, approved, returned, pickedUp, overdue, dateReturned};
        model.addRow(row);
    }

    public void reservationDropdownStatus() {
        reservationList("Pending");
        adminFrameView.getReservationView().getStatusDropdown().setSelectedItem("Pending");
        adminFrameView.getReservationView().getStatusDropdown().addActionListener(e -> {

            String selectedOption = (String) adminFrameView.getReservationView().getStatusDropdown().getSelectedItem();

            if (selectedOption != null) {
                switch (selectedOption) {
                    case "Pending" -> reservationList("Pending");
                    case "Approved" -> reservationList("Approved");
                    case "Picked-up" -> reservationList("Picked-up");
                    case "Returned" -> reservationList("Returned");
                    case "Overdue" -> reservationList("Overdue");
                }
            }
        });
    }

    public void searchListenerReservation() {
        adminFrameView.getReservationView().getSearchBar().getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateTable();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateTable();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updateTable();
            }

            private void updateTable() {
                String searchText = adminFrameView.getReservationView().getSearchBar().getText(); // Get the search text
                adminFrameView.getReservationView().getTable().filterRows(searchText); // Filter rows based on the search text
            }
        });
    }

    public void tableListenerReservation() {
        adminFrameView.getReservationView().getTable().addMouseListener(new MouseListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
                adminFrameView.getReservationView().getDeleteButton().setEnabled(true);

                int selected =  adminFrameView.getReservationView().getTable().getSelectedRow();
                String studentId =  adminFrameView.getReservationView().getTable().getValueAt(selected, 0).toString();
                String uId = adminFrameView.getReservationView().getTable().getValueAt(selected, 1).toString();
                String bookId =  adminFrameView.getReservationView().getTable().getValueAt(selected, 2).toString();
                String date =  adminFrameView.getReservationView().getTable().getValueAt(selected, 3).toString();
                String dueDate =  adminFrameView.getReservationView().getTable().getValueAt(selected, 4).toString();
                String approve =  adminFrameView.getReservationView().getTable().getValueAt(selected, 5).toString();
                String returned =  adminFrameView.getReservationView().getTable().getValueAt(selected, 6).toString();
                String pickUp =  adminFrameView.getReservationView().getTable().getValueAt(selected, 7).toString();
                String overdue =  adminFrameView.getReservationView().getTable().getValueAt(selected, 8).toString();
                String dateReturned =  adminFrameView.getReservationView().getTable().getValueAt(selected, 9).toString();


                adminFrameView.getReservationView().getStudentIdField().setText(studentId);
                adminFrameView.getReservationView().getUIdField().setText(uId);
                adminFrameView.getReservationView().getBookIdField().setText(bookId);
                adminFrameView.getReservationView().getDateField().setText(date);
                adminFrameView.getReservationView().getDueDateField().setText(dueDate);
                adminFrameView.getReservationView().getApproveField().setSelectedItem(approve);
                adminFrameView.getReservationView().getReturnField().setSelectedItem(returned);
                adminFrameView.getReservationView().getPickUpField().setSelectedItem(pickUp);
                adminFrameView.getReservationView().getOverdueField().setSelectedItem(overdue);
                adminFrameView.getReservationView().getDateReturnedField().setText(dateReturned);

                System.out.println("SELECTED: \t" + selected);
                System.out.println("APPROVED: \t" + approve);
                System.out.println("RETURN: \t" + returned);
                System.out.println("PICKUP:\t " + pickUp);
                System.out.println("OVERDUE:\t " + overdue);
                System.out.println("DATE RETURNED: " + dateReturned);

                if (approve.equals("FALSE")) {
                    adminFrameView.getReservationView().getApproveButton().setEnabled(true);
                    adminFrameView.getReservationView().getReturnButton().setEnabled(false);
                    adminFrameView.getReservationView().getPickUpButton().setEnabled(false);
                } else if (approve.equals("TRUE") && returned.equals("TRUE") && pickUp.equals("TRUE")) {
                    adminFrameView.getReservationView().getApproveButton().setEnabled(false);
                    adminFrameView.getReservationView().getPickUpButton().setEnabled(false);
                    adminFrameView.getReservationView().getReturnButton().setEnabled(false);
                } else if (approve.equals("TRUE") && pickUp.equals("TRUE")) {
                    adminFrameView.getReservationView().getApproveButton().setEnabled(false);
                    adminFrameView.getReservationView().getPickUpButton().setEnabled(false);
                    adminFrameView.getReservationView().getReturnButton().setEnabled(true);
                    adminFrameView.getReservationView().getDeleteButton().setEnabled(false);
                } else if (approve.equals("TRUE")) {
                    adminFrameView.getReservationView().getApproveButton().setEnabled(false);
                    adminFrameView.getReservationView().getPickUpButton().setEnabled(true);
                    adminFrameView.getReservationView().getReturnButton().setEnabled(false);
                    adminFrameView.getReservationView().getDeleteButton().setEnabled(false);
                } else if (pickUp.equals("TRUE")) {
                    adminFrameView.getReservationView().getApproveButton().setEnabled(false);
                    adminFrameView.getReservationView().getPickUpButton().setEnabled(false);
                    adminFrameView.getReservationView().getReturnButton().setEnabled(true);
                    adminFrameView.getReservationView().getDeleteButton().setEnabled(false);
                } else {
                    adminFrameView.getReservationView().getApproveButton().setEnabled(false);
                    adminFrameView.getReservationView().getPickUpButton().setEnabled(false);
                    adminFrameView.getReservationView().getReturnButton().setEnabled(false);
                    adminFrameView.getReservationView().getDeleteButton().setEnabled(true);
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {
            }
        });
    }
}

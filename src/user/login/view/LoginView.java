package user.login.view;

import com.formdev.flatlaf.FlatClientProperties;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class LoginView extends JPanel {

    public LoginView() {
        init();
    }

    private void init() {
        setBackground(Color.decode("#04151f"));
        setLayout(new MigLayout("fill,insets 20", "[center]", "[center]"));
        txtId = new JTextField();
        txtPassword = new JPasswordField();
        loginButton = new JButton("Login");
        activeIpLabel = new JLabel("");
        activeIpLabel.setHorizontalAlignment(SwingConstants.CENTER);
        activeIpLabel.setForeground(new Color(128, 128, 128));
        settingsButton = new JButton("⚙");

        JPanel panel = new JPanel(new MigLayout("wrap,fillx,insets 35 45 30 45", "fill,250:280", "[center]"));
        panel.putClientProperty(FlatClientProperties.STYLE, "arc:20;" +
                "[light]background:darken(@background,3%);" +
                "[dark]background:lighten(@background,3%)");

        txtPassword.putClientProperty(FlatClientProperties.STYLE, "showRevealButton:true");
        loginButton.putClientProperty(FlatClientProperties.STYLE, "[light]background:darken(@background,10%);" +
                "[dark]background:lighten(@background,10%);" +
                "borderWidth:0;" +
                "focusWidth:0;" +
                "innerFocusWidth:0");


        txtId.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "Enter your ID Number");
        txtPassword.putClientProperty(FlatClientProperties.PLACEHOLDER_TEXT, "Enter your password");

        JLabel lbTitle = new JLabel("Welcome back!");
        description = new JLabel("Please sign in to access your account");
        lbTitle.putClientProperty(FlatClientProperties.STYLE, "font:bold +10");
        description.putClientProperty(FlatClientProperties.STYLE, "[light]foreground:lighten(@foreground,30%);" +
                "[dark]foreground:darken(@foreground,30%)");


        settingsButton.putClientProperty(FlatClientProperties.STYLE, "[light]background:darken(@background,10%);" +
                "[dark]background:lighten(@background,10%);" +
                "borderWidth:0;" +
                "focusWidth:0;" +
                "innerFocusWidth:0");

        panel.add(lbTitle);
        panel.add(description);
        panel.add(new JLabel("ID Number"), "gapy 8");
        panel.add(txtId);
        panel.add(new JLabel("Password"), "gapy 8");
        panel.add(txtPassword);
        panel.add(loginButton, "gapy 10");

        panel.add(activeIpLabel, "gapy 5");
        add(settingsButton, "alignx right, aligny top, wrap, w 30!, h 30!");
        add(panel, "alignx center, aligny top");

    }

    public void setLoginActionListener(ActionListener listener) {
        loginButton.addActionListener(listener);
    }

    public void setSettingsActionListener(ActionListener listener) {
        settingsButton.addActionListener(listener);
    }

    public void setActiveIp(String activeIp) {
        activeIpLabel.setText("Server IP Address: " + activeIp);
    }

    public JTextField getTxtId() {
        return txtId;
    }

    public JPasswordField getTxtPassword() {
        return txtPassword;
    }

    public JLabel getDescription() {
        return description;
    }

    private JTextField txtId;
    private JPasswordField txtPassword;
    private JButton loginButton;
    private JLabel description;
    private JButton settingsButton;
    private JLabel activeIpLabel;

}

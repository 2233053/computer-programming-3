package user.login.model;

import shared.references.User;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * The model class for the Login.
 */
public class LoginModel {
    /**
     * Requests the server to authenticate the user's credentials.
     *
     * @param id       The user's id.
     * @param password The user's password.
     * @param host     The server's IP address.
     * @return The authentication status of the user.
     */
    public static Object loginToServer(String id, String password, String host) {
        try (Socket socket = new Socket(host, 2000)) {
            ObjectOutputStream writer = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream reader = new ObjectInputStream(socket.getInputStream());
            writer.writeObject(new User(null, null, null, password, id));
            return reader.readObject();
        }catch (IOException | ClassNotFoundException e){
            return null;
        }
    }
}
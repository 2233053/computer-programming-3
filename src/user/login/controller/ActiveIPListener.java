package user.login.controller;

public interface ActiveIPListener {
    void onActiveIPChanged(String newIP);
}
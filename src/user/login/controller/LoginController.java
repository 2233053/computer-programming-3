package user.login.controller;

import shared.AuthenticationStatus;
import user.admin.controller.AdminFrameController;
import user.admin.model.AdminFrameModel;
import user.admin.view.AdminFrameView;
import user.client.controller.JFrameController;
import user.client.model.JFrameModel;
import user.client.view.JFrameView;
import user.login.model.LoginModel;
import user.login.view.LoginView;
import user.settings.model.SettingsModel;
import user.settings.controller.SettingsController;
import user.settings.view.SettingsView;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("unused")
public class LoginController implements ActiveIPListener {
    private LoginView view;
    private LoginModel model;

    public LoginController(LoginView view, LoginModel model) {
        this.view = view;
        this.model = model;

        // Set the server IP address
        SettingsModel config = new SettingsModel();
        String host = config.getActiveIP();
        view.setActiveIp(host);

        view.setLoginActionListener(e -> {
            AuthenticationStatus status = (AuthenticationStatus) LoginModel
                    .loginToServer(view.getTxtId().getText(),
                            String.valueOf(view.getTxtPassword().getPassword()),
                            host);
            switch (status) {
                case PASSWORD_INCORRECT:
                    view.getDescription().setForeground(Color.RED);
                    view.getDescription().setText("Incorrect password. Please check your password!");
                    view.getTxtPassword().putClientProperty("JComponent.outline", Color.RED);
                    break;
                case SUCCESS_ADMIN:
                    SwingUtilities.getWindowAncestor(view).dispose();
                    AdminFrameModel adminModel = new AdminFrameModel();
                    AdminFrameView adminView = new AdminFrameView();
                    AdminFrameController controller = new AdminFrameController(adminView, adminModel);
                    adminView.setVisible(true);
                    break;
                case SUCCESS_CLIENT:
                    SwingUtilities.getWindowAncestor(view).dispose();
                    JFrameModel JFrameModel = new JFrameModel(host);
                    JFrameView JFrameView = new JFrameView(JFrameModel, String.valueOf(view.getTxtId().getText()));
                    JFrameController JFrameController = new JFrameController(JFrameView, JFrameModel, String.valueOf(view.getTxtId().getText()));
                    JFrameView.setVisible(true);
                    break;
                case USER_NOT_FOUND:
                    view.getDescription().setForeground(Color.RED);
                    view.getDescription().setText("User not found!");
                    view.getTxtId().putClientProperty("JComponent.outline", Color.RED);
                    view.getTxtPassword().putClientProperty("JComponent.outline", Color.RED);
                    break;
            }
        });

        view.setSettingsActionListener(e -> {
            SettingsView settingsView = new SettingsView();
            SettingsController settingsController = new SettingsController(settingsView, config);
            settingsView.setVisible(true);
        });

        config.addActiveIPListener(this);

    }

    @Override
    public void onActiveIPChanged(String newIP) {
        view.setActiveIp(newIP);
    }

}